Documentation:

Please see each class for documentation. Variables names and functions should be named self-explanatory, with comments where addition information is need.
We would have organized these into sub-packages, but the assignment states that all classes should be in edu.carleton.comp4101.assignment1

Configuration:
	Question 4 REQUIRES java 1.7 environment as it uses the new File IO tools which were implemented in 1.7.

Testing:

Question1
	I tested with multiple values, can be seen in comments at the top of Question1.
	Each type of bracket encapsulation behaves as expected.
		[ [ ] ] behaves as if there is no second [ ]
		[ { } ] everything between [ ] runs at any time, but all run before anything after the bracket
				on one of the values in between { } runs
		{ { } } only one thing runs between the first set of { }, which may be the second set,
				in that case, only one thing between the second set runs
		{ [ ] } only on thing runs between the { }, which may be the [ ] brackets, meaning everything
				between [ ] will show up, in any order
	Since only two layers were required, I only tested those combinations, however, more layers should work as well, by design.

	Testing examples can be found in question1Testing.txt

	The first test prints all letters, as expected for [[]]
	The second test prints only one of BCDE, and BC should appear more often
	The third test prints either B, C, DE, or ED in between A and F, as expected
	The forth test prints B, C and either D or E between A and F.

	
	
Question2
	There are 2 other options I added, -d, for debugging, and -r for a single message.
	Just add -d y and much more information is printed. Add -r y and a single message is sent, useful only with -d y on as well.
	Using -d y and -r y, where instead of running each Node thread, we run a single message from Node1 to Node 5, we see messages
	sent across the network, meaning I could see what each node did exactly.
	
	Testing examples can be found in question2Testing.txt

	First, I ran a debug messages and single message from Node 1 to Node 5 using input.txt
	Second, I ran a normal simulation using input.txt, where nodes are random generating messages.
	Third and Forth as the same as above, but using input2.txt, for a large network (10 nodes)

	Looking at the first test, you can see that Node one generates a message for node 5
		Node 1 sends the message to 2 and 3
		Node 2 sends the message to 1 and 4 and 1 says it already sent that message out
		Node 3 sends the message to 2, 5 and 1. 1 and 2 say they already sent it out. 5 notes that the message is for itself
		Node 4 sends the message to 2 and 5, they say they already sent it out
		Node 5 sends nothing out, as the message was for itself
		
	
Question3:

	Testing was quite simple, ran for a long time and checked output. Made sure the correct eater always ran based on what food
	was supplied by the agent. Checked that the argument parsing was handled properly, and that any incorrect values were explained
	through error handling.

	Testing examples can be found in question3Testing.txt
	
Question4:
	Some assumptions where made for this question:
	1. If multiple parts need the same part then the required part must be built twice
	2. In the command line if the command java Question4 -c input.txt -b A z was given
	   and z is not in the file then z will be treated as a component with no dependencies
	3. The file location is relative to the source directory so all input files are located
	   in the source folder so -c input.txt then input.txt is in the source folder.
	4. A part that is before another part is only important if it needs to be built.
	5. The -b command will only have one instance for unique name. So -b A A is not valid. 
	
	Testing focused on ensuring that the program completed correctly and that all assumptions held.
	Testing examples can be found in question4Testing.txt
	
	All classes aside from Question4.java associated with question 4 are Q4(class name).java 
	
Question5:
	Testing was done for all likely secenario as improper data arguments entered such as:
	java Question5  -C 5 -C 5 -g 1000 -c 1000 -W 4 -R 10000 
	java Question5  -C 5 -B 5 -g 1000 -c 1000 -W 4
	Which yielded errors messages such as:
	Input is not correct.
	Invalid Input
	
	For stress testing the situations chosen where similar to those suggested by the assignment such as: one customer and
	one barber, many customers and few barbers, many barbers and few customers, one chair, more than enough chairs.
	All the stress tests can be seen in the file Question5Testing.txt 
	
	All classes aside from Question5.java associated with question 5 are Q5(class name).java 