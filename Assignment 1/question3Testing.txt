Parsing arguments
... simulation time is: 30
... eat time can be up to: 2
Running simulation:
Agent has placed POTATO and WATER on the table
Eater had BUTTER, picked up POTATO and WATER and ate food in 0.09 seconds.
Agent has placed POTATO and WATER on the table
Eater had BUTTER, picked up POTATO and WATER and ate food in 0.86 seconds.
Agent has placed POTATO and WATER on the table
Eater had BUTTER, picked up POTATO and WATER and ate food in 1.44 seconds.
Agent has placed POTATO and BUTTER on the table
Eater had WATER, picked up POTATO and BUTTER and ate food in 0.26 seconds.
Agent has placed POTATO and BUTTER on the table
Eater had WATER, picked up POTATO and BUTTER and ate food in 0.79 seconds.
Agent has placed POTATO and BUTTER on the table
Eater had WATER, picked up POTATO and BUTTER and ate food in 0.73 seconds.
Agent has placed POTATO and WATER on the table
Eater had BUTTER, picked up POTATO and WATER and ate food in 0.92 seconds.
Agent has placed WATER and BUTTER on the table
Eater had POTATO, picked up WATER and BUTTER and ate food in 0.07 seconds.
Agent has placed POTATO and BUTTER on the table
Eater had WATER, picked up POTATO and BUTTER and ate food in 1.72 seconds.
Agent has placed WATER and BUTTER on the table
Eater had POTATO, picked up WATER and BUTTER and ate food in 0.84 seconds.
Agent has placed POTATO and BUTTER on the table
Eater had WATER, picked up POTATO and BUTTER and ate food in 1.76 seconds.
Agent has placed WATER and BUTTER on the table
Eater had POTATO, picked up WATER and BUTTER and ate food in 0.51 seconds.
Agent has placed WATER and BUTTER on the table
Eater had POTATO, picked up WATER and BUTTER and ate food in 1.65 seconds.
Agent has placed POTATO and BUTTER on the table
Eater had WATER, picked up POTATO and BUTTER and ate food in 1.37 seconds.
Agent has placed WATER and BUTTER on the table
Eater had POTATO, picked up WATER and BUTTER and ate food in 1.51 seconds.
Agent has placed POTATO and WATER on the table
Eater had BUTTER, picked up POTATO and WATER and ate food in 1.85 seconds.
Agent has placed WATER and BUTTER on the table
Eater had POTATO, picked up WATER and BUTTER and ate food in 1.05 seconds.
Agent has placed POTATO and BUTTER on the table
Eater had WATER, picked up POTATO and BUTTER and ate food in 0.36 seconds.
Agent has placed POTATO and BUTTER on the table
Eater had WATER, picked up POTATO and BUTTER and ate food in 0.03 seconds.
Agent has placed POTATO and BUTTER on the table
Eater had WATER, picked up POTATO and BUTTER and ate food in 1.35 seconds.
Agent has placed POTATO and WATER on the table
Eater had BUTTER, picked up POTATO and WATER and ate food in 1.00 seconds.
Agent has placed POTATO and BUTTER on the table
Eater had WATER, picked up POTATO and BUTTER and ate food in 0.10 seconds.
Agent has placed POTATO and BUTTER on the table
Eater had WATER, picked up POTATO and BUTTER and ate food in 0.17 seconds.
Agent has placed WATER and BUTTER on the table
Eater had POTATO, picked up WATER and BUTTER and ate food in 0.43 seconds.
Agent has placed POTATO and BUTTER on the table
Eater had WATER, picked up POTATO and BUTTER and ate food in 0.23 seconds.
Agent has placed POTATO and BUTTER on the table
Eater had WATER, picked up POTATO and BUTTER and ate food in 0.33 seconds.
Agent has placed POTATO and WATER on the table
Eater had BUTTER, picked up POTATO and WATER and ate food in 1.67 seconds.
Agent has placed POTATO and BUTTER on the table
Eater had WATER, picked up POTATO and BUTTER and ate food in 1.91 seconds.
Agent has placed POTATO and BUTTER on the table
Eater had WATER, picked up POTATO and BUTTER and ate food in 1.66 seconds.
Agent has placed WATER and BUTTER on the table
Eater had POTATO, picked up WATER and BUTTER and ate food in 0.66 seconds.
Agent has placed WATER and BUTTER on the table
Eater had POTATO, picked up WATER and BUTTER and ate food in 1.64 seconds.
Agent has placed POTATO and BUTTER on the table
Eater had WATER, picked up POTATO and BUTTER and ate food in 0.28 seconds.
Agent has placed POTATO and BUTTER on the table
Done Simulation

Parsing arguments
... simulation time is: 60
... eat time can be up to: 5
Running simulation:
Agent has placed WATER and BUTTER on the table
Eater had POTATO, picked up WATER and BUTTER and ate food in 4.64 seconds.
Agent has placed WATER and BUTTER on the table
Eater had POTATO, picked up WATER and BUTTER and ate food in 3.86 seconds.
Agent has placed POTATO and BUTTER on the table
Eater had WATER, picked up POTATO and BUTTER and ate food in 2.68 seconds.
Agent has placed POTATO and BUTTER on the table
Eater had WATER, picked up POTATO and BUTTER and ate food in 3.45 seconds.
Agent has placed POTATO and BUTTER on the table
Eater had WATER, picked up POTATO and BUTTER and ate food in 2.71 seconds.
Agent has placed POTATO and WATER on the table
Eater had BUTTER, picked up POTATO and WATER and ate food in 3.66 seconds.
Agent has placed POTATO and BUTTER on the table
Eater had WATER, picked up POTATO and BUTTER and ate food in 3.54 seconds.
Agent has placed POTATO and BUTTER on the table
Eater had WATER, picked up POTATO and BUTTER and ate food in 1.82 seconds.
Agent has placed POTATO and WATER on the table
Eater had BUTTER, picked up POTATO and WATER and ate food in 1.88 seconds.
Agent has placed POTATO and WATER on the table
Eater had BUTTER, picked up POTATO and WATER and ate food in 1.89 seconds.
Agent has placed POTATO and BUTTER on the table
Eater had WATER, picked up POTATO and BUTTER and ate food in 1.63 seconds.
Agent has placed POTATO and WATER on the table
Eater had BUTTER, picked up POTATO and WATER and ate food in 4.14 seconds.
Agent has placed POTATO and BUTTER on the table
Eater had WATER, picked up POTATO and BUTTER and ate food in 3.85 seconds.
Agent has placed POTATO and BUTTER on the table
Eater had WATER, picked up POTATO and BUTTER and ate food in 3.29 seconds.
Agent has placed POTATO and WATER on the table
Eater had BUTTER, picked up POTATO and WATER and ate food in 4.90 seconds.
Agent has placed POTATO and BUTTER on the table
Eater had WATER, picked up POTATO and BUTTER and ate food in 4.53 seconds.
Agent has placed WATER and BUTTER on the table
Eater had POTATO, picked up WATER and BUTTER and ate food in 3.96 seconds.
Agent has placed POTATO and WATER on the table
Eater had BUTTER, picked up POTATO and WATER and ate food in 0.15 seconds.
Agent has placed POTATO and WATER on the table
Eater had BUTTER, picked up POTATO and WATER and ate food in 0.73 seconds.
Agent has placed POTATO and WATER on the table
Done Simulation
