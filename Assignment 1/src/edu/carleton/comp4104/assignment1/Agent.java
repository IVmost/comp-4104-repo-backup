package edu.carleton.comp4104.assignment1;

import java.util.Random;
import java.util.Vector;

public class Agent implements Runnable {

	protected Control _monitorControl;
	protected Random _r = new Random();
	
	public Agent (Control monitorControl) {
		_monitorControl = monitorControl;
	}
	
	@Override
	public void run() {
		
		boolean keepRunning = true;
		
		while (keepRunning) {
			//Randomly Place two ingredients onto table
			int ingredientsChoice = _r.nextInt(3);
			Vector<Food> ingredients = new Vector<Food>();
			
			switch(ingredientsChoice) {
			case 0:
				ingredients.add(Food.POTATO);
				ingredients.add(Food.WATER);
				break;
			case 1:
				ingredients.add(Food.POTATO);
				ingredients.add(Food.BUTTER);
				break;
			case 2:
				ingredients.add(Food.WATER);
				ingredients.add(Food.BUTTER);
				break;
			default:
				System.err.println("Error: Random number was not between 0 - 2 (inclusive)");
				System.exit(1);
				break;
			}
			_monitorControl.placeIngredients(ingredients);
			
			System.out.println("Agent has placed " + ingredients.get(0) + " and " + ingredients.get(1) + " on the table");
			
			//Wait for eaters to finish
			_monitorControl.agentCanPlaceIngredients();			
		}
	}

}
