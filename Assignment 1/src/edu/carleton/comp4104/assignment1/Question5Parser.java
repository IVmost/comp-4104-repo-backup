package edu.carleton.comp4104.assignment1;

public class Question5Parser {
	
	
	//parse for input to question 5 
	public int[] parse(String[] input){
		int[] output=new int[6];
		
		//sets the output string as -1 for error checking
		for (int x=0; x<output.length; x++)
			output[x]=-1;
		
		
		//checks to see if the right number of variable were enter
		if (input.length!=12){
			System.out.println("Input is not correct.");
			return null;
		}
		int CCount=0;
		int BCount=0;
		int gCount=0;
		int cCount=0;
		int WCount=0;
		int RCount=0;
		boolean error=false;
		
		//goes through the array and orders the variables to their proper positions for setup
		for (int x=0; x<input.length;x+=2){
			//ensures that the potential number data is a number that is >= 1
			if (!input[x+1].matches("[1-9][0-9]*")){
				System.out.println("Input is not correct for "+input[x]+": "+ input[x+1]);
				return null;
			}
			//trims the command to just the character to enable the use of a switch statement
			String command=input[x].replace("-", "").trim();
			
			//switch that builds the proper input and rejects invalid command
			//it is order agnostic so the commands can be in whatever order and it will yield a correct result.
			switch (command.charAt(0)) {
			case 'C':
				error= ++CCount > 1;
				output[0]=Integer.parseInt(input[x+1]);
				break;
			case 'B':
				error= ++BCount > 1;
				output[1]=Integer.parseInt(input[x+1]);
				break;
			case 'g':
				error= ++gCount > 1;
				output[2]=Integer.parseInt(input[x+1]);
				break;
			case 'c':
				error= ++cCount > 1;
				output[3]=Integer.parseInt(input[x+1]);
				break;
			case 'W':
				error= ++WCount > 1;
				output[4]=Integer.parseInt(input[x+1]);
				break;
			case 'R':
				error= ++RCount > 1;
				output[5]=Integer.parseInt(input[x+1]);
				break;
			default:
				System.out.println("Command: " + input[x] + " is not valid");
				return null;
			}
			if (error){
				System.out.println("Command: " + input[x] +" has been entered twice");
				return null;
			}
			
				
		}
		
		//extra catch to ensure all data entered is correct.
		for (int x=0; x<output.length; x++)
		{
			if (output[x]==-1){
				switch (x) {
				case 0:
					System.out.println("Command -C was not given.");
					return null;
				case 1:
					System.out.println("Command -B was not given.");
					return null;
				case 2:
					System.out.println("Command -g was not given.");
					return null;
				case 3:
					System.out.println("Command -c was not given.");
					return null;
				case 4:
					System.out.println("Command -W was not given.");
					return null;
				case 5:
					System.out.println("Command -R was not given.");
					return null;
				
				}
			}
		}
		return output;
	}
}
