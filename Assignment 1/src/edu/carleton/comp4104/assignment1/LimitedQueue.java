//basic override of a link list to remove the head if the adding a tail exceeds the desired length
//Taken from: http://stackoverflow.com/questions/5498865/size-limited-queue-that-holds-last-n-elements-in-java

package edu.carleton.comp4104.assignment1;

import java.util.LinkedList;

public class LimitedQueue<E> extends LinkedList<E> {

	private int limit;

    public LimitedQueue(int limit) {
        this.limit = limit;
    }

    @Override
    public boolean add(E o) {
        super.add(o);
        while (size() > limit) { super.remove(); }
        return true;
    }
}
