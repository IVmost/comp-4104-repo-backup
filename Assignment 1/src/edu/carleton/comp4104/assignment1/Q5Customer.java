package edu.carleton.comp4104.assignment1;

import java.util.Date;
import java.util.Random;


//customer object
public class Q5Customer implements Runnable {

	//customer id
	private int customerNo;
	//max time hair will grow
	private final long maxGrowTime;
	
	//simulation start time
	private final long startTime;
	
	//simulation end time
	private final long endTime;

	//boolean for if the simulation is running
	private boolean running;
	
	//salon monitor object
	private Q5Salon salon;

	//constructor
	public Q5Customer(int customerNo, long maxGrowTime, Q5Salon salon,
			long startTime, long endTime) {
		this.customerNo = customerNo;
		this.running = true;
		this.maxGrowTime = maxGrowTime;
		this.salon = salon;
		this.startTime = startTime;
		this.endTime = endTime;
	}

	
	//starts the thread
	public void start() {
		new Thread(this, "Customer " + customerNo).start();

	}

	@Override
	public void run() {
		//random number generator for hair growth
		Random random = new Random();
		
		//simulation loop
		while (running == true) {
			//get random time to grow hair
			long millis = Math.abs(random.nextLong()) % maxGrowTime + 1;
			long curTime = new Date().getTime() - startTime;
			
			
			//checks to see if the simulation is done
			if (curTime < endTime) {
				//grow hair message
				curTime = new Date().getTime() - startTime;
				System.out.println("Time=" + curTime + ", "
						+ Thread.currentThread().getName()
						+ " growing hair for " + millis + "ms");

				//grows hair
				try {
					Thread.sleep(millis);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//check simulation time
				curTime = new Date().getTime() - startTime;
				if (curTime < endTime) {
					//goes to get a hair cut
					System.out.println("Time=" + curTime + ", "
							+ Thread.currentThread().getName()
							+ " needs a haircut.");
					//goes to the salon
					millis = salon.customerArrive();
					//checks if the time is valid
					if (millis != -1) {
						
						//message for getting hair cut
						curTime = new Date().getTime() - startTime;
						System.out.println("Time=" + curTime + ", "
								+ Thread.currentThread().getName()
								+ " getting a hair cut for " + millis + "ms");
						try {
							//hair getting "cut"
							Thread.sleep(millis);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				}else
					//end simulation
					running = false;
			} else
				//end simulation
				running = false;
		}
		//notify salon that the simulation is over
		salon.endSimulation();
		//end simulation message
		System.out.println(Thread.currentThread().getName()+" Complete");

	}

}
