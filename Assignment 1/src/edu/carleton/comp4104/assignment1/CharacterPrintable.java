package edu.carleton.comp4104.assignment1;

public class CharacterPrintable extends AbstractCharacterPrintable {

	//Most basic of the concrete ACPs, prints its character, and always tell the next char it can print
	
	public CharacterPrintable(char characterToPrint, AbstractCharacterPrintable characterBefore) {
		super(characterToPrint, characterBefore);
	}

	@Override
	public void printCharacter() {
		System.out.print(_characterToPrint);
	}

	@Override
	public boolean canNextCharPrint() {
		return true;
	}

}
