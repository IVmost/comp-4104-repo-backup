package edu.carleton.comp4104.assignment1;

public class Message {

	//Filled with public finals, except for the from, which can be modified for debugging purposes
	//Also contains a static Global_Message_Id and a method for getting an ID, keeps them unique
	//Has a copy constructor, so as a message propagates, it changes the from variable
	//Has a time formatter, to make system time in ms related to the first time a message was sent out
	
	protected static int Global_Message_Id;
	
	protected static long startTime = System.currentTimeMillis();
	
	public final int source;
	
	public int from;
	
	public final int destination;
	
	public final int messageId;
	
	public final String msg;
	
	public final String time;
	
	public Message(int source, int destination, String time) {
		this.source = source;
		this.from = source;
		this.destination = destination;
		this.messageId = getMessageId();
		this.time = time;
		this.msg = "Message " + messageId + " created at " + this.time;
	}
	
	public Message(Message inMessage, int fromNode) {
		this.source = inMessage.source;
		this.from = fromNode;
		this.destination = inMessage.destination;
		this.messageId = inMessage.messageId;
		this.time = inMessage.time;
		this.msg = inMessage.msg;
	}
	
	protected static synchronized int getMessageId() {
		return Global_Message_Id++;
	}
	
	public static String formatTime(long time) {
		long elapsed = time - startTime;
		return "" + elapsed + "ms";
	}
	
}
