package edu.carleton.comp4104.assignment1;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class Q5Salon {

	//array that acts as the waiting room for the customers
	private ArrayList<Thread> waitingRoom;
	//hashmap used to give customers the time thier hair cut takes
	private HashMap<Thread, Long> cuttingTime;
	
	//the simulation start time
	private long startTime;
	
	//boolean flag for the simulation running
	private boolean isRunning;
	
	//the size of the waiting room
	private final int waitingRoomSize;

	//construction
	public Q5Salon(long startTime, int waitingRoomSize) {
		waitingRoom = new ArrayList<Thread>();
		cuttingTime = new HashMap<Thread, Long>();
		isRunning = true;
		this.startTime = startTime;
		this.waitingRoomSize = waitingRoomSize;
	}

	//entrance to the monitor
	public synchronized long customerArrive() {
		//customer tries to enter the monitor
		customerEnter();

		//get the current time for message that the customer is waiting
		long curTime = new Date().getTime() - startTime;
		System.out.println("Time=" + curTime + ", "
				+ Thread.currentThread().getName() + " in room, waiting="
				+ waitingRoom.size());
		//notifies all waiting threads that a customer is waiting
		notifyAll();
		
		
		try {
			//while for customer waiting to be served be a barber
			while (!cuttingTime.containsKey(Thread.currentThread())) {
				wait();
				//exits if the simulation is done
				if (!isRunning)
					return -1;
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		//the customer leave if the simulation is still running
		if (isRunning)
			return customerLeave();
		return -1;
	}

	//customer leaves the monitor
	private synchronized long customerLeave() {
		//gets the time for cutting the customer hair
		long cutTime = cuttingTime.remove(Thread.currentThread());
		return cutTime;
	}

	//customer enters the monitor
	private synchronized void customerEnter() {
		//while loop for waiting for space in the waiting room
		while (waitingRoom.size() >= waitingRoomSize) {
			try {
				wait();
				if (!isRunning)
					return;
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		//adds current customer to the waiting room
		waitingRoom.add(Thread.currentThread());

	}

	//section for barber arriving in the monitor
	public synchronized boolean barberReady(Long cutTime) {
		//barber enters
		barberEnter();

		
		if (isRunning) {
			//gets the next waiting customer
			Thread client = waitingRoom.remove(0);
		
			long curTime = new Date().getTime() - startTime;
			System.out.println("Time=" + curTime + ", "
					+ Thread.currentThread().getName() + " has "
					+ client.getName() + ", waiting=" + waitingRoom.size());
			//assigns the customer the amount of time the hair cut takes
			cuttingTime.put(client, cutTime);
			//notifies all waiting threads
			notifyAll();
		}
		
		//barber leaves
		return barberLeave();
	}

	private synchronized boolean barberLeave() {
		//returns if simulation time has expired or not
		if (!isRunning) {
			notifyAll();
			return false;
		}
		return true;
	}

	//barber entrance
	private synchronized void barberEnter() {
		//waits for customers in the waiting room
		while (waitingRoom.isEmpty() && isRunning) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

	//call to end the simulation and free all waiting threads without requiring an interupt
	public synchronized void endSimulation() {
		isRunning = false;
		notifyAll();

	}

}
