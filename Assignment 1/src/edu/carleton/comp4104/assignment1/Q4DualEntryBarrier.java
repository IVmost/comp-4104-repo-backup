package edu.carleton.comp4104.assignment1;

//barrier used for blocking elements
public class Q4DualEntryBarrier {
	//number of controller entities which must enter
	private final int parties;
	//locks the barrier
	private boolean locked;
	//number of entries from controllers
	private int count;
	//number of entries required from a single controller (i.e. A needs two B to be built)
	private int runs;
	
	//constructor which sets the number of required entries by a controller
	public Q4DualEntryBarrier(int parties) {
		this.parties=parties;
		this.count=0;
		locked=true;
		this.runs=1;
	}
	
	//entry for an element which must enter multiple times
	public synchronized void userEntry() {
		//loop which waits for it to unlock
		while (locked)
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	public synchronized void controllerEntry() {
		//add 1 to the count and check if controllers have entered enough times
		if (++count == parties*runs){
			//unlocks barrier
			locked=false;
			//notify all waiting threads
			notifyAll();
		}
		
		
	}
	
	//get the required number of runs for the barrier
	public synchronized int getRuns(){
		return runs;
	}
	
	//sets the required number of runs for the barrier
	public synchronized void setRuns(int runs){
		this.runs=runs;
	}

	 

}
