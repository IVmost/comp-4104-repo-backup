package edu.carleton.comp4104.assignment1;

public class IntPair {
		
	//simple class to keep two ints paired
	
	public final int A;
	public final int B;
	
	public IntPair(int a, int b) {
		A = a;
		B = b;
	}
}
