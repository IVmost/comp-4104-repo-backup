package edu.carleton.comp4104.assignment1;

import com.sun.corba.se.impl.orbutil.concurrent.Sync;

public class Latch implements Sync {

	//Basic latch, starts closed, when someone opens it, everyone can go through
	
	//Taken right out of Professor Tony White's notes
	
	protected boolean latched = true;
	protected boolean firstThrough = true;
	
	@Override
	public synchronized void acquire() throws InterruptedException {
		while(latched) wait();
	}

	@Override
	public boolean attempt(long arg0) throws InterruptedException {
		return false;
	}

	@Override
	public synchronized void release() {
		latched = false;
		notifyAll();
	}

}
