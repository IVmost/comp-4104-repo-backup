package edu.carleton.comp4104.assignment1;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Vector;

public class Question2 {

	//static string which is the file directory of the input files
	static String filelocation = System.getProperty("user.dir")
			+ "\\src\\edu\\carleton\\comp4104\\assignment1\\";
	
	public static void main(String args[]) {
		
		//Single test flag
		boolean runSingleTest = false;
		
		//-----Parse Input-----
		//	we expect 6 arguments, or 8/10, the extra debugging option I added, format is -d y and/or -r y
		if (args.length != 6 && args.length != 8 && args.length != 10) {
			System.err.println("Incorrect number of arguments");
			System.exit(1);
		}
		
		//	information to be pulled from the arguments, with some default values so we get no warnings
		//		information is overwritten, or we print error and quit
		String inputFileName = null;
		int sleepTime = 5;
		int channelCapacity = 5;
		boolean debugOn = false;
		
		System.out.println("Parsing arguments");
		
		//Loop through each pair of arguments, and handle, if unexpected argument, print error, and quit
		for (int i = 0; i < args.length; i+=2) {
			String key = args[i];
			if (key.equals("-c")) {
				inputFileName = filelocation+args[i+1];
			}else if (key.equals("-C")){
				inputFileName= args[i+1];
			}
			else if (key.equals("-t")) {
				sleepTime = Integer.parseInt(args[i+1]);
			}
			else if (key.equals("-s")) {
				channelCapacity = Integer.parseInt(args[i+1]);
			}
			else if (key.equals("-d")) {
				if (args[i+1].equals("y")) {
					debugOn = true;
				}
			}
			else if (key.equals("-r")) {
				if (args[i+1].equals("y")) {
					runSingleTest = true;
				}
			}
			else {
				System.err.println("Error in arguments: don't recognize key: " + key);
				System.exit(1);
			}
		}
		
		//	print what we pulled out of the arguments
		System.out.println("...file: " + inputFileName);
		System.out.println("...run time: " + sleepTime + "s");
		System.out.println("...channel size: " + channelCapacity);
		if (debugOn) {
			System.out.println("...debug on");
		}
		
		//-----Parse file-----
		System.out.println("Parsing file");
		
		//	the information we plan to pull out of the file
		int nodeCount = 5;
		Vector<IntPair> channels = new Vector<IntPair>();
		
		//	streams to use for reading the file
		FileInputStream inFileStream = null;
		DataInputStream inDataStream = null;
		BufferedReader bufferReader = null;
		
		try {
			inFileStream = new FileInputStream(inputFileName);
			inDataStream = new DataInputStream(inFileStream);
			bufferReader = new BufferedReader(new InputStreamReader(inDataStream));
			
			System.out.println("...opened file: " + inputFileName);
			
			String curLine = null;
			
			//Read number of nodes
			curLine = bufferReader.readLine();
			try {
				if (curLine != null) nodeCount = Integer.parseInt(curLine.trim());
				else throw new IOException("Could find number of nodes");
			} catch (NumberFormatException e) {
				System.err.println("Number of nodes could not be parsed, Error: " + e.getMessage());
				System.exit(1);
			}
			
			System.out.println("...number of nodes is " + nodeCount);
			
			//Parse node map
			//	we add the parsed information to an IntPair object, to be kept in a Vector
			while ((curLine = bufferReader.readLine()) != null) {
				String[] nodes = curLine.split(" ");
				if (nodes.length != 2) throw new IOException("Expected two values");
				//We put the try/catch for number format exception here, so we can see in more detail where an error pops up
				try {
					int node1 = Integer.parseInt(nodes[0]);
					int node2 = Integer.parseInt(nodes[1]);
					channels.add(new IntPair(node1, node2));
					System.out.println("...found channel: " + node1 + ", " + node2);
				} catch (NumberFormatException e) {
					System.err.println("Could not parse value, Error: " + e.getMessage());
					System.exit(1);
				}
			}
			
			System.out.println("...found a total of " + channels.size() + " channels");
			
		} catch (FileNotFoundException e1) {
			System.err.println("\'" + inputFileName + "\' was not found");
			System.exit(1);
		} catch (IOException e) {
			System.err.println("Error reading file: " + e.getMessage());
			System.exit(1);
		} finally {
			// Clear up the buffers if they were used
			try {
				if (bufferReader != null) bufferReader.close();
				if (inDataStream != null) inDataStream.close();
				if (inFileStream != null) inFileStream.close();
			} catch (IOException e) {
				System.err.println("Could not close streams, continuing");
			}
			
		}
		
		System.out.println("Starting System:");
		
		//Create nodes, and a thread to run them
		//	we keep the nodes mapped by id, for setting up the channels
		//	we also keep them in a list to have easier way to loop through them
		//	and we keep a list of threads to join later
		HashMap<Integer, Node> nodeMap = new HashMap<Integer, Node>();
		Vector<Node> nodeList = new Vector<Node>();
		Vector<Thread> nodeThreads = new Vector<Thread>();
		
		//	create the nodes, and to the list and the map, create a thread for them, remember the thread (start later)
		for (int i = 1; i < nodeCount + 1; i++) {
			Node newNode = new Node(i, nodeCount, debugOn);
			nodeList.add(newNode);
			nodeMap.put(i, newNode);
			Thread newThread = new Thread(newNode);
			nodeThreads.add(newThread);
		}
		System.out.println("...created nodes");
		
		//Give channels to nodes
		for (int i = 0; i < channels.size(); i++) {
			IntPair pair = channels.get(i);
			int node1 = pair.A;
			int node2 = pair.B;
			BiDirectionalChannel<Message> newChannel = new BiDirectionalChannel<Message>(channelCapacity);
			nodeMap.get(node1).useChannel(newChannel);
			nodeMap.get(node2).useChannel(newChannel);
		}
		System.out.println("...gave nodes channels");
		
		//Boot up nodes
		//	start up all the senders and receivers
		for (int i = 0; i < nodeList.size(); i++) {
			nodeList.get(i).activateSendersAndReceivers();
		}
		System.out.println("...activated node senders and receivers");
		
		//System.out.println("Testing");
		// Single Message Test
		if (runSingleTest) {
			
			Node testNode = nodeList.get(0);
			System.out.println("...using node: " + testNode.ID + " sending a message to node 5");
			testNode.broadCastMessage(new Message(testNode.ID, 5, Message.formatTime(System.currentTimeMillis())));
			
			System.out.println("...continuing running for " + sleepTime + "s");
			
			try {
				Thread.sleep(sleepTime * 1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		//Run node threads
		if (!runSingleTest) {
			System.out.println("...activating node threads");
			for (int i = 0; i < nodeThreads.size(); i++) {
				nodeThreads.get(i).start();
			}
			System.out.println("...activated all node threads");
			
			System.out.println("...continuing running for " + sleepTime + "s");
			
			try {
				Thread.sleep(sleepTime * 1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			System.out.println("Stopping");
			System.out.println("...stopping nodes");
			for (int i = 0; i < nodeList.size(); i++) {
				try {
					nodeList.get(i).stopRunning();
				} catch (InterruptedException e) {
					System.err.println("Interupted while stopping Node " + nodeList.get(i).ID);
					e.printStackTrace();
				}
			}
			System.out.println("...stopped nodes");
			
			System.out.println("...stopping node threads");
			for (int i = 0; i < nodeThreads.size(); i++) {
				try {
					nodeThreads.get(i).join();
				} catch (InterruptedException e) {
					System.err.println("Interupted while joining Node " + nodeList.get(i).ID);
					e.printStackTrace();
				}
			}
			System.out.println("...stopped node threads");
		}
		
		System.out.println("...quiting to stop senders and receivers who are waiting on channels");
		System.out.println();
		System.exit(0);
	}
}
