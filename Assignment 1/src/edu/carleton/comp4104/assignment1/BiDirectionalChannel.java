package edu.carleton.comp4104.assignment1;

import java.util.concurrent.ArrayBlockingQueue;

public class BiDirectionalChannel<E> {

	//Modified BiDirectionalChannel from the second week's work.
	//Keeps track of 4 threads to use with two ArrayBlockingQueues
	
	ArrayBlockingQueue<E> _channel1;
	ArrayBlockingQueue<E> _channel2;
	
	Thread _threadAPut;
	Thread _threadATake;
	Thread _threadBPut;
	Thread _threadBTake;
	
	public BiDirectionalChannel(int capacity) {
		_channel1 = new ArrayBlockingQueue<E>(capacity);
		_channel2 = new ArrayBlockingQueue<E>(capacity);
	}
	
	public synchronized void registerThreads(Thread putter, Thread taker)
			throws IllegalAccessException{
		if (_threadAPut == null && _threadATake == null) {
			_threadAPut = putter;
			_threadATake = taker;
		}
		else if (_threadBPut == null && _threadBTake == null) {
			_threadBPut = putter;
			_threadBTake = taker;
		}
		else {
			throw new IllegalAccessException("Channel already in use.");
		}
	}
	
	public void put(E obj) throws IllegalAccessException, InterruptedException {
		if (Thread.currentThread() == _threadAPut) {
			_channel1.put(obj);
		}
		else if (Thread.currentThread() == _threadBPut) {
			_channel2.put(obj);
		}
		else {
			throw new IllegalAccessException("Unexpected thread trying to put information into the BiDirectionalChannel." +
			" Make sure the thread has been registered with the channel first");
		}
	}
	
	public E take() throws IllegalAccessException, InterruptedException {
		E toReturn;
		
		if (Thread.currentThread() == _threadATake) {
			toReturn = _channel2.take();
		}
		else if (Thread.currentThread() == _threadBTake) {
			toReturn = _channel1.take();
		}
		else {
			throw new IllegalAccessException("Unexpected thread trying to put information into the BiDirectionalChannel." +
					" Make sure the thread has been registered with the channel first");
		}
		
		return toReturn;
	}
	
}
