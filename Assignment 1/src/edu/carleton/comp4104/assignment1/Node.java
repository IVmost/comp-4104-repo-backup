package edu.carleton.comp4104.assignment1;

import java.util.Random;
import java.util.Vector;

public class Node implements Runnable {

	//A node has a public ID, and protected other values
	public final int ID;
	protected int _totalNodes;
	protected boolean _run = true;
	protected boolean _debugMessages = false;
	protected Random _r = new Random();
	
	//Keep track of all Sending and Receiving objects, as well as threads
	//Has a queue of the last 100 messages
	protected Vector<Sending> _senders = new Vector<Sending>();
	protected Vector<Receiving> _receivers = new Vector<Receiving>();
	protected Vector<ThreadPair> _threads = new Vector<ThreadPair>();
	protected LimitedQueue<Integer> _previousMessages = new LimitedQueue<Integer>(100);
	
	//Two constructors, takes the id, and totalNode count (for random messages), optional debugging switch
	public Node (int id, int totalNodes) {
		ID = id;
		_totalNodes = totalNodes;
	}
	
	public Node (int id, int totalNodes, boolean debugMessages) {
		ID = id;
		_totalNodes = totalNodes;
		_debugMessages = debugMessages;
	}
	
	//Given a channel to use, create a sender and receiver
	public void useChannel(BiDirectionalChannel<Message> channel) {
		Sending sender = new Sending(channel);
		Receiving receiver = new Receiving(channel);
		_senders.add(sender);
		_receivers.add(receiver);
		Thread senderThread = new Thread(sender);
		Thread receiverThread = new Thread(receiver);
		try {
			channel.registerThreads(senderThread, receiverThread);
		} catch (IllegalAccessException e) {
			System.err.println("Error setting up channel");
			e.printStackTrace();
			System.exit(1);
		}
		_threads.add(new ThreadPair(senderThread, receiverThread));
	}
	
	//activateSenders creates a thread for each sender, and starts it
	public void activateSendersAndReceivers() {
		for (int i = 0; i < _threads.size(); i++) {
			Thread senderThread = _threads.get(i).SENDER;
			Thread receiverThread = _threads.get(i).RECEIVER;
			senderThread.start();
			receiverThread.start();
		}
	}
	
	//Given a message, checks if this node is the receiver.
	//	If it is, and it hasn't received it yet, add it to the list of previous messages, and print it out
	//	Else, broadcast the message
	//Lots of debugging prints in here as well
	protected synchronized void handleMessage(Message newMessage) {
		if (newMessage.destination == ID) {
			if (!(_previousMessages.contains(newMessage.messageId))) {
				_previousMessages.add(newMessage.messageId);
				if (_debugMessages) {
					System.out.println("Node " + ID + " received message from node " + newMessage.from
							+ ", for itself at " +
							Message.formatTime(System.currentTimeMillis()) + ": \'" + newMessage.msg + "\'");
				}
				else {
					System.out.println("Node " + ID + " received message from node " + newMessage.source
							+ " at " + Message.formatTime(System.currentTimeMillis()) + ": \'" + newMessage.msg + "\'");
				}
			}
			else {
				if (_debugMessages) {
					System.out.println("Node " + ID + " received message from node " + newMessage.from
							+ ", for itself at " +
							Message.formatTime(System.currentTimeMillis()) + ": \'" + newMessage.msg + "\' which it already received");
				}
			}
		}
		else {
			broadCastMessage(newMessage);
		}
	}
	
	//If we haven't seen the message before, give each sender a copy (changing the from to us, for debugging info)
	//add to our previous messages
	//Again, lots of debugging prints in here make it seem more complicated than it is
	public synchronized void broadCastMessage(Message newMessage) {
		if (_previousMessages.contains(newMessage.messageId)) {
			if (_debugMessages) {
				System.out.println("Node " + ID + " received message from node " + newMessage.from
						+ ", for node " + newMessage.destination + " at " +
						Message.formatTime(System.currentTimeMillis()) + ": \'" + newMessage.msg + "\' which it already sent out");
			}
			return;
		}
		
		for (int i = 0; i < _senders.size(); i++) {
			Message copyMessage = new Message(newMessage, ID);
			_senders.get(i).addMessage(copyMessage);
		}
		_previousMessages.add(newMessage.messageId);
		
		if (_debugMessages) {
			if (ID == newMessage.from) {
				System.out.println("Node " + ID + " created a message for node " + newMessage.destination + " at " +
						Message.formatTime(System.currentTimeMillis()) + ": \'" + newMessage.msg + "\'");
			}
			else {
				System.out.println("Node " + ID + " received message from node " + newMessage.from
						+ ", for node " + newMessage.destination + " at " +
						Message.formatTime(System.currentTimeMillis()) + ": \'" + newMessage.msg + "\'");
			}
		}
	}
	
	//Stops the node from generating messages
	public void stopRunning() throws InterruptedException {
		_run = false;
	}
	
	//While running, generate a message to a random node, and broadcast it
	@Override
	public void run() {
		while (_run) {
			int nodeToSendMessageTo = _r.nextInt(_totalNodes + 1);
			
			if (nodeToSendMessageTo != ID) {	
				Message newMessage = new Message(ID, nodeToSendMessageTo, "" + Message.formatTime(System.currentTimeMillis()));	
				broadCastMessage(newMessage);
			}
			
			try {
				Thread.sleep(_r.nextInt(100) + 1);
			} catch (InterruptedException e) {
				System.err.println("Node " + ID + " couldn't sleep.");
				e.printStackTrace();
			}
		}
	}
	
	//Handles all the sending
	private class Sending implements Runnable {

		BiDirectionalChannel<Message> _channel;
		Vector<Message> _toSend = new Vector<Message>();
		
		public Sending(BiDirectionalChannel<Message> channel) {
			_channel = channel;
		}
		
		//Add a message to the queue of messages this thread should send out
		public void addMessage(Message newMessage) {
			synchronized (_toSend) { _toSend.add(newMessage); }
		}
		
		@Override
		//If there is a message, send it, and remove it from queue, then sleep for 100ms
		public void run() {
			while (_run) {
				synchronized (_toSend) {
					if (_toSend.size() != 0) {
						try {
								_channel.put(_toSend.get(0));
								_toSend.remove(0);
						} catch (IllegalAccessException e) {
							System.err.println("Sender for Node " + ID + " could not access it's channel");
							e.printStackTrace();
							System.exit(1);
						} catch (InterruptedException e) {
							System.err.println("Sender for Node " + ID + " was interrupted while putting");
							e.printStackTrace();
						}
						
					}
				}
				
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					System.err.println("Sender for Node " + ID + " was interrupted while sleeping");
					e.printStackTrace();
				}
			}
		}
		
	}
	
	private class Receiving implements Runnable {

		BiDirectionalChannel<Message> _channel;
		
		public Receiving(BiDirectionalChannel<Message> channel) {
			_channel = channel;
		}
		
		@Override
		//Try to take a message, once we have one, tell our node to handle it
		public void run() {
			while (_run) {
				try {
					Message newMessage = _channel.take();
					handleMessage(newMessage);
					Thread.sleep(100);
				} catch (IllegalAccessException e) {
					System.err.println("Receiver for Node " + ID + " could not access it's channel.");
					e.printStackTrace();
					System.exit(1);
				} catch (InterruptedException e) {
					System.err.println("Receiver for Node " + ID + " was interrupted");
					e.printStackTrace();
				}
			}
		}
		
	}
}
