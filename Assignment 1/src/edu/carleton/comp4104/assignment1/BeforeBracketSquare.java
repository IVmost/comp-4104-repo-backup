package edu.carleton.comp4104.assignment1;

public class BeforeBracketSquare extends AbstractCharacterPrintable {

	//Has a value called passCanPrint, that means, if it was chosen to print, everything after it prints
	//On prints itself if debugging.
	
	//So, in the scenario {[]}, if [ is the first to print after {, it tells everything between [] that they can print
	//If [ is not the first, then everything between [] won't print
	
	protected boolean passCanPrint = false;
	
	public BeforeBracketSquare(char characterToPrint, AbstractCharacterPrintable characterBefore) {
		super(characterToPrint, characterBefore);
		
	}

	@Override
	public synchronized void printCharacter() {
		//Normally print nothing, below for debugging
		if (debugging) {
			System.out.print("[");
		}
		passCanPrint = true;
	}

	@Override
	public synchronized boolean canNextCharPrint() {
		return passCanPrint;
	}

}
