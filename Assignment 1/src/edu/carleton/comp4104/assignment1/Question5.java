package edu.carleton.comp4104.assignment1;

import java.util.Date;

public class Question5 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.print("\n\njava Question5 ");
		for (int x=0;x<args.length; x++){
			System.out.print(" "+ args[x]);
		}
		System.out.println("\n");
		
		//parser that parses user input
		Question5Parser parser = new Question5Parser();
		//start time of the simulation
		long startTime = new Date().getTime();
		
		//parse the string input
		int[] input = parser.parse(args);
	
		//create the salon with the appropriate number of seats
		
		//if input was parsed to null than invalid input was given
		if (input == null) {
			System.out.println("Invalid Input");
		} else {
			Q5Salon salon = new Q5Salon(startTime,input[4]);
			//creates all the customers for the program
			for (int x = 0; x < input[0]; x++) {
				new Q5Customer(x + 1, input[2], salon, startTime, input[5])
						.start();
			}
			//creates all the barbers for the  program
			for (int x = 0; x < input[1]; x++) {
				new Q5Barber(x + 1, input[3], salon, startTime, input[5]).start();
			}
		}
	}

}
