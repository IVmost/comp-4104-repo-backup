package edu.carleton.comp4104.assignment1;

public class BeforeBracketSquigly extends AbstractCharacterPrintable {

	//Has a value called passCanPrint, that means, if it was chosen to print, everything after it prints
	//On prints itself if debugging.
	//The first character to ask it if it can print is allowed, but anyone after, is told not to
	
	//So, in the scenario {{}}, if the second { is the first to print after the first {,
	//it lets something between the inner {} print, if the second { is not the first, then everything between 
	//the inner {} won't print
	
	protected boolean someOneHasPrinted = false;
	protected boolean canPassPrint = false;
	
	public BeforeBracketSquigly(char characterToPrint, AbstractCharacterPrintable characterBefore) {
		super(characterToPrint, characterBefore);
	}

	@Override
	public synchronized void printCharacter() {
		//Normally print nothing, below for debugging
		if (debugging) {
			System.out.print("{");
		}
		canPassPrint = true;
	}

	@Override
	public synchronized boolean canNextCharPrint() {
		if (!someOneHasPrinted) {
			someOneHasPrinted = true;
			return canPassPrint;
		}
		
		return false;
	}

}
