package edu.carleton.comp4104.assignment1;

import java.util.Date;
import java.util.Random;

//barber entity
public class Q5Barber implements Runnable {
	//id number of the barber
	private int barberNo;
	//max time that the barber will cut hair
	private final long maxCutTime;
	//time of the simulation to end
	private final long endTime;
	//flag that the simulation is running
	private boolean running;
	
	//the salon monitor object
	private Q5Salon salon;
	
	//time that the simulation started
	private final long startTime;

	//constructor
	public Q5Barber(int barberNo, long maxCutTime, Q5Salon salon, long startTime,
			long endTime) {
		this.barberNo = barberNo;
		this.maxCutTime = maxCutTime;
		this.running = true;
		this.salon = salon;
		this.startTime = startTime;
		this.endTime = endTime;
	}

	//starts the thread
	public void start() {
		new Thread(this, "Barber " + barberNo).start();

	}

	@Override
	public void run() {
		//random number generator for cut time
		Random random = new Random();
		
		//loop for the simulation
		while (running == true) {
			
			
			//current barber is free
			long curTime = new Date().getTime() - startTime;
			System.out.println("Time=" + curTime + ", "
					+ Thread.currentThread().getName()
					+ " free,  getting Customer. ");
			
			
			//time it will take for barber to cut hair
			long millis = Math.abs(random.nextLong()) % maxCutTime + 1;
			
			//time that has passed
			curTime = new Date().getTime() - startTime;
			
			//checks if simulation time has ended
			if (curTime < endTime) {
				//enter the salon a "get" a customer
				running = salon.barberReady(millis);
				
				//cut customer hair
				curTime = new Date().getTime() - startTime;
				System.out.println("Time=" + curTime + ", "
						+ Thread.currentThread().getName()
						+ " cutting hair for " + millis + "ms");
				
				try {
					//sleep for the hair cutting time
					Thread.sleep(millis);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else
				//abort the simulation if time is up
				running=false;
		}
		//notify the monitor that the simulation is over
		salon.endSimulation();
		//print a thread complete message
		System.out.println(Thread.currentThread().getName()+" Complete");
	}

}
