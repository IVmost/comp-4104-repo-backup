package edu.carleton.comp4104.assignment1;

import java.util.Iterator;
import java.util.Random;
import java.util.Vector;

public class Eater implements Runnable {

	protected Food _ingredient;
	protected Control _monitorControl;
	protected int _eatTime;
	protected Random _r = new Random();
	
	public Eater(Food ingredient, Control monitorControl, int eatTime) {
		_ingredient = ingredient;
		_monitorControl = monitorControl;
		_eatTime = eatTime;
	}
	
	protected void makeFood(Vector<Food> ingredientsToUse) throws RuntimeException{
		Food potato = null;
		Food water = null;
		Food butter = null;
		
		//Loop through Food vector, making sure we have each ingredient
		Iterator<Food> foodIter = ingredientsToUse.iterator();
		while(foodIter.hasNext()) {
			Food item = foodIter.next();
			switch (item) {
			case POTATO:
				potato = item;
				break;
			case WATER:
				water = item;
				break;
			case BUTTER:
				butter = item;
				break;
			default:
				break;
			}
		}
		
		//If any are null, throw exception
		if (potato == null || water == null || butter == null) throw new RuntimeException("Incorrect ingredients.");
	}
	
	@Override
	public void run() {
		
		boolean keepRunning = true;
		
		while (keepRunning) {
			//Get ingredients, if null, then the simulation is over
			Vector<Food> takenIngredients = _monitorControl.getIngredients(_ingredient);
			
			if (takenIngredients == null) break;
			
			//Add our ingredient, and try to make food
			takenIngredients.add(_ingredient);
			
			try {
				makeFood(takenIngredients);
			} catch (RuntimeException e) {
				System.err.println("Couldn't make food, Error: " + e.getMessage());
				e.printStackTrace();
				System.exit(1);
			}
			
			//sleep to 'eat' food
			int sleepTime = _r.nextInt(_eatTime * 1000);
			try {
				Thread.sleep(sleepTime);
			} catch (InterruptedException e) {
				System.err.println("Stopped eating early.");
				e.printStackTrace();
			}
			System.out.println("Eater had " + _ingredient + ", picked up " + takenIngredients.get(0) + " and "
					+ takenIngredients.get(1) + " and ate food in " + String.format("%.2f", (double)sleepTime / 1000) + " seconds.");
			
			//we're done eating
			_monitorControl.doneEating();
		}
	}

}
