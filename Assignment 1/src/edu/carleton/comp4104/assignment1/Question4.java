package edu.carleton.comp4104.assignment1;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Question4 {
	// static string which is the file directory of the input files
	static String filelocation = System.getProperty("user.dir")
			+ "\\src\\edu\\carleton\\comp4104\\assignment1\\";

	public static void main(String[] args) throws IOException {

		// checks if enough arguments were given
		
		System.out.println("\n_______________________________________________________________________________");
		if (args.length < 4) {
			System.out.println("Invalid input is too short");
			return;
		}
		// prints the arguments given
		System.out.print("java Question4 ");
		for (int x = 0; x < args.length; x++) {
			System.out.print(args[x] + " ");
		}
		System.out.println();

		// checks to see if the first command is the file name
		if (!args[0].matches("-[cC]")) {
			System.out.println("Invalid Input: " + args[0] + " must be -c");
			return;
		}

		// checks to see if the input file exists
		if (!(new File(filelocation + args[1]).isFile())) {
			System.out.println("File does not exist: " + args[1]);
			return;
		}
		// checks to see if the build command was given
		if (!args[2].matches("-[bB]")) {
			System.out.println("Invalid Input: " + args[2] + " must be -b");
			return;
		}

		// builds a list of the parts to be built
		ArrayList<String> requiredPartsList = new ArrayList<String>();
		for (int x = 3; x < args.length; x++) {
			if (!args[x].matches("^[A-Za-z]$")) {
				System.out.println("Invalid Input: \"" + args[x]
						+ "\" must be a single Letter upper or lower case");
				return;
			}
			if (!requiredPartsList.contains(args[x]))
				requiredPartsList.add(args[x]);
			else{
				System.out.println("Invalid input: " + args[x] + " has already been chosen.");
				return;
			}
				
		}

		// get the path of the file
		Path path = Paths.get(filelocation + args[1]);
		//read all the lines in the file to a list JAVA 1.7 feature
		List<String> input = Files.readAllLines(path, StandardCharsets.UTF_8);

		//hash map for a component which needs certain parts
		HashMap<String, String> hasPartMap = new HashMap<String, String>();
		
		//Arraylist of all the components that need certain parts
		ArrayList<String> hasPartList = new ArrayList<String>();
		
		//hashmap of all the components which must be built before a certain part
		HashMap<String, String> afterMap = new HashMap<String, String>();
		//Arraylist of all the components that must be built before a certain part
		ArrayList<String> afterList = new ArrayList<String>();

		System.out.println("File " + args[1]+ " contents:");
		//loop for parsing the file
		for (String in : input) {
			//lines of the file for testing 
			System.out.println(in);
			//handles items which has-parts
			if (in.matches(".*has-parts.*")) {
				//splits the line by the parts which need components and the parts needed
				String[] seperate = in.split("has-parts");
				//eliminates any extra whitespace.
				seperate = trimArray(seperate);
				//splits the components which need parts, this allows for multiple parts to preceed a has-parts statement
				String[] needsParts = seperate[0].split(" ");
				//loop for creating each has Parts entry
				for (int x = 0; x < needsParts.length; x++) {
					//check if a hasPartMap already exists
					if (hasPartMap.containsKey(needsParts[x])) {
						//gets the already existing parts needed 
						String current = hasPartMap.get(needsParts[x]);
						//adds combines the new parts needed with the already existing parts
						hasPartMap.put(needsParts[x],
								mergeStrings(current, seperate[1]));
					} else {
						//create a new entry to the hasParts map
						hasPartMap.put(needsParts[x], seperate[1]);
						//add entry to the hasPartList
						hasPartList.add(needsParts[x]);
					}
				}
				//handles the before case
			} else if (in.matches(".*before.*")) {
				//split the before and after situation
				String[] seperate = in.split("before");
				//trims extra whitespace
				seperate = trimArray(seperate);
				//use the parts which need to go after certain components
				String[] afterParts = seperate[1].split(" ");
				for (int x = 0; x < afterParts.length; x++) {
					//checks if entry already exists
					if (afterMap.containsKey(afterParts[x])) {
						//adds to preexisting entries the extra requirment
						String current = afterMap.get(afterParts[x]);
						afterMap.put(afterParts[x],
								mergeStrings(current, seperate[0]));
					} else {
						//create a new entry
						afterMap.put(afterParts[x], seperate[0]);
						afterList.add(afterParts[x]);
					}
				}
			} else {
				//rejects incorrect file input
				System.out.println("invalid file input: " + in);
				return;
			}

		}
		
		//hasmap for the created components objects
		HashMap<String, Q4Component> madeMap = new HashMap<String, Q4Component>();
		//list of the component objects for running a thread
		ArrayList<Q4Component> componentList = new ArrayList<Q4Component>();
		
		//list of the names of the already existing components
		ArrayList<String> componentNameList = new ArrayList<String>();
		
		//create the required components without any barriers
		for (String element : requiredPartsList) {
			//check if a component does not exist
			if (!madeMap.containsKey(element)) {
				//create new component with the name in element
				Q4Component component = new Q4Component(element);
				//place component in the made map
				madeMap.put(element, component);
				//place component in the component list
				componentList.add(component);
				//place the component name in the component name list
				componentNameList.add(element);
			}
		}

		//builds the barriers for components that have parts
		//uses a for loop to add new components to the required list
		//which will prevent missing required parts
		for (int y = 0; y < requiredPartsList.size(); y++) {
			//get the current component name
			String element = requiredPartsList.get(y);
			//checks if the component needs parts
			if (hasPartMap.containsKey(element)) {
				//get all the pieces a part requires
				String[] required = hasPartMap.get(element).split(" ");
				//create a barrier which requires the same number as the required pieces
				Q4DualEntryBarrier barrier = new Q4DualEntryBarrier(
						required.length);
				//checks if the component has been made yet
				if (!madeMap.containsKey(element)) {
					//new component is made if it does not exist
					Q4Component component = new Q4Component(element);
					//adds the barrier and a needed parts barrier
					component.addPartsNeeded(barrier);
					//add an entry to the mademap
					madeMap.put(element, component);
					//add component to the component list
					componentList.add(component);
					//add component name to the list
					componentNameList.add(element);
				} else {
					//get the existing component
					Q4Component component = madeMap.get(element);
					//add the barrier to the existing component
					component.addPartsNeeded(barrier);
				}
				//for loop for adding the barrier to the needed components
				for (int x = 0; x < required.length; x++) {
					//check if component does not exist
					if (!madeMap.containsKey(required[x])) {
						//create new component
						Q4Component component = new Q4Component(required[x]);
						//add barrier to the component as a part of a component
						component.addPartOf(barrier);
						//add component to the mademap
						madeMap.put(required[x], component);
						//add component to the list
						componentList.add(component);
						//add the name to the list
						componentNameList.add(required[x]);
						//add the name to the list of required parts on creation
						//this will allow the loop to check for pieces required to make
						//of component pieces
						requiredPartsList.add(required[x]);
					} else {
						//update existing component
						Q4Component component = madeMap.get(required[x]);
						component.addPartOf(barrier);
					}
				}
			}
		}

		//builds barriers for components that must be built after other components
		for (String element : componentNameList) {
			//checks if a component must be built after an other component
			if (afterMap.containsKey(element)) {
				//split the components that must be built before
				String[] required = afterMap.get(element).split(" ");
				//create barrier
				Q4DualEntryBarrier barrier = new Q4DualEntryBarrier(
						required.length);
				//checks if component does not yet exist(could be deprecated but exists for redundancy)
				if (!madeMap.containsKey(element)) {
					//create components and barrier for parts before
					Q4Component component = new Q4Component(element);
					component.addPartsBefore(barrier);
					madeMap.put(element, component);
					componentList.add(component);
				} else {
					//update component
					Q4Component component = madeMap.get(element);
					component.addPartsBefore(barrier);
				}
				//loop for the components which must be before the current component
				for (int x = 0; x < required.length; x++) {
					//checks if the component has been create yet 
					if (!madeMap.containsKey(required[x])) {
						//checks if the component is actually required for the construction goal
						if (requiredPartsList.contains(required[x])) {
							//create component if it is required
							Q4Component component = new Q4Component(required[x]);
							component.addPartsAfter(barrier);
							madeMap.put(required[x], component);
							componentList.add(component);
						}else
						{
							//if the component is not required it decrease the count for it's entry
							barrier.controllerEntry();
						}
					} else {
						//update an exist component with the new barrier
						Q4Component component = madeMap.get(required[x]);
						component.addPartsAfter(barrier);
					}
				}
			}
		}
		//start all threads
		for (Q4Component component : componentList) {
			component.start();
		}

		//join with all the threads so that main can wait until all threads are done building
		for (Q4Component component : componentList) {
			component.join();
		}
		//gives the done message
		System.out.println("DONE!");
	}

	//method for merging two strings
	private static String mergeStrings(String current, String append) {
		String[] appendList = append.trim().split(" ");
		for (int x = 0; x < appendList.length; x++) {
				current += " " + appendList[x].trim();
			
		}
		return current;
	}

	//method for eliminating any extra whitespace from an array of strings
	private static String[] trimArray(String[] seperate) {
		for (int x = 0; x < seperate.length; x++)
			seperate[x] = seperate[x].trim();
		return seperate;
	}
}
