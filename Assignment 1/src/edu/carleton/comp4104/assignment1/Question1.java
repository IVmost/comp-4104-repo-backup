package edu.carleton.comp4104.assignment1;

import java.util.ArrayList;

public class Question1 {

	public static void main(String args[]) {
		
		//Given a string "A [ B C { D E } ] F" the concept is:
		// A waits for no one, it runs
		// [ waits for A
		// B, C and { are waiting for [, thus they can beat each other, but they all print at some point
		// D and E are waiting for {, and the first one to go through is allowed to print
		// } is waiting for both D and E to finish, even though only one prints
		// ] is waiting for B C { D E and } to finish before it can go (an improvement would be to have it wait for just B C { and }
		// F is waiting for ]
		
		//String testingValue = "A [ B C [ D E ] ] F";
		//String testingValue = "A { B C { D E } } F";
		//String testingValue = "A { B C [ D E ] } F";
		//String testingValue = "A [ B C { D E } ] F";
		
		//String[] values = testingValue.split(" ");
		String[] values = args[1].split(" ");
		
		System.out.print("Input: ");
		
		for (int i = 0; i < values.length; i++) {
			System.out.print(values[i]);
		}
		
		System.out.println();
		
		//System.out.println("Running test for 20 iterations");
		
		//for (int a = 0; a < 20; a++) {
		
		//We note the before char to be null, and have a list of characters
		AbstractCharacterPrintable beforeCharacter = null;
		ArrayList<AbstractCharacterPrintable> characters = new ArrayList<AbstractCharacterPrintable>();
		
		//We parse the input information, brackets are handled specially, and we skip parsing everything in between brackets,
		//	that's handled by the bracketWork method.
		//Characters are created here, waiting for the beforeCharacter to finish
		//After each character, or section of characters are handled, the beforeChar is updated.
		for (int i = 0; i < values.length; i++) {
			if (values[i].equals("[")) {
				i = bracketWork(beforeCharacter, values, characters, i, "[", "]");
				beforeCharacter = characters.get(characters.size() - 1);
			}
			else if (values[i].equals("{")) {
				i = bracketWork(beforeCharacter, values, characters, i, "{", "}");
				beforeCharacter = characters.get(characters.size() - 1);
			}
			else {
				CharacterPrintable charToPrint = new CharacterPrintable(values[i].charAt(0), beforeCharacter);
				characters.add(charToPrint);
				beforeCharacter = charToPrint;
			}
		}
		
		//Set characters up in a thread, and run them
		ArrayList<Thread> threadList = new ArrayList<Thread>();
		
		for (int i = 0; i < characters.size(); i++) {
			Thread newThread = new Thread(characters.get(i));
			threadList.add(newThread);
			newThread.start();
		}
		
		//Join all the threads to end the program
		for (int i = 0; i < characters.size(); i++) {
			try {
				threadList.get(i).join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		System.out.println();
		
		//}
		
		//System.out.println();
	}
	
	public static int bracketWork(AbstractCharacterPrintable listHead, String[] subList, 
			ArrayList<AbstractCharacterPrintable> inCharList, int curPos, 
			String beforeBracketString, String afterBracketString) {
		AbstractCharacterPrintable beforeBracket;
		
		//Depending on the bracket type, we create the begining bracket, which everything is waiting for (the beforeCharacter)
		
		if (beforeBracketString.equals("[")) {
			beforeBracket = new BeforeBracketSquare(beforeBracketString.charAt(0), listHead);
		}
		else {
			beforeBracket = new BeforeBracketSquigly(beforeBracketString.charAt(0), listHead);
		}
		
		inCharList.add(beforeBracket);
		
		//Get selection to end of matching bracket
		// we count the open brackets, to make sure we get the proper segment, and not {{{} for example
		int k = -1;
		int newOpenBrackets = 0;
		for (int j = curPos; j < subList.length; j++) {
			if (subList[j].equals(beforeBracketString)) {
				newOpenBrackets++;
			}
			else if (subList[j].equals(afterBracketString)) {
				if (newOpenBrackets == 1) {
					k = j;
					break;
				}
				else {
					newOpenBrackets--;
				}
			}
		}
		
		//Create a new sub list
		String[] newSubList = new String[(k-curPos-1)];
		
		for (int j = curPos + 1; j < k; j++) {
			newSubList[j - curPos - 1] = subList[j];
		}
		
		//Get characters in between by calling parseSubList, which, can in turn call this method again, for recursion
		ArrayList<AbstractCharacterPrintable> subCharacters = parseSubList(beforeBracket, newSubList);
		
		//Add characters to master list
		for (int j = 0; j < subCharacters.size(); j++) {
			inCharList.add(subCharacters.get(j));
		}
		
		//Do closing bracket
		AfterBracket afterBracket = new AfterBracket(afterBracketString.charAt(0), subCharacters);
		inCharList.add(afterBracket);
		
		//Return new position
		return k;
	}
	
	//The recursive part of the character parsing, is called for each bracket section, and sub bracket section
	//So given A [ B { C D } E ] F, the main (bracketWork really) calls parseSubList on B { C D } E,
	//	and parseSubList (through bracketWork) calls parseSubList on C D
	public static ArrayList<AbstractCharacterPrintable> parseSubList(AbstractCharacterPrintable listHead, String[] subList) {
		
		// similar to what's found in the main method, but the last character is not updated here
		ArrayList<AbstractCharacterPrintable> characters = new ArrayList<AbstractCharacterPrintable>();
		
		for (int i = 0; i < subList.length; i++) {
			if (subList[i].equals("[")) {
				i = bracketWork(listHead, subList, characters, i, "[", "]");
			}
			else if (subList[i].equals("{")) {
				i = bracketWork(listHead, subList, characters, i, "{", "}");
			}
			else {
				CharacterPrintable charToPrint = new CharacterPrintable(subList[i].charAt(0), listHead);
				characters.add(charToPrint);
			}
		}
		
		return characters;
	}
}
