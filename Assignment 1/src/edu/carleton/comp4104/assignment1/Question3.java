package edu.carleton.comp4104.assignment1;

public class Question3 {


	public static void main(String args[]) {
		
		//Parse input
		int simulationTime = 30;
		int eatTime = 2;
		
		//we expect 4 arguments
		if (args.length != 4) {
			System.err.println("Incorrect number of arguments");
			System.exit(1);
		}
		
		System.out.println("Parsing arguments");
		
		//Loop through each pair of arguments, and handle, if unexpected argument, print error, and quit
		try {
			for (int i = 0; i < args.length; i+=2) {
				String key = args[i];
				if (key.equals("-t")) {
					simulationTime = Integer.parseInt(args[i+1]);
				}
				else if (key.equals("-r")) {
					eatTime = Integer.parseInt(args[i+1]);
				}
				else {
					System.err.println("Error in arguments: don't recognize key: " + key);
					System.exit(1);
				}
			}
		} catch (NumberFormatException e) {
			System.err.println("Could not convert argument to number: " + e.getMessage());
			System.exit(1);
		}
		
		System.out.println("... simulation time is: " + simulationTime);
		System.out.println("... eat time can be up to: " + eatTime);
		
		System.out.println("Running simulation:");
		
		//Create simulation
		Control controller = new Control();
		Agent agent = new Agent(controller);
		Eater eaterWithPotato = new Eater(Food.POTATO, controller, eatTime);
		Eater eaterWithWater = new Eater(Food.WATER, controller, eatTime);
		Eater eaterWithButter = new Eater(Food.BUTTER, controller, eatTime);
		
		//Run simulation
		new Thread(eaterWithPotato).start();
		new Thread(eaterWithWater).start();
		new Thread(eaterWithButter).start();
		new Thread(agent).start();
		
		try {
			Thread.sleep(simulationTime * 1000);
		} catch (InterruptedException e) {
			System.err.println("Simulation ended earlier than expected");
			e.printStackTrace();
		}
		
		System.out.println("Done Simulation");
		System.exit(0);
	}
	
}
