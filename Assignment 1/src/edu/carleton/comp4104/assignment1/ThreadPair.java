package edu.carleton.comp4104.assignment1;

public class ThreadPair {

	public final Thread SENDER;
	public final Thread RECEIVER;
	
	public ThreadPair(Thread sender, Thread receiver) {
		SENDER = sender;
		RECEIVER = receiver;
	}
	
}
