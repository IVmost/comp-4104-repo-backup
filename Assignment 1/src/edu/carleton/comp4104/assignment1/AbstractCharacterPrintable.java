package edu.carleton.comp4104.assignment1;

import java.util.Random;

public abstract class AbstractCharacterPrintable implements Runnable {

	//This class represents what the characters should do.
	//	They have a character to print, another ACP to wait for, their own latch, and a random element
	//	They expect a char, and the character to wait for in the constructor
	//	Each concrete implementation requires the printChracter and canNextCharPrint methods to be written
	//		Normal chars print, brackets only in debug, and squigly left blocks some chars from printing, see concrete classes
	//	They give access to their latch, and have a run method
	//		The run method simply sleeps for a random time, tries to acquire the before char latch (if there is one)
	//		If the beforeChar (if there is one) lets us print, we print, then we release our latch, simple.
	
	protected char _characterToPrint;
	protected AbstractCharacterPrintable _characterBefore;
	protected Latch _ourLatch = new Latch();
	protected java.util.Random _r = new Random();
	protected boolean debugging = false;
	
	/**
	 * 
	 * @param characterToPrint
	 * @param characterBefore Can be null
	 */
	public AbstractCharacterPrintable(char characterToPrint, AbstractCharacterPrintable characterBefore) {
		_characterToPrint = characterToPrint;
		_characterBefore = characterBefore;
	}
	
	public abstract void printCharacter();
	public abstract boolean canNextCharPrint();

	public Latch getLatch() {
		return _ourLatch;
	}
	
	@Override
	public void run() {
		try {
			//First sleep for 0 to 100ms
			int i = _r.nextInt(100);
			Thread.sleep(i);
			
			//Acquire latch
			if (_characterBefore != null) {
				_characterBefore.getLatch().acquire();
			}
			//Print our character
			if (_characterBefore == null || _characterBefore.canNextCharPrint()) {
				printCharacter();
			}
			//Release our latch
			_ourLatch.release();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		
	}	
}
