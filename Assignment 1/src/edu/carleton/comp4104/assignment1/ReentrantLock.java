package edu.carleton.comp4104.assignment1;

import com.sun.corba.se.impl.orbutil.concurrent.Sync;

public class ReentrantLock implements Sync {
	private boolean locked;
	private long threadID;

	public ReentrantLock() {
		locked = false;
		threadID = -1;
	}

	@Override
	public synchronized void acquire() throws InterruptedException {
		long currentID = Thread.currentThread().getId();
		while (locked && threadID != currentID)
			wait();
		locked = true;
		threadID = currentID;

	}

	@Override
	public boolean attempt(long arg0) throws InterruptedException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public synchronized void release() {
		if (Thread.currentThread().getId() == threadID) {
			locked = false;
			threadID = -1;
			notify();
		}
	}

}
