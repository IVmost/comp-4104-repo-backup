package edu.carleton.comp4104.assignment1;

import java.util.Iterator;
import java.util.Vector;

public class Control {

	Vector<Food> _table = new Vector<Food>();
	
	public Control() { }
	
	public synchronized Vector<Food> getIngredients(Food haveIngredient) {
		//Flag for each eater in this method
		boolean weCanUseTable = false;
		
		//Can only escape while loop is the table is not empty, and the eater in the loop can use the table
		while (_table.isEmpty() || !weCanUseTable) {
			try {
				wait();
			} catch (InterruptedException e) {
				System.err.println("getIngredients was interrupted.");
				e.printStackTrace();
				System.exit(1);
			}
			
			//Check to see if we can use the items on the table
			if (!_table.isEmpty()) {
				
				Vector<Food> totalIngredients = new Vector<Food>(_table);
				totalIngredients.add(haveIngredient);
				
				Food potato = null;
				Food water = null;
				Food butter = null;
				
				Iterator<Food> foodIter = totalIngredients.iterator();
				while (foodIter.hasNext()) {
					Food item = foodIter.next();
					switch (item) {
					case POTATO:
						potato = item;
						break;
					case WATER:
						water = item;
						break;
					case BUTTER:
						butter = item;
						break;
					default:
						break;
					}
				}
				
				if (potato != null && water != null && butter != null) weCanUseTable = true;
			}
		}
		//Can only escape while if table is not empty, and we can use it
		Vector<Food> toReturn = new Vector<Food>(_table);
		_table.clear();
		return toReturn;
	}
	
	public synchronized void placeIngredients(Vector<Food> inIngredients) {
		_table = new Vector<Food>(inIngredients);
	}
	
	public synchronized void doneEating() {
		notifyAll();
	}
	
	public synchronized void agentCanPlaceIngredients() {
		//We notify here, and not in the placeIngredients method, as a race condition can occur
		notifyAll();
		//While table is not empty, and the simulation is running, wait
		while(!_table.isEmpty()) {
			try {
				wait();
			} catch (InterruptedException e) {
				System.err.println("agentCanPlaceIngredients was interrupted.");
				e.printStackTrace();
				System.exit(1);
			}
		}
	}	
}
