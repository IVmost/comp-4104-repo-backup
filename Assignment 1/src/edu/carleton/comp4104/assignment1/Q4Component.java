package edu.carleton.comp4104.assignment1;

import java.util.ArrayList;
import java.util.Random;

public class Q4Component implements Runnable {

	// component name
	private final String name;

	// barriers for the parts each piece requires
	private ArrayList<Q4DualEntryBarrier> partOf;
	// barriers for the parts that make up the current part
	private ArrayList<Q4DualEntryBarrier> partsNeeded;
	// barrier for the parts which must be made after the current part
	private ArrayList<Q4DualEntryBarrier> partsAfter;
	// barrier for the parts which must be made before the current part
	private ArrayList<Q4DualEntryBarrier> partsBefore;
	//Thread object used for joining
	private Thread thread;

	//constructor
	public Q4Component(String name) {
		this.name = name;
		this.partOf = new ArrayList<Q4DualEntryBarrier>();
		this.partsNeeded = new ArrayList<Q4DualEntryBarrier>();
		this.partsAfter = new ArrayList<Q4DualEntryBarrier>();
		this.partsBefore = new ArrayList<Q4DualEntryBarrier>();
	}

	//adds a barrier to the partOf list
	public void addPartOf(Q4DualEntryBarrier part) {
		partOf.add(part);
		//updates the required number of runs
		updateRuns();

	}

	//adds a barrier to the partsNeeded list
	public void addPartsNeeded(Q4DualEntryBarrier part) {
		partsNeeded.add(part);
		//update the required number of runs
		updateRuns();
	}

	//adds barrier to the parts after
	public void addPartsAfter(Q4DualEntryBarrier part) {
		partsAfter.add(part);
	}

	//adds barrier to the parts before
	public void addPartsBefore(Q4DualEntryBarrier part) {
		partsBefore.add(part);
	}

	//updates the number of required runs
	private void updateRuns() {

		int runs = 0;

		//totals the number of piece needed to be built
		for (Q4DualEntryBarrier parts : partOf) {
			runs += parts.getRuns();
		}
		//check if runs hasn't increase past 0
		if (runs==0)
			runs=1;
		//update all the required parts with the number of components required
		for (Q4DualEntryBarrier parts : partsNeeded) {
			parts.setRuns(runs);
		}

	}

	//create and starts the thread
	public void start() {
		thread=new Thread(this, name);
		thread.start();
	}

	@Override
	//runs the thread
	public void run() {
		// waits for all the parts that need to be made before the current part
		for (Q4DualEntryBarrier part : partsBefore)
			part.userEntry();

		// waits for all the parts that are needed to be made for the current
		// part
		for (Q4DualEntryBarrier part : partsNeeded)
			part.userEntry();
		
		Random rand= new Random();
		// checks to see if part is needed in any parts
		if (partOf.size() == 0){
			try {
				//sleep to simulate the build time
				Thread.sleep(rand.nextInt(990)+10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println("Build " + name);
		}
		//builds all the needed parts
		for (Q4DualEntryBarrier part : partOf) {
			//builds multiple parts if a following part needs multiple pieces
			for (int x = 0; x < part.getRuns(); x++) {
				try {
					Thread.sleep(rand.nextInt(990)+10);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("Build " + name);
				part.controllerEntry();
			}
		}
		
		//notifies following parts that they now maybe built
		for (Q4DualEntryBarrier part : partsAfter)
			part.controllerEntry();

	}

	public void join() {
		//uses join so main can wait for threads to finish
		try {
			thread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
