package edu.carleton.comp4104.assignment1;

import java.util.ArrayList;

public class AfterBracket extends AbstractCharacterPrintable {

	//The afterBracket's major difference, is that is takes an array of ACPs
	//It's print method only prints if it's debugging
	//It always tell the next character that it can print
	//The run method is overriden.
	//	It runs basicly the same, but, acquires latches on all the chars before it, up to its matching open bracket
	//	It also prints always, if debugging is on in ACP
	
	protected ArrayList<AbstractCharacterPrintable> _characterBeforeList;
	
	public AfterBracket(char characterToPrint,
			ArrayList<AbstractCharacterPrintable> characterBeforeList) {
		super(characterToPrint, characterBeforeList.get(0));
		_characterBeforeList = characterBeforeList;
	}

	@Override
	public void printCharacter() {
		//Normally print nothing, below for debugging
		if (debugging) {
			System.out.print(_characterToPrint);
		}
	}

	@Override
	public boolean canNextCharPrint() {
		return true;
	}
	
	@Override
	public void run() {
		try {
			//First sleep for 0 to 100ms
			Thread.sleep(_r.nextInt(100));
			//Acquire latch
			for (int i = 0; i < _characterBeforeList.size(); i++) {
				_characterBeforeList.get(i).getLatch().acquire();
			}
			//Bracket doesn't print, calls method if debugging, doesn't check if it can print
			printCharacter();
			//Release our latch
			_ourLatch.release();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		
	}

}
