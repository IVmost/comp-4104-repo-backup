package edu.carleton.comp4104.assignment2.server;

import java.util.HashMap;
import java.util.Vector;

import edu.carleton.comp4104.assignment2.common.ClientChangeReason;
import edu.carleton.comp4104.assignment2.common.ClientTextResultCode;
import edu.carleton.comp4104.assignment2.common.EventHandling.AbstractEvent;
import edu.carleton.comp4104.assignment2.common.EventHandling.EventProcessor;
import edu.carleton.comp4104.assignment2.common.EventHandling.Events.ClientDataChangeEvent;
import edu.carleton.comp4104.assignment2.common.EventHandling.Events.ClientGetTextRequestEvent;
import edu.carleton.comp4104.assignment2.common.EventHandling.Events.ClientSendTextReplyEvent;
import edu.carleton.comp4104.assignment2.common.EventHandling.Events.ClientSendTextRequestEvent;

public class TextMessageManager extends EventProcessor {

	protected HashMap<String, Integer> _loggedInClients = new HashMap<String, Integer>();
	
	@Override
	protected void registerForEvents() {
		_reactor.registerHandler(ClientDataChangeEvent.EventType, this);
		_reactor.registerHandler(ClientSendTextRequestEvent.EventType, this);
	}

	@Override
	protected void processEvents(AbstractEvent event) {
		if (event instanceof ClientDataChangeEvent) {
			ClientDataChangeEvent changeEvent = (ClientDataChangeEvent)event;
			processClientChange(changeEvent);
		}
		else if (event instanceof ClientSendTextRequestEvent) {
			ClientSendTextRequestEvent clientSendTextRequestEvent = (ClientSendTextRequestEvent)event;
			processClientSendTextRequestEvent(clientSendTextRequestEvent);
		}
	}
	
	protected void processClientChange(ClientDataChangeEvent event) {
		if (event.Change == ClientChangeReason.Login) {
			_loggedInClients.put(event.ClientName, event.ClientId);
		}
		else if (event.Change == ClientChangeReason.Logout) {
			_loggedInClients.remove(event.ClientName);
		}
		/*synchronized (System.out)	{ 
			System.out.print("TextMessageManager: New Client list [ ");
			for (String client : _loggedInClients.keySet()) {
				System.out.print(client + " ");
			}
			System.out.println("]");
		}*/
	}
	
	protected void processClientSendTextRequestEvent(ClientSendTextRequestEvent event) {
		
		Vector<Integer> clientFrom = new Vector<Integer>();
		clientFrom.add(event.ClientFromId);
		
		//if we don't have that clientId, or that clientId doesn't match the name		
		if (!_loggedInClients.containsKey(event.ClientFromName) ||
				!_loggedInClients.get(event.ClientFromName).equals(event.ClientFromId)) {
			//Return error message
			_reactor.dispatch(new ClientSendTextReplyEvent(event.ClientFromName, event.ClientFromId,
					event.ClientToName, null, ClientTextResultCode.ERROR_UNKNOWN_SENDER));
			//System.out.println("TextMessageManager.processClientSendTextRequest : Sender " + 
			//		event.ClientFromName + " is unknown");
			return;
		}
		
		//if we don't have a record for the recipient, return an error
		if (!_loggedInClients.containsKey(event.ClientToName)) {
			//Return error message
			_reactor.dispatch(new ClientSendTextReplyEvent(event.ClientFromName, event.ClientFromId,
					event.ClientToName, null, ClientTextResultCode.ERROR_UNKNOWN_RECEPIENT));
			//System.out.println("TextMessageManager.processClientSendTextRequest : Sender " + 
			//		event.ClientFromName + " is known of, but Receiver " + event.ClientToName + " isn't");
			return;
		}
		
		int clientToId = _loggedInClients.get(event.ClientToName);
		//send the message to the intended recipient
		_reactor.dispatch(new ClientGetTextRequestEvent(event.ClientFromName, event.ClientToName, 
				clientToId, event.TextMessage));
		//reply to the sender, saying it worked
		_reactor.dispatch(new ClientSendTextReplyEvent(event.ClientFromName, event.ClientFromId,
				event.ClientToName, event.TextMessage, ClientTextResultCode.OK));
		//System.out.println("TextMessageManager.processClientSendTextRequest : Sender " + 
		//		event.ClientFromName + " sent to " + event.ClientToName + " message \"" + 
		//		event.TextMessage + "\"");
		ServerLogging.log(event.ClientFromName + " messaged " + event.ClientToName +
				" \"" + event.TextMessage + "\"");
		return;
	}
}
