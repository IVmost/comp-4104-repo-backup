package edu.carleton.comp4104.assignment2.server;

import java.util.Date;

public abstract class ServerLogging {

	public static void log(String s) {
		System.out.println(new Date().toString() + "\t|> " + s);
	}
	
}
