package edu.carleton.comp4104.assignment2.server;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.Properties;

import edu.carleton.comp4104.assignment2.client.Client;
import edu.carleton.comp4104.assignment2.common.Acceptor;
import edu.carleton.comp4104.assignment2.common.ConnectionType;
import edu.carleton.comp4104.assignment2.common.Reactor;
import edu.carleton.comp4104.assignment2.common.EventHandling.EventProcessor;

public class Server {

	public final static int PORT = 25565;
	
	protected Reactor _reactor;
	protected Thread _reactorThread;
	
	protected EventProcessor _defaultHandler;
	protected Thread _defaultHandlerThread;
	
	protected EventProcessor _EtoMTranslator;
	protected Thread _EtoMTranslatorThread;
	
	protected EventProcessor _MtoETranslator;
	protected Thread _MtoETranslatorThread;
	
	protected EventProcessor _loginManager;
	protected Thread _loginManagerThread;
	
	protected EventProcessor _messageManager;
	protected Thread _messageManagerThread;
	
	protected Acceptor _acceptor;
	protected Thread _acceptorThread;
	
	protected ServerSocket _socket;
	
	protected String _propertiesFile;
	protected ConnectionType _conType;
	
	protected static final boolean DEBUG_MODE = false;
	protected static final boolean DEBUG_CLIENT = true;
	
	public Server(String propertiesFile, ConnectionType conType) {
		
		_propertiesFile = propertiesFile;
		_conType = conType;
		
		_reactor = new Reactor();
		_reactorThread = new Thread(_reactor, "Server Reactor");
		
	}
	
	@SuppressWarnings("rawtypes")
	public EventProcessor loadEventProcessor(Properties properties, String key) 
			throws ClassNotFoundException, InstantiationException, IllegalAccessException, NullPointerException {
		String className = properties.getProperty(key);
		if (className == null) throw new NullPointerException("Key " + key + " was not found");
		Class classObj = Class.forName(className);
		return (EventProcessor)classObj.newInstance();
	}
	
	public void start() {
		
		try {
			_socket = new ServerSocket(PORT);
		} catch (IOException e) {
			System.err.println("Server.Server : unable to create ServerSocket on port " + PORT);
			e.printStackTrace();
			System.exit(-1);
		}
		
		_acceptor = new Acceptor(_socket, _reactor, _conType);
		_acceptorThread = new Thread(_acceptor, "Server Acceptor");
		
		Properties properties = new Properties();
		
		try {
			properties.load(new FileInputStream(_propertiesFile));
		} catch (FileNotFoundException e) {
			System.err.println("Server.start : FileNotFoundException while loading server-im.cfg");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Server.start : IOException while loading server-im.cfg");
			e.printStackTrace();
		}
		
		try {
			_defaultHandler =	loadEventProcessor(properties, "Default Handler");
			_EtoMTranslator =	loadEventProcessor(properties, "EtoM");
			_MtoETranslator =	loadEventProcessor(properties, "MtoE");
			_loginManager =		loadEventProcessor(properties, "Login Manager");
			_messageManager =	loadEventProcessor(properties, "Text Message Manager");
		} catch (ClassNotFoundException e) {
			System.err.println("Server.start : ClassNotFoundException while loading event processors");
			e.printStackTrace();
			return;
		} catch (InstantiationException e) {
			System.err.println("Server.start : InstantiationException while loading event processors");
			e.printStackTrace();
			return;
		} catch (IllegalAccessException e) {
			System.err.println("Server.start : IllegalAccessException while loading event processors");
			e.printStackTrace();
			return;
		} catch (NullPointerException e) {
			System.err.println("Server.start : NullPointerException while loading event processors - no handler for "
					+ e.getMessage());
			e.printStackTrace();
			return;
		}
		
		//Register Loaded Handlers
		_reactor.registerDefaultHandler(_defaultHandler);
		_defaultHandlerThread = new Thread(_defaultHandler, "Server Default Handler");
		
		_EtoMTranslator.registerWithReactor(_reactor);
		_EtoMTranslatorThread = new Thread(_EtoMTranslator, "Server EtoM");
		
		_MtoETranslator.registerWithReactor(_reactor);
		_MtoETranslatorThread = new Thread(_MtoETranslator, "Server MtoE");
		
		_loginManager.registerWithReactor(_reactor);
		_loginManagerThread = new Thread(_loginManager, "Server Login Manager");
		
		_messageManager.registerWithReactor(_reactor);
		_messageManagerThread = new Thread(_messageManager, "Server Text Message Manager");
		
		//Start Threads
		_reactorThread.start();
		_defaultHandlerThread.start();
		_EtoMTranslatorThread.start();
		_MtoETranslatorThread.start();
		_loginManagerThread.start();
		_messageManagerThread.start();
		_acceptorThread.start();
	}
	
	public void stop() {
		_reactor.stop();
		_acceptor.stopAccepting();
		_acceptor.endConnections();
		
		try {
			_reactorThread.join();
			_acceptorThread.interrupt();
			_acceptorThread.join();
			_defaultHandlerThread.interrupt();
			_defaultHandlerThread.join();
			_EtoMTranslatorThread.interrupt();
			_EtoMTranslatorThread.join();
			_MtoETranslatorThread.interrupt();
			_MtoETranslatorThread.join();
			_loginManagerThread.interrupt();
			_loginManagerThread.join();
			_messageManagerThread.interrupt();
			_messageManagerThread.join();
		} catch (InterruptedException e) {
			System.err.println("Server.stop : InterruptedException when joining threads");
			e.printStackTrace();
		}
	}
	
	public static void main(String args[]) {
		if (DEBUG_MODE) {
			if (DEBUG_CLIENT) {
				runClientDebug();
			}
			else {
				runServerDebug();
			}
		}
		else runNormal(args);
	}
	
	public static void runNormal(String args[]) {
		System.out.println("-----STARTING SERVER-----");
		
		//Parse arguments
		String inputFileName = null;
		ConnectionType conType = ConnectionType.ObjectOverLine;
		
		if (args.length != 2 && args.length != 4) {
			System.err.println("Incorrect number of arguments");
			System.exit(1);
		}
		
		for (int i = 0; i < args.length; i+=2) {
			String key = args[i];
			if (key.equals("-c")) {
				inputFileName = args[i+1];
			}
			else if (key.equals("-t")){
				conType= ConnectionType.valueOf(args[i+1]);
			}
			else {
				System.err.println("Error in arguments: don't recognize key: " + key);
				System.exit(1);
			}
		}
		
		Server server = new Server(inputFileName, conType);
		server.start();
	}
	
	public static void runClientDebug() {
		Client test = new Client("Test");
		test.login("134.117.101.31");
	}
	
	public static void runServerDebug() {
		
		System.out.println("-----STARTING SERVER-----");
		
		Server server = new Server("server-im.cfg", ConnectionType.ObjectOverLine);
		server.start();
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			System.err.println("Main: sleep interrupted");
			e.printStackTrace();
		}
		
		System.out.println("-----STARTING CLIENTs-----");
		
		Client alice = new Client("Alice");
		//alice.start();
		Thread aliceThread = new Thread(alice, "Client " + alice.Name);
		aliceThread.start();
		Client bob = new Client("Bob");
		//bob.start();
		Thread bobThread = new Thread(bob, "Client " + bob.Name);
		bobThread.start();
		Client charlie = new Client("Charlie");
		//charlie.start();
		Thread charlieThread = new Thread(charlie, "Client " + charlie.Name);
		charlieThread.start();
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			System.err.println("Main: sleep interrupted");
			e.printStackTrace();
		}
		
		System.out.println("-----Alice logging in-----");
		
		alice.login();
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			System.err.println("Main: sleep interrupted");
			e.printStackTrace();
		}
		
		System.out.println("-----Bob logging in-----");
		
		bob.login();
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			System.err.println("Main: sleep interrupted");
			e.printStackTrace();
		}
		
		System.out.println("-----Charlie logging in-----");
		
		charlie.login();
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			System.err.println("Main: sleep interrupted");
			e.printStackTrace();
		}
		
		System.out.println("-----Alice messaging Charlie-----");
		
		alice.sendMessage(charlie.Name, "Hello Charlie!");
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			System.err.println("Main: sleep interrupted");
			e.printStackTrace();
		}
		
		System.out.println("-----Charlie messaging Doug-----");
		
		charlie.sendMessage("Doug", "Sup Brosepth");
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			System.err.println("Main: sleep interrupted");
			e.printStackTrace();
		}
		
		System.out.println("-----Alice logging off-----");
		
		alice.logout();
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			System.err.println("Main: sleep interrupted");
			e.printStackTrace();
		}
		
		System.out.println("-----Bob logging off-----");
		
		bob.logout();
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			System.err.println("Main: sleep interrupted");
			e.printStackTrace();
		}
		
		System.out.println("-----Create doug and loggin-----");
		
		Client doug = new Client("Doug");
		//doug.start();
		Thread dougThread = new Thread(doug, "Client " + doug.Name);
		dougThread.start();
		doug.login();
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			System.err.println("Main: sleep interrupted");
			e.printStackTrace();
		}
		
		System.out.println("-----Stopping everything-----");
		
		charlie.logout();
		doug.logout();
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			System.err.println("Main: sleep interrupted");
			e.printStackTrace();
		}
		
		try {
			aliceThread.interrupt();
			aliceThread.join();
			bobThread.interrupt();
			bobThread.join();
			charlieThread.interrupt();
			charlieThread.join();
			dougThread.interrupt();
			dougThread.join();
		} catch (InterruptedException e) {
			System.err.println("Main : Couldn't join a client thread");
			e.printStackTrace();
		}
		
		server.stop();
	}
	
}
