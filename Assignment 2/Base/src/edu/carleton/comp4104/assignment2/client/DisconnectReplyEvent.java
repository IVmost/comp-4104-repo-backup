package edu.carleton.comp4104.assignment2.client;

import edu.carleton.comp4104.assignment2.common.EventHandling.AbstractEvent;

public class DisconnectReplyEvent extends AbstractEvent {

	public final static String EventType = DisconnectReplyEvent.class.getName();
	
	public final boolean Success;
	
	public DisconnectReplyEvent(boolean success) {
		super(EventType);
		Success = success;
	}

}
