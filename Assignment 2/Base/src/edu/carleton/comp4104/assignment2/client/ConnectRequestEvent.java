package edu.carleton.comp4104.assignment2.client;

import edu.carleton.comp4104.assignment2.common.EventHandling.AbstractEvent;

public class ConnectRequestEvent extends AbstractEvent {

	public final static String EventType = ConnectRequestEvent.class.getName();
	
	public ConnectRequestEvent() {
		super(EventType);
	}

}
