package edu.carleton.comp4104.assignment2.client;

import java.io.IOException;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import edu.carleton.comp4104.assignment2.common.ConnectionType;
import edu.carleton.comp4104.assignment2.common.Connector;
import edu.carleton.comp4104.assignment2.common.MessageToEventTranslator;
import edu.carleton.comp4104.assignment2.common.Reactor;
import edu.carleton.comp4104.assignment2.common.Connection.AbstractConnection;
import edu.carleton.comp4104.assignment2.common.EventHandling.AbstractEvent;
import edu.carleton.comp4104.assignment2.common.EventHandling.EventProcessor;
import edu.carleton.comp4104.assignment2.common.EventHandling.Events.ClientDataChangeEvent;
import edu.carleton.comp4104.assignment2.common.EventHandling.Events.ClientGetTextRequestEvent;
import edu.carleton.comp4104.assignment2.common.EventHandling.Events.ClientSendTextReplyEvent;
import edu.carleton.comp4104.assignment2.common.EventHandling.Events.ClientSendTextRequestEvent;
import edu.carleton.comp4104.assignment2.common.EventHandling.Events.LoginReplyEvent;
import edu.carleton.comp4104.assignment2.common.EventHandling.Events.LoginRequestEvent;
import edu.carleton.comp4104.assignment2.common.EventHandling.Events.LogoutReplyEvent;
import edu.carleton.comp4104.assignment2.common.EventHandling.Events.LogoutRequestEvent;
import edu.carleton.comp4104.assignment2.server.Server;

public class Client extends EventProcessor {

	public final String Name;
	
	protected ThreadPoolExecutor _threadPool;
	
	protected Reactor _reactor;
	protected Thread _reactorThread;
	
	protected ClientEventToMessageTranslator _EtoMTranslator;
	protected Thread _EtoMTranslatorThread;
	
	protected MessageToEventTranslator _MtoETranslator;
	protected Thread _MtoETranslatorThread;
	
	protected AbstractConnection _connection = null;
	
	protected ConnectionType _conType = ConnectionType.ObjectOverLine;
	
	public Client(String name) { 
		Name = name;
		_threadPool = new ThreadPoolExecutor(2, 6, 1, TimeUnit.MINUTES, new SynchronousQueue<Runnable>());
		
		_reactor = new Reactor();
		// ----- These are the events the client has to listen for, somewhere, anywhere
		//			Different classes can listen for different events. One class doesn't have to listen for all
		_reactor.registerHandler(LoginReplyEvent.EventType, this);				//Reply when this client logs in
		_reactor.registerHandler(LogoutReplyEvent.EventType, this);				//Reply when this client logs off
		_reactor.registerHandler(ClientDataChangeEvent.EventType, this);		//Event when another client logs in or out
		_reactor.registerHandler(ClientGetTextRequestEvent.EventType, this);	//Event when another client messages us
		_reactor.registerHandler(ClientSendTextReplyEvent.EventType, this);		//Reply when this client sends message
		// -----
		_reactorThread = new Thread(_reactor, "Client " + Name + " Reactor");
		_EtoMTranslator = new ClientEventToMessageTranslator();					//Send this client's events to server
		_EtoMTranslator.registerWithReactor(_reactor);
		_EtoMTranslatorThread = new Thread(_EtoMTranslator, "Client " + Name + " EtoM");
		_MtoETranslator = new MessageToEventTranslator();						//Translates server messages into events above
		_MtoETranslator.registerWithReactor(_reactor);
		_MtoETranslatorThread = new Thread(_MtoETranslator, "Client " + Name + " MtoE");
	}
	
	@Override
	public void registerWithReactor(Reactor reactor) {
		//Not used in this testing setup
	}
	
	@Override
	protected void registerForEvents() {
		//Not use in the testing setup
	}
	
	//This is the client listening to events from messages from the server
			//These can be spread out in your code
	@Override
	protected void processEvents(AbstractEvent event) {
		
		boolean stopClient = false;
		
		synchronized (System.out) {
			
			System.out.print("Client " + Name + " got a " + event.shortName());
			
			if (event instanceof LoginReplyEvent) {
				LoginReplyEvent loginReplyEvent = (LoginReplyEvent)event;
				System.out.print(". LOGIN REPLY --- Success: " + loginReplyEvent.Success + " Clients [ ");
				for (String clientName : loginReplyEvent.ClientNames) {
					System.out.print(clientName + " ");
				}
				System.out.print("]");
			}
			else if (event instanceof LogoutReplyEvent) {
				LogoutReplyEvent logoutReplyEvent = (LogoutReplyEvent)event;
				System.out.print(". LOGOUT REPLY --- Success: " + logoutReplyEvent.Success);
				stopClient = true;
			}
			else if (event instanceof ClientGetTextRequestEvent) {
				ClientGetTextRequestEvent messageRequest = (ClientGetTextRequestEvent)event;
				System.out.print(". Message from " + messageRequest.ClientFromName);
				System.out.print(". Message: \"" + messageRequest.TextMessage + "\"");
			}
			else if (event instanceof ClientSendTextReplyEvent) {
				ClientSendTextReplyEvent messageResponse = (ClientSendTextReplyEvent)event;
				System.out.print(". Message to: " + messageResponse.ClientToName);
				System.out.print(". Result was: " + messageResponse.Code);
			}
			else if (event instanceof ClientDataChangeEvent) {
				ClientDataChangeEvent changeEvent = (ClientDataChangeEvent)event;
				System.out.print(". Change [ " + changeEvent.Change + " ] ");
				System.out.print(". Client [ " + changeEvent.ClientName + " ]");
			}
			else {
				System.out.print(" which it doesn't handle.");
			}
			System.out.println();
		}
		
		if (stopClient) stop();
	}
	
	//These are for the client sending events, to be mesages, to be sent to the server
	//If you want to login, logout, or send a message, then you just need to use the dispatches below
	public void login() {
		start();
		_reactor.dispatch(new LoginRequestEvent(Name, -1));
	}
	
	public void login(String ip) {
		start(ip);
		_reactor.dispatch(new LoginRequestEvent(Name, -1));
	}
	
	public void logout() {
		_reactor.dispatch(new LogoutRequestEvent(Name, -1));
	}
	
	public void sendMessage(String clientTo, String message) {
		_reactor.dispatch(new ClientSendTextRequestEvent(Name, -1, clientTo, message));
	}
	
	
	//Running and stopping the demo
	public void start() {
		start("127.0.0.1");
	}
	
	public void start(String ip) {
		_reactorThread.start();
		_EtoMTranslatorThread.start();
		_MtoETranslatorThread.start();
		try {
			_connection = Connector.connect(ip, Server.PORT, _reactor, _conType, _threadPool);
		} catch (IOException e) {
			System.err.println("Client " + Name + " couldn't connect");
			e.printStackTrace();
			return;
		}
	}
	
	public void stop() {
		_reactor.stop();
		_connection.endWriter();
		_connection.endReader();
		_threadPool.shutdown();
		try {
			_reactorThread.join();
			_EtoMTranslatorThread.interrupt();
			_EtoMTranslatorThread.join();
			_MtoETranslatorThread.interrupt();
			_MtoETranslatorThread.join();
		} catch (InterruptedException e) {
			System.err.println("Client " + Name + " interrupted joing reactor and connection thread");
			e.printStackTrace();
		}
	}
}
