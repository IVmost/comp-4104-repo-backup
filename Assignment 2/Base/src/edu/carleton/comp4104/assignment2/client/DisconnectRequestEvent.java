package edu.carleton.comp4104.assignment2.client;

import edu.carleton.comp4104.assignment2.common.EventHandling.AbstractEvent;

public class DisconnectRequestEvent extends AbstractEvent {

	public final static String EventType = DisconnectRequestEvent.class.getName();
	
	public DisconnectRequestEvent() {
		super(EventType);
	}

}
