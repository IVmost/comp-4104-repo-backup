package edu.carleton.comp4104.assignment2.common.EventHandling.Events;

import edu.carleton.comp4104.assignment2.common.ClientTextResultCode;
import edu.carleton.comp4104.assignment2.common.EventHandling.AbstractEvent;

public class ClientSendTextReplyEvent extends AbstractEvent {

	public final static String EventType = ClientSendTextReplyEvent.class.getName();
	
	public final String ClientFromName;
	public final int ClientFromId;
	public final String ClientToName;
	public final String TextMessage;
	public final ClientTextResultCode Code;
	
	public ClientSendTextReplyEvent(String clientFromName, int clientFromId, 
			String clientToName, String textMessage, ClientTextResultCode code) {
		super(EventType);
		ClientFromName = clientFromName;
		ClientFromId = clientFromId;
		ClientToName = clientToName;
		TextMessage = textMessage;
		Code = code;
	}
}
