package edu.carleton.comp4104.assignment2.common.Connection.HTTP;

import java.io.ByteArrayOutputStream;
import java.net.Socket;

import edu.carleton.comp4104.assignment2.common.Connection.AbstractConnectionWriter;
import edu.carleton.comp4104.assignment2.common.Connection.HTTP.Output.HttpOutputStream;
import edu.carleton.comp4104.assignment2.common.Connection.Messages.Message;

public class HttpConnectionWriter extends AbstractConnectionWriter {

	protected ByteArrayOutputStream _byteOutputStream;
	protected HttpOutputStream _httpOutputStream;
	
	public HttpConnectionWriter(Socket peer) {
		super(peer);
	}

	@Override
	protected byte[] parseMessage(Message inMsg) {
		_byteOutputStream = new ByteArrayOutputStream();
		_httpOutputStream = new HttpOutputStream(_byteOutputStream);
		_httpOutputStream.writeMessage(inMsg);
		return _byteOutputStream.toByteArray();
	}
}

/*

protected class HttpConnectionWriter extends ConnectionWriter {
		
		public void parseRequest(Message inMsg) {
			try {
				transmitCharArray(inMsg.Cmd.toCharArray());
				_peer.getOutputStream().write(' ');
				transmitCharArray(inMsg.Name.toCharArray());
				_peer.getOutputStream().write(' ');
				String protocol = "HTTP 1.1";
				transmitCharArray(protocol.toCharArray());
				_peer.getOutputStream().write('\n');
				
				String header1 = "User-Agent:onlyClient";
				transmitCharArray(header1.toCharArray());
				_peer.getOutputStream().write('\n');
				String header2 = "Host:127.0.0.1";	
				transmitCharArray(header2.toCharArray());
				_peer.getOutputStream().write('\n');
				_peer.getOutputStream().write('\n');
			
				if (inMsg.Obj != null) {
					byte[] bytesToObj = null;
					bytesToObj = Marshaller.serializeObject(inMsg.Obj);
					_peer.getOutputStream().write(bytesToObj);
				}
				
				_peer.getOutputStream().write(-1);
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		
		public void parseReply(Message inMsg) {
			try {
				String protocol = "HTTP 1.1 200 OK";
				transmitCharArray(protocol.toCharArray());
				_peer.getOutputStream().write('\n');
				
				String header1 = "Name:" + inMsg.Name;
				transmitCharArray(header1.toCharArray());
				_peer.getOutputStream().write('\n');
				
				if (inMsg.Obj != null) {
					String header2 = "hasBody:YES";
					transmitCharArray(header2.toCharArray());
					_peer.getOutputStream().write('\n');
				}
				
				_peer.getOutputStream().write('\n');
				
				if (inMsg.Obj != null) {
					byte[] bytesToObj = null;
					bytesToObj = Marshaller.serializeObject(inMsg.Obj);
					_peer.getOutputStream().write(bytesToObj);
				}
				_peer.getOutputStream().write(-1);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		public void transmitCharArray(char[] toSend) throws IOException{
			for (int i = 0; i < toSend.length; i++) {
				_peer.getOutputStream().write(toSend[i]);
			}
		}
		
	}

*/
