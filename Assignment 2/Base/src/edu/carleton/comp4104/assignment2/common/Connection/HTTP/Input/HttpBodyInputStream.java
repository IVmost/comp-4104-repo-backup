package edu.carleton.comp4104.assignment2.common.Connection.HTTP.Input;

import java.io.IOException;
import java.io.InputStream;

import com.google.gson.Gson;

import edu.carleton.comp4104.assignment2.common.Connection.Messages.MessageBody;
import edu.carleton.comp4104.assignment2.common.Connection.Messages.MessageHeader;
import edu.carleton.comp4104.assignment2.common.Connection.Messages.MessageHeader.Command;
import edu.carleton.comp4104.assignment2.common.Connection.Messages.MessageHeader.CommandType;
import edu.carleton.comp4104.assignment2.common.Connection.Messages.MessageBodies.ClientChangesBody;
import edu.carleton.comp4104.assignment2.common.Connection.Messages.MessageBodies.ListOfClientsBody;
import edu.carleton.comp4104.assignment2.common.Connection.Messages.MessageBodies.TextMessageBody;

public class HttpBodyInputStream {

	protected InputStream _stream;
	protected Gson gson = new Gson();
	
	public HttpBodyInputStream(InputStream stream) {
		_stream = stream;
	}

	public MessageBody readMessageBody(MessageHeader header) throws IOException {
		MessageBody toReturn = null;
		
		//Messages without body
		if (
				(header.Cmd == Command.Login && header.Type == CommandType.Request) ||
				(header.Cmd == Command.Logout && header.Type == CommandType.Request)
			) {
			return toReturn;
		}
		
		//Messages with body
		String jsonBody = readString('\n');
		if (header.Cmd == Command.Login && header.Type == CommandType.Reply) {
			toReturn = gson.fromJson(jsonBody, ListOfClientsBody.class);
		}
		else if (header.Cmd == Command.Logout && header.Type == CommandType.Reply) {
			toReturn = gson.fromJson(jsonBody, MessageBody.class);
		}
		else if (header.Cmd == Command.ClientDataChange) {
			toReturn = gson.fromJson(jsonBody, ClientChangesBody.class);
		}
		else if (header.Cmd == Command.ClientGetMessage && header.Type == CommandType.Reply) {
			toReturn = gson.fromJson(jsonBody, MessageBody.class);
		}
		else if (header.Cmd == Command.ClientGetMessage || header.Cmd == Command.ClientSendMessage) {
			toReturn = gson.fromJson(jsonBody, TextMessageBody.class);
		}
		return toReturn;
	}
	
	public String readString(char untilChar) throws IOException{
		String toReturn = "";
		while (true) {
			int val = _stream.read();
			if (val != -1) {
				if ((char)val == untilChar) return toReturn;
				else toReturn += (char)val;
			}
			else throw new IOException("End of Stream");
		}
	}
}
