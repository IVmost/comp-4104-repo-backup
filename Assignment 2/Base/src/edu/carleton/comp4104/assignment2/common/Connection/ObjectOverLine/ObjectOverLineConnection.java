package edu.carleton.comp4104.assignment2.common.Connection.ObjectOverLine;

import java.net.Socket;
import java.util.concurrent.ThreadPoolExecutor;

import edu.carleton.comp4104.assignment2.common.Reactor;
import edu.carleton.comp4104.assignment2.common.Connection.AbstractConnection;
import edu.carleton.comp4104.assignment2.common.Connection.AbstractConnectionReader;
import edu.carleton.comp4104.assignment2.common.Connection.AbstractConnectionWriter;

public class ObjectOverLineConnection extends AbstractConnection {
	
	public ObjectOverLineConnection(Socket peer, ThreadPoolExecutor threadPool) {
		super(peer, threadPool);
	}

	@Override
	protected AbstractConnectionReader getConnectionReader(int clientId, Socket peer, Reactor reactor) {
		return new ObjectOverLineConnectionReader(clientId, peer, reactor, this);
	}

	@Override
	protected AbstractConnectionWriter getConnectionWriter(Socket peer) {
		return new ObjectOverLineConnectionWriter(peer);
	}
}
