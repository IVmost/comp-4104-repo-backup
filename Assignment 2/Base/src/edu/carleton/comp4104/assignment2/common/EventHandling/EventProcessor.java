package edu.carleton.comp4104.assignment2.common.EventHandling;

import java.util.concurrent.ArrayBlockingQueue;

import edu.carleton.comp4104.assignment2.common.Reactor;

public abstract class EventProcessor implements EventHandler, Runnable {

	protected boolean _keepRunning = true;
	protected Reactor _reactor;
	protected ArrayBlockingQueue<AbstractEvent> _events = new ArrayBlockingQueue<AbstractEvent>(100);
	
	@Override
	public void registerWithReactor(Reactor reactor) {
		_reactor = reactor;
		registerForEvents();
	}
	
	protected abstract void registerForEvents();
	
	@Override
	public void handleEvent(AbstractEvent inEvent) {
		_events.add(inEvent);
	}
	
	@Override
	public void run() {
		while (_keepRunning) {
			AbstractEvent curEvent = null;
			try {
				curEvent = _events.take();
				processEvents(curEvent);
			} catch (InterruptedException e) {
				_keepRunning = false;
			}
		}
	}
	
	protected abstract void processEvents(AbstractEvent event);
}
