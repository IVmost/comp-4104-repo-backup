package edu.carleton.comp4104.assignment2.common;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Vector;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import edu.carleton.comp4104.assignment2.common.Connection.AbstractConnection;
import edu.carleton.comp4104.assignment2.common.Connection.HTTP.HttpConnection;
import edu.carleton.comp4104.assignment2.common.Connection.ObjectOverLine.ObjectOverLineConnection;
import edu.carleton.comp4104.assignment2.common.Reactor;

public class Acceptor implements Runnable {

	protected ServerSocket _listener;
	protected Reactor _reactor;
	protected ConnectionType _conType;
	protected Vector<AbstractConnection> _connectionList = new Vector<AbstractConnection>();
	protected boolean _keepAccepting = true;
	protected ThreadPoolExecutor _threadPool;
	
	public Acceptor(ServerSocket listener, Reactor reactor, ConnectionType conType) {
		_listener = listener;
		_reactor = reactor;
		_conType = conType;
		_threadPool = new ThreadPoolExecutor(0, 200, 1, TimeUnit.MICROSECONDS, new SynchronousQueue<Runnable>());
	}
	
	protected AbstractConnection accept() throws IOException {
		Socket newSocket = _listener.accept();
		newSocket.setKeepAlive(true);
		AbstractConnection newConnection = null;
		switch (_conType) {
		case ObjectOverLine:
			newConnection = new ObjectOverLineConnection(newSocket, _threadPool);
			break;
		case HTTP:
			newConnection = new HttpConnection(newSocket, _threadPool);
			break;
		}
		if (newConnection == null) {
			throw new IOException("Couldn't create connection");
		}
		newConnection.registerWithReactor(_reactor);
		return newConnection;
	}
	
	public void endConnections() {
		for (AbstractConnection abstractConnection : _connectionList) {
			abstractConnection.endWriter();
		}
		for (AbstractConnection abstractConnection : _connectionList) {
			abstractConnection.endReader();
		}
		_threadPool.shutdown();
	}
	
	public void stopAccepting() {
		_keepAccepting = false;
		try {
			_listener.close();
		} catch (IOException e) {
			System.err.println("Acceptor.stopAccepting : IOException while closing ServerSocket");
			e.printStackTrace();
		}
	}
	
	@Override
	public void run() {
		while (_keepAccepting) {
			try {
				AbstractConnection newConnection = accept();
				_connectionList.add(newConnection);
				//System.out.println("Acceptor created a connection and started it");
			} catch (IOException e) {
				//System.out.println("Acceptor stop accepting, probably due to ServerSocket closing");
			}
		}
	}
}
