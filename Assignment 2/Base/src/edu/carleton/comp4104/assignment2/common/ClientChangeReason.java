package edu.carleton.comp4104.assignment2.common;

public enum ClientChangeReason {
	Login, Logout
}
