package edu.carleton.comp4104.assignment2.common.Connection.Messages;

import java.io.Serializable;

public class MessageBody implements Serializable{

	private static final long serialVersionUID = -2490612559406683072L;
	
	public final boolean Success;
	
	public MessageBody(boolean success) {
		Success = success;
	}
}
