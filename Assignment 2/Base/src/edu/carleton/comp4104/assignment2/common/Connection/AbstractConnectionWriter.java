package edu.carleton.comp4104.assignment2.common.Connection;

import java.io.IOException;
import java.net.Socket;
import java.util.Vector;

import edu.carleton.comp4104.assignment2.common.Connection.Messages.Message;

public abstract class AbstractConnectionWriter implements Runnable {

	protected boolean connectionAlive = true;
	protected Socket _peer;
	protected Vector<Message> messagesToSend = new Vector<Message>();
	
	public AbstractConnectionWriter(Socket peer) {
		_peer = peer;
	}
	
	public synchronized void endWriter() {
		connectionAlive = false;
		notify();
	}
	
	public synchronized void sendMessage(Message msg) {
		messagesToSend.add(msg);
		notify();
	}
	
	protected abstract byte[] parseMessage(Message inMsg);
	
	@Override
	public synchronized void run() {

		while (connectionAlive) {
			while (connectionAlive && messagesToSend.isEmpty())
			{
				try {
					wait();
				} catch (InterruptedException e) {
					System.err.println("AbstractConnectionWriter.run : InterruptedException while waiting");
					e.printStackTrace();
					connectionAlive = false;
				}	
			}
			
			if (connectionAlive && !messagesToSend.isEmpty()) {
				Message msg = messagesToSend.remove(0);
				byte[] dataToSend = parseMessage(msg);
				
				if (dataToSend != null) {
					try {
						_peer.getOutputStream().write(dataToSend);
					} catch (IOException e) {
						System.err.println("AbstractConnectionWriter.run : IOException while writing");
						e.printStackTrace();
					}
				}
				else {
					System.err.println("AbstractConnectionWriter.run : parsed dataToSend was null");
				}
			}
		}
	}
}
