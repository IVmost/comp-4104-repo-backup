package edu.carleton.comp4104.assignment2.common.Connection.Messages.MessageBodies;

import edu.carleton.comp4104.assignment2.common.ClientTextResultCode;
import edu.carleton.comp4104.assignment2.common.Connection.Messages.MessageBody;

public class TextMessageBody extends MessageBody {

	private static final long serialVersionUID = -1745989643261795600L;
	
	public final String Recipient;
	public final String TextMessage;
	public final ClientTextResultCode Code;
	
	public TextMessageBody(boolean success, String recipient, String textMessage, ClientTextResultCode code) {
		super(success);
		Recipient = recipient;
		TextMessage = textMessage;
		Code = code;
	}

}
