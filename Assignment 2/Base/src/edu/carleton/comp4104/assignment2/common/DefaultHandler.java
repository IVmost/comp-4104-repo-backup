package edu.carleton.comp4104.assignment2.common;

import edu.carleton.comp4104.assignment2.common.EventHandling.AbstractEvent;
import edu.carleton.comp4104.assignment2.common.EventHandling.EventProcessor;

public class DefaultHandler extends EventProcessor {

	@Override
	protected void registerForEvents() {
		//We don't register, we're set to be the default handler
	}

	@Override
	protected void processEvents(AbstractEvent event) {
		//System.out.println("DefaultHandler: got event of type: " + event.shortName() 
		//		+ " (Long name: " + event.EventType + ")");
	}

}
