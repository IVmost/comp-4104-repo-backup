package edu.carleton.comp4104.assignment2.common.Connection;

import java.io.IOException;
import java.net.Socket;

import edu.carleton.comp4104.assignment2.common.Reactor;
import edu.carleton.comp4104.assignment2.common.Connection.Messages.Message;
import edu.carleton.comp4104.assignment2.common.EventHandling.Events.ClientDisconnectEvent;
import edu.carleton.comp4104.assignment2.common.EventHandling.Events.ReceivedMessageEvent;

public abstract class AbstractConnectionReader implements Runnable {

	protected boolean connectionAlive = true;
	protected int _clientId;
	protected Socket _peer;
	protected Reactor _reactor;
	protected AbstractConnection _connection;
	
	public AbstractConnectionReader(int clientId, Socket peer, Reactor reactor, AbstractConnection connection) {
		_clientId = clientId;
		_peer = peer;
		_reactor = reactor;
		_connection = connection;
	}
	
	protected void endReader() {
		connectionAlive = false;
	}
	
	protected abstract Message parseMessage(byte[] data);
	
	@Override
	public void run() {
		
		byte[] dataReceived = new byte[10000];
		int val = 0;
		
		while (connectionAlive) {
			
			try {
				val = _peer.getInputStream().read(dataReceived);
				if (val == -1) {
					connectionAlive = false;
				}
			} catch (IOException e) {
				//System.err.println("AbstractConnectionReader.run : IOException while reading from socket");
				connectionAlive = false;
			}
			
			if (connectionAlive) {
				Message newMessage = parseMessage(dataReceived);
				//Send out message to reactor
				if (newMessage != null) {
					_reactor.dispatch(new ReceivedMessageEvent(newMessage, _clientId));
				}
				else {
					System.err.println("AbstractConnectionReader.run : parsed message was null");
				}
			}
		}
		_connection.endWriter();
		_reactor.dispatch(new ClientDisconnectEvent(_clientId));
	}
}
