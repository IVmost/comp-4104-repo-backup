package edu.carleton.comp4104.assignment2.common.Connection.HTTP.Input;

import java.io.IOException;
import java.io.InputStream;

import edu.carleton.comp4104.assignment2.common.Connection.Messages.Message;
import edu.carleton.comp4104.assignment2.common.Connection.Messages.MessageBody;
import edu.carleton.comp4104.assignment2.common.Connection.Messages.MessageHeader;

public class HttpInputStream {
	
	protected HttpHeaderInputStream _headerStream;
	protected HttpBodyInputStream _bodyStream;
	
	public HttpInputStream(InputStream stream) {
		_headerStream = new HttpHeaderInputStream(stream);
		_bodyStream = new HttpBodyInputStream(stream);
	}
	
	public Message readMessage() {
		MessageHeader head = null;
		MessageBody body = null;
		try {
			head = _headerStream.readMessageHeader();
			body = _bodyStream.readMessageBody(head);
		} catch (IOException e) {
			System.err.println("HttpInputStream.readMessage : IOException error while reading header and body");
			e.printStackTrace();
		}
		return new Message(head, body);
	}
}
