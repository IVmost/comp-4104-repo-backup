package edu.carleton.comp4104.assignment2.common.EventHandling;

public abstract class AbstractEvent {
	
	public final String EventType;
	
	public AbstractEvent(String eventType) {
		EventType = eventType;
	}
	
	public String toString() {
		return "Event of type: " + EventType;
	}
	
	public String shortName() {
		String[] parts = EventType.split("\\.");
		return parts[parts.length - 1];
	}
}
