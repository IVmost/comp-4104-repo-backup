package edu.carleton.comp4104.assignment2.common.EventHandling.Events;

import edu.carleton.comp4104.assignment2.common.EventHandling.AbstractEvent;

public class ClientDisconnectEvent extends AbstractEvent {

	public final static String EventType = ClientDisconnectEvent.class.getName();
	
	public final int ClientId;
	
	public ClientDisconnectEvent(int id) {
		super(EventType);
		ClientId = id;
	}

}
