package edu.carleton.comp4104.assignment2.common;

public enum ClientTextResultCode { 
	OK,
	ERROR_UNKNOWN_SENDER, 
	ERROR_UNKNOWN_RECEPIENT
}
