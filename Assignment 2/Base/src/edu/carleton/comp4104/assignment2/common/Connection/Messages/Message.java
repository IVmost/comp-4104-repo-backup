package edu.carleton.comp4104.assignment2.common.Connection.Messages;

import java.io.Serializable;

public class Message implements Serializable {
	
	private static final long serialVersionUID = -5306871309686848904L;
	
	public final MessageHeader Header;
	public final MessageBody Body;
	
	public Message(MessageHeader header, MessageBody body) {
		Header = header;
		Body = body;
	}
}
