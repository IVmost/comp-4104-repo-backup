package edu.carleton.comp4104.assignment2.common.EventHandling.Events;

import java.util.Vector;

import edu.carleton.comp4104.assignment2.common.EventHandling.AbstractEvent;

public class LoginReplyEvent extends AbstractEvent {

	public final static String EventType = LoginReplyEvent.class.getName();
	
	public final boolean Success;
	public final int LoggedInClientId;
	public final Vector<String> ClientNames;
	
	public LoginReplyEvent(boolean success, int loggedInClientId, Vector<String> clientNames) {
		super(EventType);
		Success = success;
		LoggedInClientId = loggedInClientId;
		ClientNames = clientNames;
	}
}
