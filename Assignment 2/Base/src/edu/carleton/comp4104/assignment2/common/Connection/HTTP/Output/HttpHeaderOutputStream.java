package edu.carleton.comp4104.assignment2.common.Connection.HTTP.Output;

import java.io.IOException;
import java.io.OutputStream;

import edu.carleton.comp4104.assignment2.common.Connection.Messages.MessageHeader;
import edu.carleton.comp4104.assignment2.common.Connection.Messages.MessageHeader.CommandType;

public class HttpHeaderOutputStream {

	protected OutputStream _stream;
	
	public HttpHeaderOutputStream(OutputStream stream) {
		_stream = stream;
	}

	public void writeMessageHeader(MessageHeader header) throws IOException {
		//Write a request
		if (header.Type == CommandType.Request) {
			writeString("POST " + header.Cmd.toString() + " HTTP 1.1\n");
			writeString("userName:" + header.ClientName + "\n");
		}
		//Write a reply
		else {
			writeString("HTTP 1.1 200 OK\n");
			writeString("userName:" + header.ClientName + "\n");
			writeString("type:" + header.Cmd.toString() + "\n");
		}
		writeString("\n");
	}
	
	public void writeString(String toSend) throws IOException{
		char[] toSendArray = toSend.toCharArray();
		for (int i = 0; i < toSendArray.length; i++) {
			_stream.write(toSendArray[i]);
		}
	}
}
