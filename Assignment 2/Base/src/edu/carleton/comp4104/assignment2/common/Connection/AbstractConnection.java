package edu.carleton.comp4104.assignment2.common.Connection;

import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.ThreadPoolExecutor;

import edu.carleton.comp4104.assignment2.common.Reactor;
import edu.carleton.comp4104.assignment2.common.Connection.Messages.Message;
import edu.carleton.comp4104.assignment2.common.EventHandling.AbstractEvent;
import edu.carleton.comp4104.assignment2.common.EventHandling.EventHandler;
import edu.carleton.comp4104.assignment2.common.EventHandling.Events.SendMessageEvent;

public abstract class AbstractConnection implements EventHandler {
	
	protected static int currentId = 0;
	protected static int getNewId() { return ++currentId; }
	
	protected Socket _peer;
	protected Reactor _reactor;
	
	protected AbstractConnectionReader _reader;
	protected AbstractConnectionWriter _writer;
	
	protected ThreadPoolExecutor _threadPool = null;
	
	protected final int _clientId;
	
	public AbstractConnection(Socket peer, ThreadPoolExecutor threadPool) {
		_peer = peer;
		_clientId = getNewId();
		_threadPool = threadPool;
	}
	
	@Override
	public void registerWithReactor(Reactor reactor) {
		_reactor = reactor;
		_reactor.registerHandler(SendMessageEvent.EventType, this);
		_reader = getConnectionReader(_clientId, _peer, _reactor);
		_writer = getConnectionWriter( _peer);
		_threadPool.execute(_reader);
		_threadPool.execute(_writer);

	}
	
	protected abstract AbstractConnectionReader getConnectionReader(int clientId, Socket peer, Reactor reactor);
	protected abstract AbstractConnectionWriter getConnectionWriter(Socket peer);
	
	public void endReader() {
		_reader.endReader();
		
		try {
			_peer.close();
		} catch (IOException e) {
			System.err.println("Connection.end : IOException when closing socket");
			e.printStackTrace();
		}
	}
	
	public void endWriter() {
		_writer.endWriter();
	}
	
	@Override
	public void handleEvent(AbstractEvent inEvent) {
		if (inEvent instanceof SendMessageEvent) {
			SendMessageEvent msgEvent = (SendMessageEvent)inEvent;
			if (msgEvent.ClientIdsToSendTo == null || msgEvent.ClientIdsToSendTo.contains(_clientId)) {
				//Message for us to send
				_writer.sendMessage(msgEvent.Message);
			}
		}
	}
	
	public void sendMessage(Message msg) {
		_writer.sendMessage(msg);
	}
}
