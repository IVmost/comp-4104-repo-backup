package edu.carleton.comp4104.assignment2.common;

import edu.carleton.comp4104.assignment2.common.Connection.Messages.MessageBody;
import edu.carleton.comp4104.assignment2.common.Connection.Messages.MessageHeader;
import edu.carleton.comp4104.assignment2.common.Connection.Messages.MessageHeader.Command;
import edu.carleton.comp4104.assignment2.common.Connection.Messages.MessageHeader.CommandType;
import edu.carleton.comp4104.assignment2.common.Connection.Messages.MessageBodies.ClientChangesBody;
import edu.carleton.comp4104.assignment2.common.Connection.Messages.MessageBodies.ListOfClientsBody;
import edu.carleton.comp4104.assignment2.common.Connection.Messages.MessageBodies.TextMessageBody;
import edu.carleton.comp4104.assignment2.common.EventHandling.AbstractEvent;
import edu.carleton.comp4104.assignment2.common.EventHandling.EventProcessor;
import edu.carleton.comp4104.assignment2.common.EventHandling.Events.ClientDataChangeEvent;
import edu.carleton.comp4104.assignment2.common.EventHandling.Events.ClientGetTextRequestEvent;
import edu.carleton.comp4104.assignment2.common.EventHandling.Events.ClientSendTextReplyEvent;
import edu.carleton.comp4104.assignment2.common.EventHandling.Events.ClientSendTextRequestEvent;
import edu.carleton.comp4104.assignment2.common.EventHandling.Events.LoginReplyEvent;
import edu.carleton.comp4104.assignment2.common.EventHandling.Events.LoginRequestEvent;
import edu.carleton.comp4104.assignment2.common.EventHandling.Events.LogoutReplyEvent;
import edu.carleton.comp4104.assignment2.common.EventHandling.Events.LogoutRequestEvent;
import edu.carleton.comp4104.assignment2.common.EventHandling.Events.ReceivedMessageEvent;

public class MessageToEventTranslator extends EventProcessor {

	@Override
	protected void registerForEvents() {
		//process messages into events
		_reactor.registerHandler(ReceivedMessageEvent.EventType, this);
	}

	@Override
	protected void processEvents(AbstractEvent event) {
		if (event instanceof ReceivedMessageEvent) {
			ReceivedMessageEvent receivedMessageEvent = (ReceivedMessageEvent)event;
			processReceivedMessageEvent(receivedMessageEvent);
		}
	}
	
	protected void processReceivedMessageEvent(ReceivedMessageEvent event) {
		MessageHeader header = event.Message.Header;
		MessageBody body = event.Message.Body;
		int id = event.ClientId;
		
		if (header.Cmd == Command.Login) {
			processLogin(header, body, id);
		}
		else if (header.Cmd == Command.Logout) {
			processLogout(header, body, id);
		}
		else if (header.Cmd == Command.ClientDataChange) {
			processClientDataChange(header, body, id);
		}
		else if (header.Cmd == Command.ClientGetMessage) {
			processClientGetMessage(header, body, id);
		}
		else if (header.Cmd == Command.ClientSendMessage) {
			processClientSendMessage(header, body, id);
		}
	}
	
	protected void processLogin(MessageHeader header, MessageBody body, int id) {
		if (header.Type == CommandType.Request) {
			_reactor.dispatch(new LoginRequestEvent(header.ClientName, id));
		}
		else if (header.Type == CommandType.Reply) {
			ListOfClientsBody list = (ListOfClientsBody)body;
			_reactor.dispatch(new LoginReplyEvent(list.Success, id, list.Clients));
		}
	}
	
	protected void processLogout(MessageHeader header, MessageBody body, int id) {
		if (header.Type == CommandType.Request) {
			_reactor.dispatch(new LogoutRequestEvent(header.ClientName, id));
		}
		else if (header.Type == CommandType.Reply) {
			_reactor.dispatch(new LogoutReplyEvent(body.Success, id));
		}
	}
	
	protected void processClientDataChange(MessageHeader header, MessageBody body, int id) {
		ClientChangesBody changeBody = (ClientChangesBody)body;
		if (header.Type == CommandType.Request) {
			_reactor.dispatch(new ClientDataChangeEvent(changeBody.Change, changeBody.ClientName, id));
		}
		else if (header.Type == CommandType.Reply) {
			//We don't handle replies to this message
		}
	}
	
	protected void processClientGetMessage(MessageHeader header, MessageBody body, int id) {
		TextMessageBody textBody = (TextMessageBody)body;
		if (header.Type == CommandType.Request) {
			_reactor.dispatch(new ClientGetTextRequestEvent(header.ClientName, textBody.Recipient, 
					id, textBody.TextMessage));
		}
		else if (header.Type == CommandType.Reply) {
			//We don't handle replies to this message
		}
	}
	
	protected void processClientSendMessage(MessageHeader header, MessageBody body, int id) {
		TextMessageBody textBody = (TextMessageBody)body;
		if (header.Type == CommandType.Request) {
			_reactor.dispatch(new ClientSendTextRequestEvent(header.ClientName, id, textBody.Recipient, 
					textBody.TextMessage));
		}
		else if (header.Type == CommandType.Reply) {
			_reactor.dispatch(new ClientSendTextReplyEvent(header.ClientName, id, textBody.Recipient, 
					textBody.TextMessage, textBody.Code));
		}
	}

}
