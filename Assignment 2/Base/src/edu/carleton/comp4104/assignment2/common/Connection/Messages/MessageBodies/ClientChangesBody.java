package edu.carleton.comp4104.assignment2.common.Connection.Messages.MessageBodies;

import edu.carleton.comp4104.assignment2.common.ClientChangeReason;
import edu.carleton.comp4104.assignment2.common.Connection.Messages.MessageBody;

public class ClientChangesBody extends MessageBody {

	private static final long serialVersionUID = 7139105366146174636L;

	public final ClientChangeReason Change;
	public final String ClientName;
	
	public ClientChangesBody(boolean success, ClientChangeReason change, String clientName) {
		super(success);
		Change = change;
		ClientName = clientName;
	}

}
