package edu.carleton.comp4104.assignment2.common.Connection.HTTP.Output;

import java.io.IOException;
import java.io.OutputStream;

import com.google.gson.Gson;

import edu.carleton.comp4104.assignment2.common.Connection.Messages.Message;
import edu.carleton.comp4104.assignment2.common.Connection.Messages.MessageHeader;
import edu.carleton.comp4104.assignment2.common.Connection.Messages.MessageHeader.Command;
import edu.carleton.comp4104.assignment2.common.Connection.Messages.MessageHeader.CommandType;

public class HttpBodyOutputStream {

	protected OutputStream _stream;
	protected Gson gson = new Gson();
	
	public HttpBodyOutputStream(OutputStream stream) {
		_stream = stream;
	}

	public void writeMessageBody(Message msg) throws IOException {
		MessageHeader header = msg.Header;
		//Messages without body
		if (
				(header.Cmd == Command.Login && header.Type == CommandType.Request) ||
				(header.Cmd == Command.Logout && header.Type == CommandType.Request)
			) {
			return;
		}
		
		//Messages with body
		String jsonBody = gson.toJson(msg.Body);
		writeString(jsonBody + "\n");
		/*if (header.Cmd == Command.Login && header.Type == CommandType.Reply) {
			toReturn = gson.fromJson(jsonBody, ListOfClientsBody.class);
			
		}
		else if (header.Cmd == Command.Logout && header.Type == CommandType.Reply) {
			toReturn = gson.fromJson(jsonBody, MessageBody.class);
		}
		else if (header.Cmd == Command.ClientDataChange) {
			toReturn = gson.fromJson(jsonBody, ClientChangesBody.class);
		}
		else if (header.Cmd == Command.ClientGetMessage && header.Type == CommandType.Reply) {
			toReturn = gson.fromJson(jsonBody, MessageBody.class);
		}
		else if (header.Cmd == Command.ClientGetMessage || header.Cmd == Command.ClientSendMessage) {
			toReturn = gson.fromJson(jsonBody, TextMessageBody.class);
		}*/
	}
	
	public void writeString(String toSend) throws IOException{
		char[] toSendArray = toSend.toCharArray();
		for (int i = 0; i < toSendArray.length; i++) {
			_stream.write(toSendArray[i]);
		}
	}
}
