package edu.carleton.comp4104.assignment2.common.EventHandling;

import edu.carleton.comp4104.assignment2.common.Reactor;

public interface EventHandler {
	public void registerWithReactor(Reactor reactor);
	public void handleEvent(AbstractEvent inEvent);
}
