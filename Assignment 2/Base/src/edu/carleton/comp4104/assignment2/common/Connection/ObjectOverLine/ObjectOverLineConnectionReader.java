package edu.carleton.comp4104.assignment2.common.Connection.ObjectOverLine;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;

import edu.carleton.comp4104.assignment2.common.Reactor;
import edu.carleton.comp4104.assignment2.common.Connection.AbstractConnectionReader;
import edu.carleton.comp4104.assignment2.common.Connection.Messages.Message;

public class ObjectOverLineConnectionReader extends AbstractConnectionReader {

	protected ByteArrayInputStream _byteInputStream;
	protected ObjectInputStream _objInputStream;
	
	public ObjectOverLineConnectionReader(int clientId, Socket peer, Reactor reactor, 
			ObjectOverLineConnection connection) {
		super(clientId, peer, reactor, connection);	
	}

	@Override
	protected Message parseMessage(byte[] data) {
		_byteInputStream = new ByteArrayInputStream(data);
		
		try {
			_objInputStream = new ObjectInputStream(_byteInputStream);
		} catch (IOException e) {
			System.err.println("ObjectOverLineConnectionReader.parseMessage : IOException when creating object stream");
			e.printStackTrace();
			return null;
		}
		
		Object obj = null;
		
		try {
			obj = _objInputStream.readObject();
		} catch (IOException e) {
			System.err.println("ObjectOverLineConnectionReader.parseMessage : IOException when reading object stream");
			e.printStackTrace();
			return null;
		} catch (ClassNotFoundException e) {
			System.err.println("ObjectOverLineConnectionReader.parseMessage : ClassNotFoundException when reading object stream");
			e.printStackTrace();
			return null;
		}
		
		Message msg = null;
		
		if (obj != null && obj instanceof Message) {
			msg = (Message)obj;
		}
		
		return msg;
	}

}
