package edu.carleton.comp4104.assignment2.common;

import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.ThreadPoolExecutor;

import edu.carleton.comp4104.assignment2.common.Connection.AbstractConnection;
import edu.carleton.comp4104.assignment2.common.Connection.HTTP.HttpConnection;
import edu.carleton.comp4104.assignment2.common.Connection.ObjectOverLine.ObjectOverLineConnection;
import edu.carleton.comp4104.assignment2.common.Reactor;

public class Connector {
	
	public static AbstractConnection connect(String host, int port, Reactor reactor, ConnectionType conType,
			ThreadPoolExecutor threadPool) throws IOException {
		Socket peer = new Socket(host, port);
		peer.setKeepAlive(true);
		AbstractConnection newConnection = null;
		switch (conType) {
		case ObjectOverLine:
			newConnection = new ObjectOverLineConnection(peer, threadPool);
			break;
		case HTTP:
			newConnection = new HttpConnection(peer, threadPool);
			break;
		}
		if (newConnection == null) {
			throw new IOException("Couldn't create connection");
		}
		newConnection.registerWithReactor(reactor);
		return newConnection;
	}
}
