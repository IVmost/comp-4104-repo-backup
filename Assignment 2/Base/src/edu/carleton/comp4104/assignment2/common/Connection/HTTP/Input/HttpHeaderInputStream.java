package edu.carleton.comp4104.assignment2.common.Connection.HTTP.Input;

import java.io.IOException;
import java.io.InputStream;

import edu.carleton.comp4104.assignment2.common.Connection.Messages.MessageHeader;
import edu.carleton.comp4104.assignment2.common.Connection.Messages.MessageHeader.Command;
import edu.carleton.comp4104.assignment2.common.Connection.Messages.MessageHeader.CommandType;

public class HttpHeaderInputStream {

	protected InputStream _stream;
	
	public HttpHeaderInputStream(InputStream stream) {
		_stream = stream;
	}

	public MessageHeader readMessageHeader() throws IOException {
		Command command = null;
		CommandType type = null;
		String clientName = null;
		
		String firstLine = readString('\n');
		String[] parts = firstLine.split(" ");
		
		//Determine if request or reply
			//request
		if (parts[0].equals("POST")) {
			command = Command.valueOf(parts[1]);
			type = CommandType.Request;
			String userNameLine = readString('\n');
			String[] userNameLineParts = userNameLine.split(":");
			clientName = userNameLineParts[1];
		}
			//reply
		else {
			type = CommandType.Reply;
			String userNameLine = readString('\n');
			String[] userNameLineParts = userNameLine.split(":");
			clientName = userNameLineParts[1];
			String commandLine = readString('\n');
			String[] commandLineParts = commandLine.split(":");
			command = Command.valueOf(commandLineParts[1]);
		}
		
		readString('\n');
		
		return new MessageHeader(command, type, clientName);
	}
	
	public String readString(char untilChar) throws IOException{
		String toReturn = "";
		while (true) {
			int val = _stream.read();
			if (val != -1) {
				if ((char)val == untilChar) return toReturn;
				else toReturn += (char)val;
			}
			else throw new IOException("End of Stream");
		}
	}
}
