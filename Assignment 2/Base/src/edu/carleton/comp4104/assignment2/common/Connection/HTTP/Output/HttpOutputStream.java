package edu.carleton.comp4104.assignment2.common.Connection.HTTP.Output;

import java.io.IOException;
import java.io.OutputStream;

import edu.carleton.comp4104.assignment2.common.Connection.Messages.Message;

public class HttpOutputStream {
	
	protected HttpHeaderOutputStream _headerStream;
	protected HttpBodyOutputStream _bodyStream;
	
	public HttpOutputStream(OutputStream stream) {
		_headerStream = new HttpHeaderOutputStream(stream);
		_bodyStream = new HttpBodyOutputStream(stream);
	}
	
	public void writeMessage(Message msg) {
		try {
			_headerStream.writeMessageHeader(msg.Header);
			_bodyStream.writeMessageBody(msg);
		} catch (IOException e) {
			
		}
	}
}
