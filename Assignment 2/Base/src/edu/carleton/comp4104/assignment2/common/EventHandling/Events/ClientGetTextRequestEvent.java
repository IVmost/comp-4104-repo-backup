package edu.carleton.comp4104.assignment2.common.EventHandling.Events;

import edu.carleton.comp4104.assignment2.common.EventHandling.AbstractEvent;

public class ClientGetTextRequestEvent extends AbstractEvent{
	
	public final static String EventType = ClientGetTextRequestEvent.class.getName();
	
	public final String ClientFromName;
	public final String ClientToName;
	public final int ClientToId;
	public final String TextMessage;
	
	public ClientGetTextRequestEvent(String clientFromName,	String clientToName, 
			int clientToId, String textMessage) {
		super(EventType);
		ClientFromName = clientFromName;
		ClientToName = clientToName;
		ClientToId = clientToId;
		TextMessage = textMessage;
	}
}
