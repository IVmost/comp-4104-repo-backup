package edu.carleton.comp4104.assignment2.common.Connection.ObjectOverLine;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;

import edu.carleton.comp4104.assignment2.common.Connection.AbstractConnectionWriter;
import edu.carleton.comp4104.assignment2.common.Connection.Messages.Message;

public class ObjectOverLineConnectionWriter extends AbstractConnectionWriter {

	protected ByteArrayOutputStream _byteOutputStream;
	protected ObjectOutputStream _objOutputStream;
	
	public ObjectOverLineConnectionWriter(Socket peer) {
		super(peer);
	}

	@Override
	protected byte[] parseMessage(Message inMsg) {
		_byteOutputStream = new ByteArrayOutputStream();
		
		try {
			_objOutputStream = new ObjectOutputStream(_byteOutputStream);
		} catch (IOException e) {
			System.err.println("ObjectOverLineConnectionWriter.parseMessage : IOException when creating object stream");
			e.printStackTrace();
			return null;
		}
		
		try {
			_objOutputStream.writeObject(inMsg);
		} catch (IOException e) {
			System.err.println("ObjectOverLineConnectionWriter.parseMessage : IOException when writing object stream");
			e.printStackTrace();
			return null;
		}
		
		return _byteOutputStream.toByteArray();
	}

}
