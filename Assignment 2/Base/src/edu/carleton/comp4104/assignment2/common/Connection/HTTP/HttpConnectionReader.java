package edu.carleton.comp4104.assignment2.common.Connection.HTTP;

import java.io.ByteArrayInputStream;
import java.net.Socket;

import edu.carleton.comp4104.assignment2.common.Reactor;
import edu.carleton.comp4104.assignment2.common.Connection.AbstractConnection;
import edu.carleton.comp4104.assignment2.common.Connection.AbstractConnectionReader;
import edu.carleton.comp4104.assignment2.common.Connection.HTTP.Input.HttpInputStream;
import edu.carleton.comp4104.assignment2.common.Connection.Messages.Message;

public class HttpConnectionReader extends AbstractConnectionReader {

	protected ByteArrayInputStream _byteInputStream;
	protected HttpInputStream _httpInputStream;
	
	public HttpConnectionReader(int clientId, Socket peer, Reactor reactor,
			AbstractConnection connection) {
		super(clientId, peer, reactor, connection);
	}

	@Override
	protected Message parseMessage(byte[] data) {
		_byteInputStream = new ByteArrayInputStream(data);
		_httpInputStream = new HttpInputStream(_byteInputStream);
		return _httpInputStream.readMessage();
	}
	
	

}