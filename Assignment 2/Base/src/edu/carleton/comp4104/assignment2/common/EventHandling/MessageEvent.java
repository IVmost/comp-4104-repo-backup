package edu.carleton.comp4104.assignment2.common.EventHandling;

import edu.carleton.comp4104.assignment2.common.Connection.Messages.Message;

public abstract class MessageEvent extends AbstractEvent {

	public Message Message;
	
	public MessageEvent(String eventType, Message message) {
		super(eventType);
		Message = message;
	}

}