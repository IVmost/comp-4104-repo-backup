package edu.carleton.comp4104.assignment2.app.filters;

import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.widget.Button;

public class IpFilter implements InputFilter {

	private Button ipSaveButton;
	public IpFilter(Button ipSaveButton) {
		this.ipSaveButton=ipSaveButton;
	}

	@Override
	public CharSequence filter(CharSequence source, int start, int end,
			Spanned dest, int dstart, int dend) {
		if (end > start) {
			char[] newinput = new char[end - start];
			TextUtils.getChars(source, start, end, newinput, 0);
			String[] splitIp = dest.toString().split("[\\.]");
			String fixed = "";
			if (splitIp.length <= 4) {
				if (splitIp[splitIp.length - 1].length() == 3
						&& newinput[newinput.length - 1] != '.'
						&& dest.toString().lastIndexOf('.') != dest
								.toString().length() - 1)
					fixed = ".";
				for (int x = 0; x < newinput.length; x++) {
					if (Character.isDigit(newinput[x])
							|| (newinput[x] == '.' && dest.toString()
									.lastIndexOf('.') != dest
									.toString().length() - 1)
							&& splitIp.length != 4)
						fixed += newinput[x];
				}
				if (splitIp.length == 4
						&& splitIp[splitIp.length - 1].length() == 3)
					fixed = "";
			}
			if (fixed.indexOf('.') == -1) {
				if (Integer
						.parseInt((splitIp[splitIp.length - 1] + fixed)) > 255
						&& dest.toString().lastIndexOf('.') != dest
								.toString().length() - 1) {
					if (splitIp.length == 4)
						fixed = "";
					else
						fixed = "." + fixed;
				}
			}

			if (splitIp.length == 4
					&& (splitIp[splitIp.length - 1].length() + fixed
							.length()) > 0)
				ipSaveButton.setEnabled(true);
			else{
				String[] tempSplit= (dest.toString()+fixed).split("[\\.]");
				if (tempSplit.length==4 && tempSplit[tempSplit.length-1].length()>0)
					ipSaveButton.setEnabled(true);
				else
					ipSaveButton.setEnabled(false);
			}
			return fixed;
		} else if (dest.toString().lastIndexOf('.') == dest.toString()
				.length() - 2)
			ipSaveButton.setEnabled(false);
		return null;
	}


}
