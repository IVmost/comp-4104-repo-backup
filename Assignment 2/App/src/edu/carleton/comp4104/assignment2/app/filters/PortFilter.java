package edu.carleton.comp4104.assignment2.app.filters;

import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.widget.Button;

public class PortFilter implements InputFilter{
	private Button portSaveButton;
	
	public PortFilter(Button portSaveButton) {
		this.portSaveButton=portSaveButton;
	}

	@Override
	public CharSequence filter(CharSequence source, int start, int end,
			Spanned dest, int dstart, int dend) {
		if (end>start){
			char[] newinput = new char[end - start];
			TextUtils.getChars(source, start, end, newinput, 0);
			String fixed="";
			for (int x = 0; x < newinput.length; x++) {
				if (Character.isDigit(newinput[x]))
					fixed+=newinput[x];
			}
			
			String newString=dest.toString()+fixed;
			if (newString.length()>0)
				portSaveButton.setEnabled(true);
			if (Integer.parseInt(newString)>65535)
				fixed="";
			return fixed;
			
		}else{
			if (dest.toString().length()==1)
				portSaveButton.setEnabled(false);
		}
		return null;
	}

}
