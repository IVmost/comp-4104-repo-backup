package edu.carleton.comp4104.assignment2.app;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import edu.carleton.comp4104.assignment2.client.ConnectReplyEvent;
import edu.carleton.comp4104.assignment2.client.ConnectRequestEvent;
import edu.carleton.comp4104.assignment2.client.DisconnectRequestEvent;
import edu.carleton.comp4104.assignment2.common.ConnectionType;
import edu.carleton.comp4104.assignment2.common.Connector;
import edu.carleton.comp4104.assignment2.common.Connection.AbstractConnection;
import edu.carleton.comp4104.assignment2.common.EventHandling.AbstractEvent;
import edu.carleton.comp4104.assignment2.common.EventHandling.EventProcessor;
import edu.carleton.comp4104.assignment2.common.EventHandling.Events.LoginRequestEvent;
import edu.carleton.comp4104.assignment2.common.EventHandling.Events.LogoutReplyEvent;

public class AppBackEnd extends EventProcessor {
	private Hashtable<String, ArrayList<Map<String, String>>> messageDatabase;
	private ArrayList<String> users;
	private ArrayList<Map<String, String>> currentMessages;

	protected ThreadPoolExecutor _threadPool;
	protected AbstractConnection _connection = null;

	protected final ConnectionType _conType;

	public static String msgData = "msgData";
	public static String userData = "userData";
	private Settings settings;

	public AppBackEnd(Settings settings, ConnectionType conType) {
		this.settings = settings;
		this.messageDatabase = new Hashtable<String, ArrayList<Map<String, String>>>();
		this.users = new ArrayList<String>();
		this.currentMessages = new ArrayList<Map<String, String>>();
		_threadPool = new ThreadPoolExecutor(0, 6, 1, TimeUnit.MILLISECONDS,
				new SynchronousQueue<Runnable>());
		_conType = conType;
	}

	public ArrayList<String> getUsers() {
		return users;
	}

	public synchronized void addUser(String user) {
		users.add(user);
		messageDatabase.put(user, new ArrayList<Map<String, String>>());
	}

	public synchronized void removeUser(String user) {

		users.remove(user);
		messageDatabase.remove(user);
	}

	public ArrayList<Map<String, String>> getMessages() {
		return currentMessages;
	}

	public void setCurrentMessages(String user) {
		if (messageDatabase.containsKey(user)) {
			currentMessages = (messageDatabase.get(user));
		}
	}

	public void addOutgoingMessage(String userSelected, String message) {
		Map<String, String> newMsg = new HashMap<String, String>();
		newMsg.put(userData, settings.username + "->" + userSelected);
		newMsg.put(msgData, message);
		messageDatabase.get(userSelected).add(newMsg);

	}

	protected void connect() {
		// If we haven't connected already
		if (_connection == null) {
			try {

				_connection = Connector.connect(settings.ipAddress,
						Integer.parseInt(settings.port), _reactor, _conType,
						_threadPool);

			} catch (IOException e) {
				_reactor.dispatch(new ConnectReplyEvent(false));
				System.err.println("Client " + settings.username
						+ " couldn't connect");
				e.printStackTrace();
				return;
			}
			settings.connected = true;
			_reactor.dispatch(new ConnectReplyEvent(true));
		}

		_reactor.dispatch(new LoginRequestEvent(settings.username, -1));
	}

	public void disconnect() {
		/*
		 * settings.connected = false; _reactor.dispatch(new
		 * ConnectReplyEvent(false)); _connection.endWriter();
		 * _connection.endReader();
		 */
		//_reactor.dispatch(new LogoutRequestEvent(settings.username, -1));
		_connection.endReader();
		_connection.endWriter();
		_connection = null;
		_reactor.dispatch(new LogoutReplyEvent(true, -1));
	}

	public void start() {
		new Thread(this).start();
	}

	public void sendMessage(String userSelected, String message) {
		// send message by client
		addOutgoingMessage(userSelected, message);
	}

	@Override
	protected void registerForEvents() {
		_reactor.registerHandler(ConnectRequestEvent.EventType, this);
		_reactor.registerHandler(DisconnectRequestEvent.EventType, this);
	}

	@Override
	protected void processEvents(AbstractEvent event) {
		if (event instanceof ConnectRequestEvent) {
			connect();
		} else if (event instanceof DisconnectRequestEvent) {
			disconnect();
		}
	}

	public void receiveMessage(String clientFromName, String textMessage) {
		if (messageDatabase.containsKey(clientFromName)) {
			Map<String, String> newMsg = new HashMap<String, String>();
			newMsg.put(userData, clientFromName + "->" + settings.username);
			newMsg.put(msgData, textMessage);
			messageDatabase.get(clientFromName).add(newMsg);
		}

	}

	public String clearSelected(String clientName) {
		removeUser(clientName);
		if (users.isEmpty()) {
			currentMessages.clear();
			return null;
		} else {
			String newSelected = users.get(0);
			return newSelected;
		}
	}

	public void clearData() {
		users.clear();
		messageDatabase.clear();
		currentMessages.clear();
		
	}

}
