package edu.carleton.comp4104.assignment2.app;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import edu.carleton.comp4104.assignment2.client.ConnectRequestEvent;
import edu.carleton.comp4104.assignment2.client.DisconnectRequestEvent;
import edu.carleton.comp4104.assignment2.common.ConnectionType;
import edu.carleton.comp4104.assignment2.common.Reactor;
import edu.carleton.comp4104.assignment2.common.EventHandling.EventHandler;
import edu.carleton.comp4104.assignment2.common.EventHandling.EventProcessor;
import edu.carleton.comp4104.assignment2.common.EventHandling.Events.ClientSendTextRequestEvent;
import android.app.Activity;
import android.app.Application;

public class App extends Application {

	protected Reactor _reactor;
	protected Thread _reactorThread;

	protected EventProcessor _defaultHandler;
	protected Thread _defaultHandlerThread;

	protected EventProcessor _EtoMTranslator;
	protected Thread _EtoMTranslatorThread;

	protected EventProcessor _MtoETranslator;
	protected Thread _MtoETranslatorThread;

	private AppBackEnd appBackEnd;
	protected Thread _appBackEndThread;

	public Settings settings;
	private Activity mainActivity;
	private static String defaultUsername = "Derp";
	private static String defaultIpAddress = "10.10.220.76";
	private static String defaultPort = "25565";

	@Override
	public void onCreate() {
		super.onCreate();

		_reactor = new Reactor();
		_reactorThread = new Thread(_reactor, "Reactor");

		settings = new Settings(defaultUsername, defaultIpAddress, false,
				false, defaultPort);

		Properties properties = new Properties();

		try {
			properties.load(this.getAssets().open("client-im.cfg"));
		} catch (FileNotFoundException e) {
			System.err
					.println("Server.start : FileNotFoundException while loading client-im.cfg");
			e.printStackTrace();
		} catch (IOException e) {
			System.err
					.println("Server.start : IOException while loading client-im.cfg");
			e.printStackTrace();
		}
		
		ConnectionType conType = ConnectionType.ObjectOverLine;
		
		conType = ConnectionType.valueOf((String)properties.get("Connection Type"));
		
		appBackEnd = new AppBackEnd(settings, conType);
		appBackEnd.registerWithReactor(_reactor);
		_appBackEndThread = new Thread(appBackEnd);
		_appBackEndThread.start();

		try {
			_defaultHandler = loadEventProcessor(properties, "Default Handler");
			_EtoMTranslator = loadEventProcessor(properties, "EtoM");
			_MtoETranslator = loadEventProcessor(properties, "MtoE");
		} catch (ClassNotFoundException e) {
			System.err
					.println("Server.start : ClassNotFoundException while loading event processors");
			e.printStackTrace();
			return;
		} catch (InstantiationException e) {
			System.err
					.println("Server.start : InstantiationException while loading event processors");
			e.printStackTrace();
			return;
		} catch (IllegalAccessException e) {
			System.err
					.println("Server.start : IllegalAccessException while loading event processors");
			e.printStackTrace();
			return;
		} catch (NullPointerException e) {
			System.err
					.println("Server.start : NullPointerException while loading event processors - no handler for "
							+ e.getMessage());
			e.printStackTrace();
			return;
		}

		_EtoMTranslator.registerWithReactor(_reactor);
		_EtoMTranslatorThread = new Thread(_EtoMTranslator, "Client EtoM");
		_MtoETranslator.registerWithReactor(_reactor);
		_MtoETranslatorThread = new Thread(_MtoETranslator, "Client MtoE");

		_reactorThread.start();
		_EtoMTranslatorThread.start();
		_MtoETranslatorThread.start();
	}

	@SuppressWarnings("rawtypes")
	public EventProcessor loadEventProcessor(Properties properties, String key)
			throws ClassNotFoundException, InstantiationException,
			IllegalAccessException, NullPointerException {
		String className = properties.getProperty(key);
		if (className == null)
			throw new NullPointerException("Key " + key + " was not found");
		Class classObj = Class.forName(className);
		return (EventProcessor) classObj.newInstance();
	}

	public void registerEventHandler(EventHandler handler) {
		handler.registerWithReactor(_reactor);
	}

	public void setBackEnd(AppBackEnd appBackEnd) {
		this.appBackEnd = appBackEnd;
		this.appBackEnd.registerWithReactor(_reactor);
	}

	public void connect() {
		_reactor.dispatch(new ConnectRequestEvent());
	}

	public void disconnect() {
		_reactor.dispatch(new DisconnectRequestEvent());
	}

	public void sendMessage(String clientName, String message) {
		_reactor.dispatch(new ClientSendTextRequestEvent(settings.username, -1,
				clientName, message));
	}

	public Settings getSettings() {
		return settings;
	}

	public Reactor getReactor() {
		return _reactor;
	}

	public AppBackEnd getAppBackEnd() {
		return appBackEnd;
	}
	
	public void setMainActivity(Activity mainActivity){
		this.mainActivity=mainActivity;
	}
	
	public Activity getMainActivity(){
		return mainActivity;
	}
}
