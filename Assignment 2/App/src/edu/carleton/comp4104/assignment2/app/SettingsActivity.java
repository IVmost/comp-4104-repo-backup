package edu.carleton.comp4104.assignment2.app;

import edu.carleton.comp4104.assignment2.app.filters.IpFilter;
import edu.carleton.comp4104.assignment2.app.filters.PortFilter;
import edu.carleton.comp4104.assignment2.client.ConnectReplyEvent;
import edu.carleton.comp4104.assignment2.common.Reactor;
import edu.carleton.comp4104.assignment2.common.EventHandling.AbstractEvent;
import edu.carleton.comp4104.assignment2.common.EventHandling.EventHandler;
import edu.carleton.comp4104.assignment2.common.EventHandling.Events.LoginReplyEvent;
import edu.carleton.comp4104.assignment2.common.EventHandling.Events.LogoutReplyEvent;
import android.os.Bundle;
import android.app.Activity;
import android.text.InputFilter;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class SettingsActivity extends Activity implements EventHandler {
	private EditText ipInput;
	private TextView currentIp;
	private Button ipSaveButton;

	private EditText userInput;
	private TextView currentUser;
	private Button userSaveButton;
	
	private EditText portInput;
	private TextView currentPort;
	private Button portSaveButton;
	
	private Settings setting;
	private ToggleButton connectionButton;
	
	private App app;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);
		
		app=(App)getApplicationContext();
		setting=app.getSettings();
		
		app.registerEventHandler(this);
		
		ipInput = (EditText) findViewById(R.id.ipEntry);
		
		currentIp = (TextView) findViewById(R.id.currentIpAddress);
		currentIp.setText(setting.ipAddress);
		
		ipSaveButton = (Button) findViewById(R.id.ipSaveButton);
		ipSaveButton.setEnabled(false);

		
		userInput = (EditText) findViewById(R.id.user_entry);
		userSaveButton = (Button) findViewById(R.id.userNameButton);
		userSaveButton.setEnabled(!setting.connected);
		
		currentUser = (TextView) findViewById(R.id.cUserNameDisplay);
		currentUser.setText(setting.username);
		
		portInput=(EditText) findViewById(R.id.portEntry);
		
		currentPort=(TextView) findViewById(R.id.currentPortMessage);
		currentPort.setText(setting.port);
		
		portSaveButton=(Button) findViewById(R.id.portSaveButton);
		portSaveButton.setEnabled(false);
					
		InputFilter portFilter =new PortFilter(portSaveButton);
		portInput.setFilters(new InputFilter[]{portFilter});
		
		connectionButton=(ToggleButton) findViewById(R.id.connectButton);
		connectionButton.setChecked(setting.connected);

		InputFilter ipfilter = new IpFilter(ipSaveButton);
		ipInput.setFilters(new InputFilter[] { ipfilter });

		enableDataChange(!setting.connected);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_settings, menu);
		return true;
	}

	public void saveUserName(View v) {
		String username = userInput.getText().toString();
		if (username.length() > 0) {
			setting.username=username;
			currentUser.setText(setting.username);
			userInput.setText("");
		} 
	}

	public void saveIpAddress(View v) {
		setting.ipAddress = ipInput.getText().toString();
		currentIp.setText(setting.ipAddress);
	}
	
	public void savePort(View v){
		setting.port= portInput.getText().toString();
		currentPort.setText(setting.port);
	}
	
	public void connect(View v){
		enableDataChange(false);
		//Connect
		if (connectionButton.isChecked()) {
			connectionButton.setEnabled(false);
			app.connect();
		}
		//Disconnect
		else {
			connectionButton.setEnabled(false);
			app.disconnect();
		}
	}
	
	public void closeSettings(MenuItem filler){
		finish();
	}
	public void onStop(View v){
		finish();
	}

	@Override
	public void registerWithReactor(Reactor reactor) {
		reactor.registerHandler(ConnectReplyEvent.EventType, this);
		reactor.registerHandler(LoginReplyEvent.EventType, this);
		reactor.registerHandler(LogoutReplyEvent.EventType, this);
	}

	@Override
	public void handleEvent(final AbstractEvent inEvent) {
		this.runOnUiThread(new Runnable() {
			public void run() {
				if (inEvent instanceof ConnectReplyEvent) {
					ConnectReplyEvent connectReplyEvent = (ConnectReplyEvent)inEvent;
					processConnectReplyEvent(connectReplyEvent);
				}
				else if (inEvent instanceof LoginReplyEvent) {
					LoginReplyEvent loginReplyEvent = (LoginReplyEvent)inEvent;
					processLoginReplyEvent(loginReplyEvent);
				}
				else if (inEvent instanceof LogoutReplyEvent) {
					LogoutReplyEvent logoutReplyEvent = (LogoutReplyEvent)inEvent;
					processLogoutReplyEvent(logoutReplyEvent);
				}
			}
		});
	}
	
	public void processConnectReplyEvent(ConnectReplyEvent event) {
		if (!event.Success) {
			Toast.makeText(getApplicationContext(), "Could not connect. Check server properties.", 
					Toast.LENGTH_LONG).show();
			connectionButton.setChecked(false);
			connectionButton.setEnabled(true);
			enableDataChange(true);
		}
	}
	
	public void processLoginReplyEvent(LoginReplyEvent event) {
		if (!event.Success) {
			Toast.makeText(getApplicationContext(), "Could not login. Trying changing your name.", 
					Toast.LENGTH_LONG).show();
			connectionButton.setChecked(false);
			enableDataChange(true);
		}
		connectionButton.setEnabled(true);
		if (event.Success) {
			this.finish();
		}
	}
	
	public void processLogoutReplyEvent(LogoutReplyEvent event) {
		if (!event.Success) {
			Toast.makeText(getApplicationContext(), "Could not logout.", 
					Toast.LENGTH_LONG).show();
			connectionButton.setChecked(false);
		}else {
			app.getAppBackEnd().clearData();
			((MainActivity)app.getMainActivity()).updateLists();
		}
		connectionButton.setEnabled(true);
		enableDataChange(true);
	}
	
	public void enableDataChange(boolean enable) {
		userSaveButton.setEnabled(enable);
		userInput.setEnabled(enable);
		ipInput.setEnabled(enable);
		portInput.setEnabled(enable);
	}
}
