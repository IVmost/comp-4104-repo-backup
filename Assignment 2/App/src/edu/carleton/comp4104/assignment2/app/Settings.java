package edu.carleton.comp4104.assignment2.app;

import java.io.Serializable;

public class Settings implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String username;
	public String ipAddress;
	public boolean connected;
	public boolean loggedIn;
	public String port;
	
	public Settings(String username, String ipAddress, boolean connected, boolean loggedIn, String port) {
		this.username=username;
		this.ipAddress=ipAddress;
		this.connected=connected;
		this.loggedIn=loggedIn;
		this.port=port;
	}

}
