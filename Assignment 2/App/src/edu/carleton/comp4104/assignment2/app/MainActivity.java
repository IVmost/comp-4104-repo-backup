package edu.carleton.comp4104.assignment2.app;

import java.util.ArrayList;
import java.util.Map;
import java.util.Vector;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;
import edu.carleton.comp4104.assignment2.common.ClientChangeReason;
import edu.carleton.comp4104.assignment2.common.Reactor;
import edu.carleton.comp4104.assignment2.common.EventHandling.AbstractEvent;
import edu.carleton.comp4104.assignment2.common.EventHandling.EventHandler;
import edu.carleton.comp4104.assignment2.common.EventHandling.Events.ClientDataChangeEvent;
import edu.carleton.comp4104.assignment2.common.EventHandling.Events.ClientGetTextRequestEvent;
import edu.carleton.comp4104.assignment2.common.EventHandling.Events.ClientSendTextReplyEvent;
import edu.carleton.comp4104.assignment2.common.EventHandling.Events.LoginReplyEvent;

public class MainActivity extends Activity implements EventHandler {

	private ListView userView;
	private ListView messageView;

	private LinearLayout mainLayout;

	private ArrayAdapter<String> userListAdapter;
	private SimpleAdapter msgListAdapter;

	private String userSelected;
	private Intent settingsIntent;

	private String setting;

	private Settings settings;

	private App app;

	private AppBackEnd appBackEnd;

	// Assign adapter to ListView
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		app = (App) getApplicationContext();
		app.setMainActivity(this);
		settings = app.getSettings();
		app.registerEventHandler(this);

		appBackEnd = app.getAppBackEnd();

		mainLayout = (LinearLayout) findViewById(R.id.main_Layout);
		userView = (ListView) findViewById(R.id.userList);
		AppBackEnd appBackEnd = app.getAppBackEnd();
		ArrayList<String> users = appBackEnd.getUsers();

		ArrayList<Map<String, String>> messages = appBackEnd.getMessages();

		userListAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, android.R.id.text1, users);

		userView.setAdapter(userListAdapter);
		userView.setCacheColorHint(Color.TRANSPARENT);
		userView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

		messageView = (ListView) findViewById(R.id.chatList);

		msgListAdapter = new SimpleAdapter(this, messages,
				android.R.layout.simple_list_item_2, new String[] {
						AppBackEnd.userData, AppBackEnd.msgData }, new int[] {
						android.R.id.text2, android.R.id.text1 });
		messageView.setAdapter(msgListAdapter);
		userSelected = null;
		LinearLayout messageLayout = (LinearLayout) findViewById(R.id.message_layout);
		userView.setHeaderDividersEnabled(true);
		messageView.setHeaderDividersEnabled(true);
		userView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				String newSelected = (String) (userView.getItemAtPosition(arg2));
				if (!newSelected.equals(userSelected))
					updateMessageList(newSelected);
				// userListAdapter.notifyDataSetChanged();
				// userView.setSelection(arg2);
				userView.postInvalidate();
			}
		});

		settingsIntent = new Intent(this, SettingsActivity.class);

		startActivity(settingsIntent);
	}

	protected void updateMessageList(String newSelected) {
		userSelected = newSelected;
		newMsgList();

	}

	private void newMsgList() {
		appBackEnd.setCurrentMessages(userSelected);
		msgListAdapter = new SimpleAdapter(this, appBackEnd.getMessages(),
				android.R.layout.simple_list_item_2, new String[] {
						AppBackEnd.userData, AppBackEnd.msgData }, new int[] {
						android.R.id.text2, android.R.id.text1 });
		;
		messageView.setAdapter(msgListAdapter);
		messageView.setSelection(appBackEnd.getMessages().size()-1);
		updateLists();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	public void sendMessage(View view) {
		if (userSelected != null) {
			EditText editText = (EditText) findViewById(R.id.edit_message);
			String message = editText.getText().toString();
			app.sendMessage(userSelected, message);
			editText.getText().clear();
			AppBackEnd appBackEnd = app.getAppBackEnd();
			appBackEnd.sendMessage(userSelected, message);

			msgListAdapter.notifyDataSetChanged();
			messageView.setSelection(appBackEnd.getMessages().size());

		}
	}

	public void openSettings(MenuItem item) {

		startActivity(settingsIntent);
	}

	@Override
	public void registerWithReactor(Reactor reactor) {
		reactor.registerHandler(LoginReplyEvent.EventType, this);
		reactor.registerHandler(ClientDataChangeEvent.EventType, this);
		reactor.registerHandler(ClientGetTextRequestEvent.EventType, this);
		reactor.registerHandler(ClientSendTextReplyEvent.EventType, this); // Optional
																			// later
	}

	@Override
	public void handleEvent(final AbstractEvent inEvent) {
		this.runOnUiThread(new Runnable() {
			public void run() {
				if (inEvent instanceof ClientDataChangeEvent) {
					ClientDataChangeEvent clientDataChangeEvent = (ClientDataChangeEvent) inEvent;
					processClientDataChangeEvent(clientDataChangeEvent);
				} else if (inEvent instanceof ClientGetTextRequestEvent) {
					ClientGetTextRequestEvent clientGetTextRequestEvent = (ClientGetTextRequestEvent) inEvent;
					processClientGetTextRequestEvent(clientGetTextRequestEvent);
				} else if (inEvent instanceof ClientSendTextReplyEvent) {
					ClientSendTextReplyEvent clientSendTextReplyEvent = (ClientSendTextReplyEvent) inEvent;
					processClientSendTextReplyEvent(clientSendTextReplyEvent);
				} else if (inEvent instanceof LoginReplyEvent) {
					LoginReplyEvent loginReplyEvent = (LoginReplyEvent) inEvent;
					processLoginReplyEvent(loginReplyEvent);
				} else {

				}
			}
		});
	}

	protected void processLoginReplyEvent(LoginReplyEvent loginReplyEvent) {
		userSelected=null;
		Vector<String> users = loginReplyEvent.ClientNames;
		appBackEnd.clearData();
		for (String user : users) {
			appBackEnd.addUser(user);
		}
		updateLists();

	}

	// Someone other than us has logged in or out
	public void processClientDataChangeEvent(ClientDataChangeEvent event) {
		if (event.Change == ClientChangeReason.Login) {
			appBackEnd.addUser(event.ClientName);
			notifyUser();
		} else {
			if (event.ClientName.equals(userSelected)) {
				userSelected = appBackEnd.clearSelected(event.ClientName);
				if (userSelected != null)
					newMsgList();

			} else {
				appBackEnd.removeUser(event.ClientName);
			}
		}

		updateLists();

	}

	private void notifyUser() {
		try {
			Uri notification = RingtoneManager
					.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			Ringtone r = RingtoneManager.getRingtone(getApplicationContext(),
					notification);
			r.play();
			new Thread(new Runnable() {

				@Override
				public void run() {
					Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

					v.vibrate(300);

				}
			}).start();

		} catch (Exception e) {
		}

	}

	public void updateLists() {
		this.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				msgListAdapter.notifyDataSetChanged();
				userListAdapter.notifyDataSetChanged();
			}
		});

	}

	// Someone sent us a message
	public void processClientGetTextRequestEvent(ClientGetTextRequestEvent event) {
		appBackEnd.receiveMessage(event.ClientFromName, event.TextMessage);
		if (event.ClientFromName.equals(userSelected)) {
			messageView.setSelection(appBackEnd.getMessages().size()-1);
		} else {
			Toast.makeText(getApplicationContext(),
					event.ClientFromName + " sent you a message.",
					Toast.LENGTH_LONG).show();
		}

		notifyUser();
		updateLists();
	}

	// Message saying our sent message went through (optional, if we have time)
	public void processClientSendTextReplyEvent(ClientSendTextReplyEvent event) {

	}

}
