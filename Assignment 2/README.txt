README

Some of the client requirements haven't been met as asked, since we choose the Android client route.

Note:
The App is stored in a seperate project than the server. So both projects must be loaded in eclipse to work.

The client doesn't choose the cfg file through command line, it always uses the client-im.cfg.
	You can however, still configure the client-im.cfg file to use different classes
	You also choose the connection type in the client-im.cfg, "ObjectOverLine" or "HTTP" (without quotes)
The client doesn't choose their name through the command line, they must choose one when connecting
The client boots up into the settings menu so you can configure connection information and name.
	After connecting, you are taken to the chat area.
	
The Server has an extra command line option, -t <connectionType>, to choose ObjectOverLine or HTTP
	"-t ObjectOverLine" or "-t HTTP" for example
	Without the command, the server does connections with ObjectOverLine
	
-----TESTING-----

Clients can login and logout
	If the client disconnects by shutting down the app, the server handles it and logs them out
Clients update their lists when other users login or logout
Client can send and get messages
Tested with 3 Android clients running on one server
