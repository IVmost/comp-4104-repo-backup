package edu.carleton.comp4104.assignment3.common.EventHandling;

import edu.carleton.comp4104.assignment3.common.Reactor;

public interface EventHandler {
	public void registerWithReactor(Reactor reactor);
	public void handleEvent(AbstractEvent inEvent);
}
