package edu.carleton.comp4104.assignment3.common.Connection;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Vector;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.naming.NamingException;

import edu.carleton.comp4104.assignment3.common.ConnectorFactory;
import edu.carleton.comp4104.assignment3.common.Reactor;
import edu.carleton.comp4104.assignment3.common.Connection.Messages.Message;
import edu.carleton.comp4104.assignment3.common.EventHandling.AbstractEvent;
import edu.carleton.comp4104.assignment3.common.EventHandling.EventHandler;
import edu.carleton.comp4104.assignment3.common.EventHandling.Events.ReceivedMessageEvent;
import edu.carleton.comp4104.assignment3.common.EventHandling.Events.SendMessageEvent;

public class JmsConnection implements EventHandler, MessageListener {
	
	protected static int currentId = 0;
	protected static int getNewId() { return ++currentId; }
	
	protected static final String JMS_MESSAGE_OBJECT_ID = "MESSAGE";
	
	protected Reactor _reactor;
	
	protected Connection _connection = null;
	protected Session _session = null;
	protected MessageConsumer _in;
	//protected MessageProducer _out;
	protected final String _queueNameIn;
	protected final String _queueNameOut;
	
	protected Queue _queueIn;
	
	protected HashMap<Integer, String> _queueNamesById = new HashMap<Integer, String>();
	protected HashMap<String, Integer> _idsByQueueName = new HashMap<String, Integer>();
	
	public JmsConnection(String queueNameIn) {
		_queueNameIn = queueNameIn;
		_queueNameOut = null;
	}
	
	public JmsConnection(String queueNameIn, String queueNameOut) {
		_queueNameIn = queueNameIn;
		_queueNameOut = queueNameOut;		
	}
	
	public void start(Reactor reactor) {
		registerWithReactor(reactor);
		try {
			_connection = ConnectorFactory.getConnectionFactory().createConnection();
			_connection.start();
			_session = _connection.createSession(false, Session.AUTO_ACKNOWLEDGE); // false=NotTransacted
			_queueIn = _session.createQueue(_queueNameIn);
			_in = _session.createConsumer(_queueIn);
			_in.setMessageListener(this);
			/*if (_queueNameOut != null) {
				_out = _session.createProducer(_session.createQueue(_queueNameOut));
				_out.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
			}*/
		} catch (JMSException e) {
			stop();
			e.printStackTrace();
		} catch (NamingException e) {
			stop();
			e.printStackTrace();
		}
	}
	
	public void stop() {
		try {
			if (_connection != null) _connection.close();
		} catch (JMSException e) {
			System.err.println("JmsConnection.stop : Error closing connection.");
			e.printStackTrace();
		}
	}
	
	@Override
	public void registerWithReactor(Reactor reactor) {
		_reactor = reactor;
		_reactor.registerHandler(SendMessageEvent.EventType, this);
	}
	
	@Override
	public void handleEvent(AbstractEvent inEvent) {
		if (inEvent instanceof SendMessageEvent) {
			SendMessageEvent msgEvent = (SendMessageEvent)inEvent;
			sendMessage(msgEvent.Message, msgEvent.ClientIdsToSendTo);
		}
	}
	
	public void sendMessage(Message msg, Vector<Integer> queues) {
		//If we have a queueNameOut, we always send on that queue
		try {
			if (_queueNameOut != null) {
				MessageProducer out = _session.createProducer(_session.createQueue(_queueNameOut));
				out.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
				sendMessageOnMessageProducer(msg, out);
			}
			//Else, we look for a matching queue
			else {	
				for (int queueId : queues) {
					if (_queueNamesById.containsKey(queueId)) {
						MessageProducer out = _session.createProducer(_session.createQueue(_queueNamesById.get(queueId)));
						out.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
						sendMessageOnMessageProducer(msg, out);
					}
					else {
						System.err.println("JmsConnection.sendMessage : no queue with that id");
					}
				}
			}
		} catch (JMSException e) {
			System.err.println("JmsConnection.sendMessage : JMSException");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("JmsConnection.sendMessage : IOException");
			e.printStackTrace();
		}
	}
	
	protected void sendMessageOnMessageProducer(Message msg, MessageProducer out) throws JMSException, IOException {
		MapMessage outMsg = _session.createMapMessage();
		byte[] serializedMessage = parseMessage(msg);
		outMsg.setObject(JMS_MESSAGE_OBJECT_ID, serializedMessage);
		outMsg.setJMSReplyTo(_queueIn);
		out.send(outMsg);
	}
	
	protected byte[] parseMessage(Message inMsg) throws IOException {
		ByteArrayOutputStream _byteOutputStream = new ByteArrayOutputStream();
		ObjectOutputStream _objOutputStream;
		_objOutputStream = new ObjectOutputStream(_byteOutputStream);
		_objOutputStream.writeObject(inMsg);
		return _byteOutputStream.toByteArray();
	}

	@Override
	public synchronized void onMessage(javax.jms.Message jmsMsg) {
		try {
			Destination fromDest = jmsMsg.getJMSReplyTo();
			if (fromDest instanceof Queue) {
				Queue fromQueue = (Queue)fromDest;
				String queueName = fromQueue.getQueueName();
				int clientId;
				//Check to see if we already know of this queue
				if (_idsByQueueName.containsKey(queueName)) {
					clientId = _idsByQueueName.get(queueName);
				}
				//Else, we assign a new id, and record it
				else {
					clientId = getNewId();
					_idsByQueueName.put(queueName, clientId);
					_queueNamesById.put(clientId, queueName);
				}
				
				//Convert to map message
				if (jmsMsg instanceof MapMessage) {
					MapMessage jmsMap = (MapMessage)jmsMsg;
					//Send event through the system
					byte[] data = (byte[]) jmsMap.getObject(JMS_MESSAGE_OBJECT_ID);
					Message msg = parseMessage(data);
					ReceivedMessageEvent newEvent = new ReceivedMessageEvent(msg, clientId);
					_reactor.dispatch(newEvent);
				}
				else throw new Exception("Not a map message");
			}
			else throw new Exception("Destination is not a queue");
		} catch (Exception e) {
			System.err.println("JmsConnection.onMessage : Exception");
			e.printStackTrace();
		}
	}
	
	protected Message parseMessage(byte[] data) throws IOException, ClassNotFoundException{
		ByteArrayInputStream _byteInputStream = new ByteArrayInputStream(data);
		ObjectInputStream _objInputStream = new ObjectInputStream(_byteInputStream);
		Object obj = _objInputStream.readObject();
		Message msg = null;
		if (obj != null && obj instanceof Message) {
			msg = (Message)obj;
		}
		return msg;
	}
}
