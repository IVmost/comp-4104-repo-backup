package edu.carleton.comp4104.assignment3.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import edu.carleton.comp4104.assignment3.common.EventHandling.AbstractEvent;
import edu.carleton.comp4104.assignment3.common.EventHandling.EventHandler;

public class Reactor implements Runnable {

	protected boolean _keepRunning = true;
	protected ArrayList<AbstractEvent> _eventQueue = new ArrayList<AbstractEvent>();
	protected HashMap<String, ArrayList<EventHandler>> _eventHandlers = new HashMap<String, ArrayList<EventHandler>>();
	protected EventHandler _defaultHandler;
	
	public void registerHandler(String eventType, EventHandler inHandle) {
		if (_eventHandlers.containsKey(eventType)) {
			ArrayList<EventHandler> currentHandlerVector = _eventHandlers.get(eventType);
			currentHandlerVector.add(inHandle);
		}
		else {
			ArrayList<EventHandler> newHandlerVector = new ArrayList<EventHandler>();
			newHandlerVector.add(inHandle);
			_eventHandlers.put(eventType, newHandlerVector);
		}
	}
	
	public void registerDefaultHandler(EventHandler inHandle) {
		_defaultHandler = inHandle;
	}
	
	public void removeAllHandlers() {
		_eventHandlers.clear();
	}
	
	public boolean removeHandler(String eventType) {
		if (_eventHandlers.containsKey(eventType)) {
			_eventHandlers.remove(eventType);
			return true;
		}
		else {
			return false;
		}
	}
	
	public boolean removeHandler(String eventType, EventHandler inHandle) {
		if (_eventHandlers.containsKey(eventType)) {
			ArrayList<EventHandler> currentHandlerVector = _eventHandlers.get(inHandle);
			boolean toReturn = currentHandlerVector.remove(inHandle);
			if (currentHandlerVector.isEmpty()) {
				_eventHandlers.remove(eventType);
			}
			return toReturn;
		}
		else {
			return false;
		}
	}
	
	public synchronized void dispatch(AbstractEvent inEvent) {
		_eventQueue.add(inEvent);
		notify();
	}
	
	public synchronized void stop() {
		_keepRunning = false;
		notify();
	}

	public void useDefaultHandler(AbstractEvent event) {
		if (_defaultHandler == null) {
			System.err.println("Error: Reactor has no default handler or handlers for event type: " + event.EventType);
		}
		else {
			_defaultHandler.handleEvent(event);
		}
	}
	
	@Override
	public synchronized void run() {
		
		while (_keepRunning) {
			//Wait for Events
			if (_eventQueue.isEmpty() && _keepRunning) {
				try {
					wait();
					continue;
				} catch (InterruptedException e) {
					System.err.println("Reactor.run : InterruptedException while waiting");
					e.printStackTrace();
					return;
				}
			}
			
			//Process Events
			if (_keepRunning && !_eventQueue.isEmpty()) {
				AbstractEvent curEvent = _eventQueue.remove(0);
				
				if (!(_eventHandlers.containsKey(curEvent.EventType))) {
					useDefaultHandler(curEvent);
					continue;
				}
				
				ArrayList<EventHandler> handlersToUse = _eventHandlers.get(curEvent.EventType);
				
				if (handlersToUse.isEmpty()) {
					useDefaultHandler(curEvent);
					continue;
				}
				
				//Loop through and call handlers
				Iterator<EventHandler> eventHandlerIter = handlersToUse.iterator();
				while (eventHandlerIter.hasNext()) {
					eventHandlerIter.next().handleEvent(curEvent);
				}
			}
		}
		
	}
	
}
