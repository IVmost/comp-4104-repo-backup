package edu.carleton.comp4104.assignment3.common.EventHandling.Events;

import java.util.Vector;

import edu.carleton.comp4104.assignment3.common.Connection.Messages.Message;
import edu.carleton.comp4104.assignment3.common.EventHandling.AbstractEvent;

public class SendMessageEvent extends AbstractEvent {

	public final static String EventType = SendMessageEvent.class.getName();
	
	public final Message Message;
	public final Vector<Integer> ClientIdsToSendTo;
	
	public SendMessageEvent(Message message, Vector<Integer> clientIdsToSendTo) {
		super(EventType);
		Message = message;
		ClientIdsToSendTo = clientIdsToSendTo;
	}
}
