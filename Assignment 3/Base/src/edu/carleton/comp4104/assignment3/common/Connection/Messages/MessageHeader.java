package edu.carleton.comp4104.assignment3.common.Connection.Messages;

import java.io.Serializable;

public class MessageHeader implements Serializable {

	private static final long serialVersionUID = 2031757655772927532L;

	public enum Command { Login, Logout, ClientDataChange, ClientSendMessage, ClientGetMessage }
	public enum CommandType { Request, Reply }
	
	public final Command Cmd;
	public final CommandType Type;
	public final String ClientName;
	
	public MessageHeader(Command cmd, CommandType type, String client) {
		Cmd = cmd;
		Type = type;
		ClientName = client;
	}
	
}
