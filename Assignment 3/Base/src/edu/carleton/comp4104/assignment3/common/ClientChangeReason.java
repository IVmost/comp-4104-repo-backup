package edu.carleton.comp4104.assignment3.common;

public enum ClientChangeReason {
	Login, Logout
}
