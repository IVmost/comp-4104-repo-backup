package edu.carleton.comp4104.assignment3.common;

public enum ConnectionType {
	ObjectOverLine, HTTP, JMS
}
