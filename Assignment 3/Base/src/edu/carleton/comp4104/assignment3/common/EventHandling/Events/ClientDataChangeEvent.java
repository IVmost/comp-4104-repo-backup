package edu.carleton.comp4104.assignment3.common.EventHandling.Events;

import edu.carleton.comp4104.assignment3.common.ClientChangeReason;
import edu.carleton.comp4104.assignment3.common.EventHandling.AbstractEvent;

public class ClientDataChangeEvent extends AbstractEvent {

	public final static String EventType = ClientDataChangeEvent.class.getName();
	
	public final ClientChangeReason Change;
	public final String ClientName;
	public final int ClientId;
	
	public ClientDataChangeEvent(ClientChangeReason change, String clientName, int clientId) {
		super(EventType);
		Change = change;
		ClientName = clientName;
		ClientId = clientId;
	}

}
