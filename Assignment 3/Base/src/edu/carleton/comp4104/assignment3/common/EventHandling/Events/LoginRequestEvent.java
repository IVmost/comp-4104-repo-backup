package edu.carleton.comp4104.assignment3.common.EventHandling.Events;

import edu.carleton.comp4104.assignment3.common.EventHandling.AbstractEvent;

public class LoginRequestEvent extends AbstractEvent {

	public final static String EventType = LoginRequestEvent.class.getName();
	
	public final String ClientName;
	public final int ClientId;
	
	public LoginRequestEvent(String clientName, int clientId) {
		super(EventType);
		ClientName = clientName;
		ClientId = clientId;
	}
}
