package edu.carleton.comp4104.assignment3.common.EventHandling.Events;

import edu.carleton.comp4104.assignment3.common.Connection.Messages.Message;
import edu.carleton.comp4104.assignment3.common.EventHandling.MessageEvent;

public class ReceivedMessageEvent extends MessageEvent {
	
	public final static String EventType = ReceivedMessageEvent.class.getName();
	
	public final Message Message;
	public final int ClientId;
	
	public ReceivedMessageEvent(Message message, int clientId) {
		super(EventType, message);
		Message = message;
		ClientId = clientId;
	}
}
