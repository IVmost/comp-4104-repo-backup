package edu.carleton.comp4104.assignment3.common.EventHandling.Events;

import edu.carleton.comp4104.assignment3.common.EventHandling.AbstractEvent;

public class LogoutReplyEvent extends AbstractEvent {

	public final static String EventType = LogoutReplyEvent.class.getName();

	public final boolean Success;
	public final int ClientId;
	
	public LogoutReplyEvent(boolean success, int clientId) {
		super(EventType);
		Success = success;
		ClientId = clientId;
	}
}
