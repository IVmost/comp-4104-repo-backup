package edu.carleton.comp4104.assignment3.common.EventHandling.Events;

import edu.carleton.comp4104.assignment3.common.EventHandling.AbstractEvent;

public class ClientSendTextRequestEvent extends AbstractEvent {

	public final static String EventType = ClientSendTextRequestEvent.class.getName();
	
	public final String ClientFromName;
	public final int ClientFromId;
	public final String ClientToName;
	public final String TextMessage;
	
	public ClientSendTextRequestEvent(String clientFromName, int clientFromId, 
			String clientToName, String textMessage) {
		super(EventType);
		ClientFromName = clientFromName;
		ClientFromId = clientFromId;
		ClientToName = clientToName;
		TextMessage = textMessage;
	}
}
