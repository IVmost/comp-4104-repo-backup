package edu.carleton.comp4104.assignment3.common.Connection.Messages.MessageBodies;

import java.util.Vector;

import edu.carleton.comp4104.assignment3.common.Connection.Messages.MessageBody;

public class ListOfClientsBody extends MessageBody {

	private static final long serialVersionUID = -455936600348873354L;
	
	public final Vector<String> Clients;
	
	public ListOfClientsBody(boolean success, Vector<String> clients) {
		super(success);
		Clients = clients;
	}

}
