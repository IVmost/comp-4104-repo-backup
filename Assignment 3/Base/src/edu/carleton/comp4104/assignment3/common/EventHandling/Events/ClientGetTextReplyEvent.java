package edu.carleton.comp4104.assignment3.common.EventHandling.Events;

import edu.carleton.comp4104.assignment3.common.ClientTextResultCode;
import edu.carleton.comp4104.assignment3.common.EventHandling.AbstractEvent;

public class ClientGetTextReplyEvent extends AbstractEvent{
	
	public final static String EventType = ClientGetTextReplyEvent.class.getName();
	
	public final String ClientFromName;
	public final String ClientToName;
	public final int ClientToId;
	public final ClientTextResultCode Code;
	
	public ClientGetTextReplyEvent(String clientFromName, String clientToName, 
			int clientToId, ClientTextResultCode code) {
		super(EventType);
		ClientFromName = clientFromName;
		ClientToName = clientToName;
		ClientToId = clientToId;
		Code = code;
	}
}
