package edu.carleton.comp4104.assignment3.common.EventHandling;

import edu.carleton.comp4104.assignment3.common.Connection.Messages.Message;

public abstract class MessageEvent extends AbstractEvent {

	public Message Message;
	
	public MessageEvent(String eventType, Message message) {
		super(eventType);
		Message = message;
	}

}