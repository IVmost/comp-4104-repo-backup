package edu.carleton.comp4104.assignment3.server;

import java.util.HashMap;
import java.util.Vector;

import edu.carleton.comp4104.assignment3.common.ClientChangeReason;
import edu.carleton.comp4104.assignment3.common.ClientTextResultCode;
import edu.carleton.comp4104.assignment3.common.Connection.Messages.Message;
import edu.carleton.comp4104.assignment3.common.Connection.Messages.MessageBody;
import edu.carleton.comp4104.assignment3.common.Connection.Messages.MessageHeader;
import edu.carleton.comp4104.assignment3.common.Connection.Messages.MessageBodies.ClientChangesBody;
import edu.carleton.comp4104.assignment3.common.Connection.Messages.MessageBodies.ListOfClientsBody;
import edu.carleton.comp4104.assignment3.common.Connection.Messages.MessageBodies.TextMessageBody;
import edu.carleton.comp4104.assignment3.common.Connection.Messages.MessageHeader.Command;
import edu.carleton.comp4104.assignment3.common.Connection.Messages.MessageHeader.CommandType;
import edu.carleton.comp4104.assignment3.common.EventHandling.AbstractEvent;
import edu.carleton.comp4104.assignment3.common.EventHandling.EventProcessor;
import edu.carleton.comp4104.assignment3.common.EventHandling.Events.ClientDataChangeEvent;
import edu.carleton.comp4104.assignment3.common.EventHandling.Events.ClientGetTextRequestEvent;
import edu.carleton.comp4104.assignment3.common.EventHandling.Events.ClientSendTextReplyEvent;
import edu.carleton.comp4104.assignment3.common.EventHandling.Events.LoginReplyEvent;
import edu.carleton.comp4104.assignment3.common.EventHandling.Events.LogoutReplyEvent;
import edu.carleton.comp4104.assignment3.common.EventHandling.Events.SendMessageEvent;

public class ServerEventToMessageTranslator extends EventProcessor {

	protected final String serverName = "Server";
	protected HashMap<String, Integer> _loggedInClients = new HashMap<String, Integer>();
	
	@Override
	protected void registerForEvents() {
		//process out going events into messages
		_reactor.registerHandler(LoginReplyEvent.EventType, this);
		_reactor.registerHandler(LogoutReplyEvent.EventType, this);
		_reactor.registerHandler(ClientDataChangeEvent.EventType, this);
		_reactor.registerHandler(ClientGetTextRequestEvent.EventType, this);
		_reactor.registerHandler(ClientSendTextReplyEvent.EventType, this);
	}

	@Override
	protected void processEvents(AbstractEvent event) {
		if (event instanceof LoginReplyEvent) {
			LoginReplyEvent loginReplyEvent = (LoginReplyEvent)event;
			processLoginReplyEvent(loginReplyEvent);
		}
		else if (event instanceof LogoutReplyEvent) {
			LogoutReplyEvent logoutReplyEvent = (LogoutReplyEvent)event;
			processLogoutReplyEvent(logoutReplyEvent);
		}
		else if (event instanceof ClientDataChangeEvent) {
			ClientDataChangeEvent clientDataChangeEvent = (ClientDataChangeEvent)event;
			processClientDataChangeEvent(clientDataChangeEvent);
		}
		else if (event instanceof ClientGetTextRequestEvent) {
			ClientGetTextRequestEvent clientGetTextRequestEvent = (ClientGetTextRequestEvent)event;
			processClientGetTextRequestEvent(clientGetTextRequestEvent);
		}
		else if (event instanceof ClientSendTextReplyEvent) {
			ClientSendTextReplyEvent clientSendTextReplyEvent = (ClientSendTextReplyEvent)event;
			processClientSendTextReplyEvent(clientSendTextReplyEvent);
		}
		else {
			System.err.println("MessageToEventTranslator.processEvents : don't know how to process - " + event.toString());
		}
	}
	
	protected void processLoginReplyEvent(LoginReplyEvent event) {
		Vector<Integer> loggedInClientId = new Vector<Integer>();
		loggedInClientId.add(event.LoggedInClientId);
		
		//Send a message to client
		_reactor.dispatch(
				new SendMessageEvent(
						new Message(
								new MessageHeader(Command.Login, CommandType.Reply, serverName),
								new ListOfClientsBody(event.Success, event.ClientNames)
								),
						loggedInClientId
						)
				);
	}

	protected void processLogoutReplyEvent(LogoutReplyEvent event) {		
		Vector<Integer> loggedInClientId = new Vector<Integer>();
		loggedInClientId.add(event.ClientId);
		
		//Send a message back to the client
		_reactor.dispatch(
				new SendMessageEvent(
						new Message(
								new MessageHeader(Command.Logout, CommandType.Reply, serverName), 
								new MessageBody(event.Success)
								),
						loggedInClientId
						)
				);
	}

	protected void processClientDataChangeEvent(ClientDataChangeEvent event) {
		//Send out a message to clients with the change and change our own set
		if (event.Change == ClientChangeReason.Login) {
			dispatchClientDataChangeEvent(event);
			_loggedInClients.put(event.ClientName, event.ClientId);
		}
		else if (event.Change == ClientChangeReason.Logout) {
			_loggedInClients.remove(event.ClientName);
			dispatchClientDataChangeEvent(event);
		}
		/*synchronized (System.out)	{ 
			System.out.print("ServerEtoMTranslator: New Client list [ ");
			for (String client : _loggedInClients.keySet()) {
				System.out.print(client + " ");
			}
			System.out.println("]");
		}*/
	}
	
	protected void dispatchClientDataChangeEvent(ClientDataChangeEvent event) {
		Vector<Integer> clientIds = new Vector<Integer>(_loggedInClients.values());
		_reactor.dispatch(
				new SendMessageEvent(
						new Message(
								new MessageHeader(Command.ClientDataChange, CommandType.Request, serverName),
								new ClientChangesBody(true, event.Change, event.ClientName)
								),
						clientIds
						)
				);
	}

	protected void processClientGetTextRequestEvent(ClientGetTextRequestEvent event) {
		
		Vector<Integer> clientTo = new Vector<Integer>();
		clientTo.add(event.ClientToId);
		
		_reactor.dispatch(
				new SendMessageEvent(
						new Message(
								new MessageHeader(Command.ClientGetMessage, 
										CommandType.Request, 
										event.ClientFromName), 
								new TextMessageBody(true, event.ClientToName, event.TextMessage, ClientTextResultCode.OK)
								),
						clientTo
						)
				);
	}
	
	protected void processClientSendTextReplyEvent(ClientSendTextReplyEvent event) {
		
		Vector<Integer> clientFrom = new Vector<Integer>();
		clientFrom.add(event.ClientFromId);
		
		boolean success = (event.Code == ClientTextResultCode.OK);
		
		_reactor.dispatch(
				new SendMessageEvent(
						new Message(
								new MessageHeader(Command.ClientSendMessage, 
										CommandType.Reply, 
										serverName), 
								new TextMessageBody(success, event.ClientToName, event.TextMessage, event.Code)
								),
						clientFrom
						)
				);
	}
}
