package edu.carleton.comp4104.assignment3.server;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import edu.carleton.comp4104.assignment3.common.ClientChangeReason;
import edu.carleton.comp4104.assignment3.common.EventHandling.AbstractEvent;
import edu.carleton.comp4104.assignment3.common.EventHandling.EventHandler;
import edu.carleton.comp4104.assignment3.common.EventHandling.EventProcessor;
import edu.carleton.comp4104.assignment3.common.EventHandling.Events.ClientDataChangeEvent;
import edu.carleton.comp4104.assignment3.common.EventHandling.Events.ClientDisconnectEvent;
import edu.carleton.comp4104.assignment3.common.EventHandling.Events.LoginReplyEvent;
import edu.carleton.comp4104.assignment3.common.EventHandling.Events.LoginRequestEvent;
import edu.carleton.comp4104.assignment3.common.EventHandling.Events.LogoutReplyEvent;
import edu.carleton.comp4104.assignment3.common.EventHandling.Events.LogoutRequestEvent;

public class LoginManager extends EventProcessor implements EventHandler, Runnable {

	protected HashMap<String, Integer> _loggedInClients = new HashMap<String, Integer>();
	
	public LoginManager() {	}
	
	@Override
	protected void registerForEvents() {
		_reactor.registerHandler(LoginRequestEvent.EventType, this);
		_reactor.registerHandler(LogoutRequestEvent.EventType, this);
		_reactor.registerHandler(ClientDisconnectEvent.EventType, this);
	}
	
	protected void processEvents(AbstractEvent event) {
		if (event instanceof LoginRequestEvent) {
			LoginRequestEvent loginRequestEvent = (LoginRequestEvent)event;
			processLogin(loginRequestEvent);
		}
		else if (event instanceof LogoutRequestEvent) {
			LogoutRequestEvent logoutRequestEvent = (LogoutRequestEvent)event;
			processLogout(logoutRequestEvent);
		}
		else if (event instanceof ClientDisconnectEvent) {
			ClientDisconnectEvent clientDisconnectEvent = (ClientDisconnectEvent)event;
			processDisconnect(clientDisconnectEvent);
		}
	}
	
	protected void processLogin(LoginRequestEvent event) {

		Vector<Integer> ourClientId = new Vector<Integer>();
		ourClientId.add(event.ClientId);
		
		Vector<String> clientNames = new Vector<String>(_loggedInClients.keySet());
		
		if (_loggedInClients.containsKey(event.ClientName)) {
			//Already loggedIn, send out LoginReply with failure
			//System.out.println("LoginManager.processLogin : User " + event.ClientId + " tried to login with name " +
			//		event.ClientName + " which is already in use. Denied");
			_reactor.dispatch(new LoginReplyEvent(false, event.ClientId, null));
		}
		else {
			//Can log client in
				//send out loginReplyEvent
			//System.out.println("LoginManager.processLogin : User " + event.ClientId + " logged in with name " +
			//		event.ClientName);
			ServerLogging.log(event.ClientName + " is online");
			_reactor.dispatch(new LoginReplyEvent(true, event.ClientId, clientNames));
				//update our client list
			_loggedInClients.put(event.ClientName, event.ClientId);
				//tell other server components that the client list has changed
			_reactor.dispatch(
					new ClientDataChangeEvent(
							ClientChangeReason.Login,
							event.ClientName,
							event.ClientId
							)
					);
		}
	}
	
	protected void processLogout(LogoutRequestEvent event) {
		
		Vector<Integer> ourClientId = new Vector<Integer>();
		ourClientId.add(event.ClientId);
		
		if (_loggedInClients.containsKey(event.ClientName) &&
				_loggedInClients.get(event.ClientName) == event.ClientId) {
			//Loggged in, and the client requesting matches our records. We can log them out
				//change our client data
			//System.out.println("LoginManager.processLogout : User " + event.ClientId + " with name " +
			//		event.ClientName + " logged out.");
			ServerLogging.log(event.ClientName + " is offline");
			_loggedInClients.remove(event.ClientName);
				//send message to client saying we logged them out
			_reactor.dispatch(new LogoutReplyEvent(true, event.ClientId));
				//tell others that the client list has changed
			_reactor.dispatch(
					new ClientDataChangeEvent(
							ClientChangeReason.Logout,
							event.ClientName,
							event.ClientId
							)
					);
		}
		else {
			//We can't log this client out, send a message to them
			//System.out.println("LoginManager.processLogout : User " + event.ClientId + " attempted to log out " +
			//		"with invalid credentials. Denied");
			_reactor.dispatch(new LogoutReplyEvent(false, event.ClientId));
		}
	}
	
	protected void processDisconnect(ClientDisconnectEvent event) {
		Iterator<String> iter = _loggedInClients.keySet().iterator();
		while (iter.hasNext()) {
			String name = iter.next();
			if (_loggedInClients.get(name) == event.ClientId) {
				iter.remove();
				_loggedInClients.remove(name);
				ServerLogging.log(name + " is offline");
				_reactor.dispatch(
						new ClientDataChangeEvent(
								ClientChangeReason.Logout,
								name,
								event.ClientId
								)
						);
				return;
			}
		}
	}
}
