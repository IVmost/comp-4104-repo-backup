package edu.carleton.comp4104.assignment3.client;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Vector;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.filechooser.FileFilter;

import edu.carleton.comp4104.assignment3.client.filters.ChooserFilter;
import edu.carleton.comp4104.assignment3.common.ClientChangeReason;
import edu.carleton.comp4104.assignment3.common.Reactor;
import edu.carleton.comp4104.assignment3.common.EventHandling.AbstractEvent;
import edu.carleton.comp4104.assignment3.common.EventHandling.EventHandler;
import edu.carleton.comp4104.assignment3.common.EventHandling.Events.ClientDataChangeEvent;
import edu.carleton.comp4104.assignment3.common.EventHandling.Events.ClientGetTextRequestEvent;
import edu.carleton.comp4104.assignment3.common.EventHandling.Events.ClientSendTextReplyEvent;
import edu.carleton.comp4104.assignment3.common.EventHandling.Events.LoginReplyEvent;
import javax.swing.SpringLayout;
import javax.swing.ScrollPaneConstants;

public class GUI extends JFrame implements EventHandler {
	private JTextArea transcript;
	private JButton sendButton;
	private JList userList;
	private JTextField chatBox;
	private Vector<String> users;
	private String userName = "Bryan";
	private JLabel userLabel;

	private App app;
	private AppBackEnd appBackEnd;

	/**
	 * Set up the gui to allow the user to chat
	 * 
	 * @param title
	 * @param sendQueue
	 */
	public GUI(String config) {

		this.setBounds(200, 200, 554,433);
		
		App.init(config);
		app = App.getInstance();
		appBackEnd = app.getAppBackEnd();
		app.setGUI(this);
		app.registerEventHandler(this);

		this.userName = userName;
		JPanel panel = new JPanel();
		userName = app.settings.username;
		SpringLayout sl_panel = new SpringLayout();
		panel.setLayout(sl_panel);

		userLabel = new JLabel("User: " + userName);
		sl_panel.putConstraint(SpringLayout.NORTH, userLabel, 0, SpringLayout.NORTH, panel);
		sl_panel.putConstraint(SpringLayout.WEST, userLabel, 0, SpringLayout.WEST, panel);
		sl_panel.putConstraint(SpringLayout.SOUTH, userLabel, 20, SpringLayout.NORTH, panel);

		panel.add(userLabel);
		userLabel.setText("User:" + app.settings.username);
		
		
		users = new Vector<String>();
		users.add("You are alone");
		userList = new JList(users);
		sl_panel.putConstraint(SpringLayout.NORTH, userList, 0, SpringLayout.SOUTH, userLabel);
		sl_panel.putConstraint(SpringLayout.WEST, userList, 0, SpringLayout.WEST, panel);
		panel.add(userList);
		transcript = new JTextArea(10, 50);

		transcript.setEditable(false);

		JScrollPane aScrollPane = new JScrollPane(transcript);
		aScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		sl_panel.putConstraint(SpringLayout.NORTH, aScrollPane, 0, SpringLayout.SOUTH, userList);
		sl_panel.putConstraint(SpringLayout.WEST, aScrollPane, 0, SpringLayout.WEST, panel);
		sl_panel.putConstraint(SpringLayout.EAST, aScrollPane, 0, SpringLayout.EAST, panel);
		panel.add(aScrollPane);
		chatBox = new JTextField();
		JScrollPane anotherScrollPane = new JScrollPane(chatBox);
		sl_panel.putConstraint(SpringLayout.WEST, anotherScrollPane, 0, SpringLayout.WEST, panel);
		sl_panel.putConstraint(SpringLayout.SOUTH, anotherScrollPane, 0, SpringLayout.SOUTH, panel);
		panel.add(anotherScrollPane);
		// panel.add(chatBox);

		sendButton = new JButton("Send");
		sl_panel.putConstraint(SpringLayout.SOUTH, aScrollPane, 0, SpringLayout.NORTH, sendButton);
		sl_panel.putConstraint(SpringLayout.SOUTH, sendButton, 0, SpringLayout.SOUTH, panel);
		sl_panel.putConstraint(SpringLayout.NORTH, anotherScrollPane, 0, SpringLayout.NORTH, sendButton);
		sl_panel.putConstraint(SpringLayout.EAST, anotherScrollPane, 0, SpringLayout.WEST, sendButton);
		sl_panel.putConstraint(SpringLayout.EAST, sendButton, 0, SpringLayout.EAST, aScrollPane);
		sendButton.addActionListener(new SendMessage());
		panel.add(sendButton);
		panel.setVisible(true);
		this.setContentPane(panel);

		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	/**
	 * Append the msg to the chat window
	 * 
	 * @param msg
	 */
	public synchronized void append(String msg) {

		transcript.append(msg + "\n");
	}

	/**
	 * Allow the Acceptor to update the list of users
	 */
	public synchronized void updateUserList() {
		Vector<String> nameList = appBackEnd.getUsers();
		int selected = userList.getSelectedIndex();
		userList.setListData(nameList);
		// userList = new JList(users);
		userList.setSelectedIndex(selected);
		this.repaint();
	}

	/**
	 * Listener for the send button, to let the user sent a message
	 * 
	 * @author bryan
	 * 
	 */
	private class SendMessage implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			try {
				if (userList.getSelectedIndex() == -1) {
					throw new IllegalArgumentException(
							"Must select a user to send a message to");
				}

				String message = chatBox.getText();
				String selectedUser = (String) userList.getSelectedValue();
				app.sendMessage(selectedUser, message);
				chatBox.setText("");
				AppBackEnd appBackEnd = app.getAppBackEnd();
				appBackEnd.sendMessage((String) userList.getSelectedValue(),
						message);
				transcript.append(app.settings.username + "->" + selectedUser + ":"
						+ message + "\n");
			} catch (IllegalArgumentException ie) {
				JOptionPane.showMessageDialog(null, ie.getMessage());
			}
		}
	}

	@Override
	public void registerWithReactor(Reactor reactor) {
		reactor.registerHandler(LoginReplyEvent.EventType, this);
		reactor.registerHandler(ClientDataChangeEvent.EventType, this);
		reactor.registerHandler(ClientGetTextRequestEvent.EventType, this);
		reactor.registerHandler(ClientSendTextReplyEvent.EventType, this); // Optional
																			// later
	}

	@Override
	public void handleEvent(final AbstractEvent inEvent) {
		if (inEvent instanceof ClientDataChangeEvent) {
			ClientDataChangeEvent clientDataChangeEvent = (ClientDataChangeEvent) inEvent;
			processClientDataChangeEvent(clientDataChangeEvent);
		} else if (inEvent instanceof ClientGetTextRequestEvent) {
			ClientGetTextRequestEvent clientGetTextRequestEvent = (ClientGetTextRequestEvent) inEvent;
			processClientGetTextRequestEvent(clientGetTextRequestEvent);
		} else if (inEvent instanceof ClientSendTextReplyEvent) {
			ClientSendTextReplyEvent clientSendTextReplyEvent = (ClientSendTextReplyEvent) inEvent;
			processClientSendTextReplyEvent(clientSendTextReplyEvent);
		} else if (inEvent instanceof LoginReplyEvent) {
			LoginReplyEvent loginReplyEvent = (LoginReplyEvent) inEvent;
			processLoginReplyEvent(loginReplyEvent);
		} else {

		}
	}

	protected void processLoginReplyEvent(LoginReplyEvent loginReplyEvent) {
		Vector<String> users = loginReplyEvent.ClientNames;
		appBackEnd.clearData();
		if (users != null)
			for (String user : users) {
				appBackEnd.addUser(user);
			}
		updateUserList();

	}

	// Someone other than us has logged in or out
	public void processClientDataChangeEvent(ClientDataChangeEvent event) {
		if (event.Change == ClientChangeReason.Login) {
			appBackEnd.addUser(event.ClientName);
			userList.setListData(appBackEnd.getUsers());
		} else {
			appBackEnd.removeUser(event.ClientName);
			userList.setListData(appBackEnd.getUsers());

		}

		updateUserList();

	}

	// Someone sent us a message
	public void processClientGetTextRequestEvent(ClientGetTextRequestEvent event) {
		appBackEnd.receiveMessage(event.ClientFromName, event.TextMessage);
		append(event.ClientFromName + "->" + app.settings.username + ":" + event.TextMessage);

	}

	// Message saying our sent message went through (optional, if we have time)
	public void processClientSendTextReplyEvent(ClientSendTextReplyEvent event) {

	}

	private String host;
	private Socket s;
	// private Message m;
	private AtomicBoolean running = new AtomicBoolean(true);
	private Properties props;
	private String name;
	private int port;

	/**
	 * Create a new client
	 */

	/**
	 * load configuation properties from a file
	 * 
	 * @param file
	 */
	public void loadPropertiesFile(String file) {
		props = new Properties();
		try {
			props.load(new FileReader(file));
			app.settings.username = props.getProperty("name");
			app.settings.ipAddress = props.getProperty("host");
			app.settings.port = props.getProperty("port");
		} catch (FileNotFoundException e) {
			System.out.println("File does not exist. Exitting");
			System.exit(1);
		} catch (IOException e) {
			System.out.println("File cannot be read. Exitting");
			System.exit(1);
		}

	}

	/**
	 * load a set of default configuration properties
	 */
	public void defaultProperties() {
		app.settings.username = "Bryan";
		app.settings.ipAddress = "localhost";
		app.settings.port = "8080";
	}

	/**
	 * Send the inital login request, and handle changes in protocol
	 */
	public void start() {
		setDetails("Chat client", app.settings.username);
		this.setVisible(true);
		SettingsActivity settings = new SettingsActivity();
		settings.connect();
	}

	private void setDetails(String string, String name2) {
		setTitle(string);
		userLabel.setText("User: " + name2);

	}

	/**
	 * Load the user name, ip and port from a config file
	 * 
	 * @param args
	 * @return
	 */
	public static String getConfigFile(String[] args) {
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-c") && i + 1 < args.length) {
				return args[i + 1];
			}
		}
		return null;
	}

	/**
	 * Parse the user name, ip and port from the command line arguments
	 */
	public static boolean getNameAndServer(String[] args) {
		String name = null;
		String host = null;
		String port = null;
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-name") && i + 1 < args.length) {
				name = args[i + 1];
			} else if (args[i].equals("-ip") && i + 1 < args.length) {
				host = args[i + 1];

			} else if (args[i].equals("-port") && i + 1 < args.length) {
				port = args[i + 1];
			}

		}
		if (name == null || host == null || port == null)
			return true;
		return false;
	}

	/**
	 * Entry point
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		String str = getConfigFile(args); // get the config file if one is given
											// on the command line
		if (str == null) {
			JFileChooser chooser = new JFileChooser();
			FileFilter filter = new ChooserFilter();
			chooser.setFileFilter(filter);
			((ChooserFilter) filter).addExtension("cfg");
			((ChooserFilter) filter)
					.setDescription("Client configuration file");
			chooser.addChoosableFileFilter(filter);
			chooser.setCurrentDirectory(new File(System.getProperty("user.dir")));

			chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
			int returnVal = chooser.showOpenDialog(new JFrame());
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				str = chooser.getSelectedFile().getName();
				System.out.println("You chose to open this file: " + str);
			}
		}
		if (str != null) {
			GUI gui = new GUI(str);
			gui.loadPropertiesFile(str);
			gui.start();
		}
		//
		// else if (getNameAndServer(args)) {
		// System.err
		// .println("Usage: -c 'path to config file' | -name NAME -ip IPADDRESS -port PORTNUMBER");
		// System.exit(1);
		// }
	}
}
