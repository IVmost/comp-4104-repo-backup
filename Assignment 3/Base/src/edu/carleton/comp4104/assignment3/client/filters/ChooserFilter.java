package edu.carleton.comp4104.assignment3.client.filters;

import java.util.ArrayList;
import java.io.File;
import javax.swing.*;
import javax.swing.filechooser.*;

public class ChooserFilter extends FileFilter {
	private ArrayList<String> extensionList= new ArrayList<String>();
	private String description;
	public void addExtension(String extension){
		extensionList.add(extension);
	}
	public boolean accept(File f) {
		if (extensionList.isEmpty())
			return true;
		
		String filename= f.getName();
		int location = filename.lastIndexOf('.');
		String ext;
        if (location > 0 &&  location < filename.length() - 1) {
            ext = filename.substring(location+1).toLowerCase();
        }else
        	return false;
        
        for (String extension: extensionList){
        	if (ext.matches(extension))
        		return true;
        }
		
		return false;
	}
	
	public void setDescription(String description){
		this.description=description;
	}
	@Override
	public String getDescription() {
		
		return description;
	}

}
