package edu.carleton.comp4104.assignment3.client;

import java.util.HashMap;
import java.util.Vector;

import edu.carleton.comp4104.assignment3.common.Connection.Messages.Message;
import edu.carleton.comp4104.assignment3.common.Connection.Messages.MessageBody;
import edu.carleton.comp4104.assignment3.common.Connection.Messages.MessageHeader;
import edu.carleton.comp4104.assignment3.common.Connection.Messages.MessageBodies.TextMessageBody;
import edu.carleton.comp4104.assignment3.common.Connection.Messages.MessageHeader.Command;
import edu.carleton.comp4104.assignment3.common.Connection.Messages.MessageHeader.CommandType;
import edu.carleton.comp4104.assignment3.common.EventHandling.AbstractEvent;
import edu.carleton.comp4104.assignment3.common.EventHandling.EventProcessor;
import edu.carleton.comp4104.assignment3.common.EventHandling.Events.ClientGetTextReplyEvent;
import edu.carleton.comp4104.assignment3.common.EventHandling.Events.ClientSendTextRequestEvent;
import edu.carleton.comp4104.assignment3.common.EventHandling.Events.LoginRequestEvent;
import edu.carleton.comp4104.assignment3.common.EventHandling.Events.LogoutRequestEvent;
import edu.carleton.comp4104.assignment3.common.EventHandling.Events.SendMessageEvent;

public class ClientEventToMessageTranslator extends EventProcessor {

	protected HashMap<String, Integer> _loggedInClients = new HashMap<String, Integer>();
	
	@Override
	protected void registerForEvents() {
		//process out going events into messages to send to the server
		_reactor.registerHandler(LoginRequestEvent.EventType, this);
		_reactor.registerHandler(LogoutRequestEvent.EventType, this);
		_reactor.registerHandler(ClientGetTextReplyEvent.EventType, this);
		_reactor.registerHandler(ClientSendTextRequestEvent.EventType, this);
	}

	@Override
	protected void processEvents(AbstractEvent event) {
		if (event instanceof LoginRequestEvent) {
			LoginRequestEvent loginRequestEvent = (LoginRequestEvent)event;
			processLoginRequestEvent(loginRequestEvent);
		}
		else if (event instanceof LogoutRequestEvent) {
			LogoutRequestEvent logoutRequestEvent = (LogoutRequestEvent)event;
			processLogoutRequestEvent(logoutRequestEvent);
		}
		else if (event instanceof ClientGetTextReplyEvent) {
			ClientGetTextReplyEvent clientGetTextReplyEvent = (ClientGetTextReplyEvent)event;
			processClientGetTextReplyEvent(clientGetTextReplyEvent);
		}
		else if (event instanceof ClientSendTextRequestEvent) {
			ClientSendTextRequestEvent clientSendTextRequestEvent = (ClientSendTextRequestEvent)event;
			processClientSendTextRequestEvent(clientSendTextRequestEvent);
		}
		else {
			System.err.println("MessageToEventTranslator.processEvents : don't know how to process - " + event.toString());
		}
	}
	
	protected void processLoginRequestEvent(LoginRequestEvent event) {
		//Send a message to server
		_reactor.dispatch(
				new SendMessageEvent(
						new Message(
								new MessageHeader(Command.Login, CommandType.Request, event.ClientName),
								null
								),
						null
						)
				);
	}

	protected void processLogoutRequestEvent(LogoutRequestEvent event) {		
		Vector<Integer> loggedInClientId = new Vector<Integer>();
		loggedInClientId.add(event.ClientId);
		
		//Send a message back to the client
		_reactor.dispatch(
				new SendMessageEvent(
						new Message(
								new MessageHeader(Command.Logout, CommandType.Request, event.ClientName), 
								null
								),
						null
						)
				);
	}

	protected void processClientGetTextReplyEvent(ClientGetTextReplyEvent event) {
		
		Vector<Integer> clientTo = new Vector<Integer>();
		clientTo.add(event.ClientToId);
		
		_reactor.dispatch(
				new SendMessageEvent(
						new Message(
								new MessageHeader(Command.ClientGetMessage, 
										CommandType.Request, 
										event.ClientFromName), 
								new MessageBody(true)
								),
						null
						)
				);
	}
	
	protected void processClientSendTextRequestEvent(ClientSendTextRequestEvent event) {
		
		_reactor.dispatch(
				new SendMessageEvent(
						new Message(
								new MessageHeader(Command.ClientSendMessage, 
										CommandType.Request, 
										event.ClientFromName), 
								new TextMessageBody(true, event.ClientToName, event.TextMessage, null)
								),
						null
						)
				);
	}
}
