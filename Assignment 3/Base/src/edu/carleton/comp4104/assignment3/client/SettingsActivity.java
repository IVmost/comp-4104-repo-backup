package edu.carleton.comp4104.assignment3.client;

import edu.carleton.comp4104.assignment3.client.ConnectReplyEvent;
import edu.carleton.comp4104.assignment3.common.Reactor;
import edu.carleton.comp4104.assignment3.common.EventHandling.AbstractEvent;
import edu.carleton.comp4104.assignment3.common.EventHandling.EventHandler;
import edu.carleton.comp4104.assignment3.common.EventHandling.Events.LoginReplyEvent;
import edu.carleton.comp4104.assignment3.common.EventHandling.Events.LogoutReplyEvent;

public class SettingsActivity implements EventHandler {

	private Settings setting;

	private App app;

	public SettingsActivity() {

		app = App.getInstance();
		setting = app.getSettings();

		app.registerEventHandler(this);

	}

	public void connect() {
		// Connect
		app.connect();

	}

	@Override
	public void registerWithReactor(Reactor reactor) {
		reactor.registerHandler(ConnectReplyEvent.EventType, this);
		reactor.registerHandler(LoginReplyEvent.EventType, this);
		reactor.registerHandler(LogoutReplyEvent.EventType, this);
	}

	@Override
	public void handleEvent(final AbstractEvent inEvent) {
		if (inEvent instanceof ConnectReplyEvent) {
			ConnectReplyEvent connectReplyEvent = (ConnectReplyEvent) inEvent;
			processConnectReplyEvent(connectReplyEvent);
		} else if (inEvent instanceof LoginReplyEvent) {
			LoginReplyEvent loginReplyEvent = (LoginReplyEvent) inEvent;
			processLoginReplyEvent(loginReplyEvent);
		} else if (inEvent instanceof LogoutReplyEvent) {
			LogoutReplyEvent logoutReplyEvent = (LogoutReplyEvent) inEvent;
			processLogoutReplyEvent(logoutReplyEvent);
		}
	}

	public void processConnectReplyEvent(ConnectReplyEvent event) {
		if (!event.Success) {
					System.err.println("Could not connect. Check server properties.");
					System.exit(1);
		}
	}

	public void processLoginReplyEvent(LoginReplyEvent event) {
		if (!event.Success) {
			System.err.println("Could not login. Trying changing your name.");
			System.exit(1);
		}
	}

	public void processLogoutReplyEvent(LogoutReplyEvent event) {
		if (!event.Success) {
			System.out.println("Could not logout.");
			
		} else {
			app.getAppBackEnd().clearData();
			app.getGUI().updateUserList();
		}
	}

}
