package edu.carleton.comp4104.assignment3.client;

import edu.carleton.comp4104.assignment3.common.EventHandling.AbstractEvent;

public class ConnectReplyEvent extends AbstractEvent {

	public final static String EventType = ConnectReplyEvent.class.getName();
	
	public final boolean Success;
	
	public ConnectReplyEvent(boolean success) {
		super(EventType);
		Success = success;
	}

}
